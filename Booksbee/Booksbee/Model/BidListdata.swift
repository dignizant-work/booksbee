import Foundation 
import ObjectMapper 

class BidListdata: Mappable { 

	var message: String = ""
	var bidData: [BidData] = []
	var flag: Int = 0

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		message <- map["message"] 
		bidData <- map["data"] 
		flag <- map["flag"] 
	}
} 

class BidData: Mappable { 

	var status: Int = 0
	var price: Int = 0
	var profile: String = ""
	var name: String = ""
	var bidPrice: String = ""
	var bidTime: String = ""
	var bookId: Int = 0
	var isWinner: Int = 0
	var timezone: String = ""
	var id: Int = 0

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		status <- map["status"] 
		price <- map["price"] 
		profile <- map["profile"] 
		name <- map["name"] 
		bidPrice <- map["bid_price"] 
		bidTime <- map["bid_time"] 
		bookId <- map["book_id"] 
		isWinner <- map["is_winner"] 
		timezone <- map["timezone"] 
		id <- map["id"] 
	}
} 

