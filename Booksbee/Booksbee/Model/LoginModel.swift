import Foundation 
import ObjectMapper 

class LoginModel: Mappable { 

	var flag: Int = 0
	var accessToken: String = ""
	var tokenType: String = ""
	var message: String = ""
    var data: LoginData? = nil

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
        flag <- map["flag"]
		accessToken <- map["access_token"] 
		tokenType <- map["token_type"] 
		message <- map["message"] 
		data <- map["data"] 
	}
} 

class LoginData: Mappable {

	var id: NSNumber = 0
	var name: String = ""
	var email: String = ""
	var emailVerification: NSNumber = 0
	var username: String = ""
	var mobileNo: String = ""
	var profile: String = ""
    var contryCode: String = ""
    
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		name <- map["name"] 
		email <- map["email"] 
		emailVerification <- map["email_verification"] 
		username <- map["username"] 
		mobileNo <- map["mobile_no"] 
		profile <- map["profile"]
        contryCode <- map["country_code"]
	}
} 

