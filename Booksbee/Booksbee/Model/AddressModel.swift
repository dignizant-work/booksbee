import Foundation 
import ObjectMapper 

class AddressModel: Mappable { 

	var flag: Int = 0
	var data: [AddressData] = []
	var message: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		data <- map["data"] 
		message <- map["message"] 
	}
} 

class AddressData: Mappable {

	var id: Int = 0
	var avenue: String = ""
	var block: String = ""
	var houseType: String = ""
	var flat: String = ""
	var addressName: String = ""
	var description: String = ""
	var street: String = ""
	var area: String = ""
	var floor: String = ""
    var isDefault: Int = 0
    
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		avenue <- map["avenue"] 
		block <- map["block"] 
		houseType <- map["house_type"] 
		flat <- map["flat"] 
		addressName <- map["address_name"] 
		description <- map["description"] 
		street <- map["street"] 
		area <- map["area"] 
		floor <- map["floor"]
        isDefault <- map["is_default"]
	}
} 

