import Foundation 
import ObjectMapper 

class HomeModel: Mappable { 

	var flag: Int = 0
	var homeData: HomeData? = nil
	var message: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		homeData <- map["data"]
		message <- map["message"] 
	}
} 

class HomeData: Mappable { 

	var banner: [Banner] = []
	var bestBook: [BestBook] = []
	var bestSeller: [BestSeller] = []

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		banner <- map["banner"] 
		bestBook <- map["best_book"] 
		bestSeller <- map["best_seller"] 
	}
} 

class BestSeller: Mappable { 

	var userId: Int = 0
	var name: String = ""
	var profile: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		userId <- map["user_id"] 
		name <- map["name"] 
		profile <- map["profile"] 
	}
} 

class BestBook: Mappable { 

	var id: Int = 0
	var userId: Int = 0
	var categoryId: Int = 0
	var isbn: String = ""
	var price: String = ""
	var saleType: Int = 0
	var status: String = ""
	var timezone: String = ""
	var isBest: Int = 0
	var startDateTime: String = ""
	var endDateTime: String = ""
	var isBidActive: Int = 0
	var maxBidPrice: String = ""
	var image: String = ""
	var images: [Images]? 
	var bookName: String = ""
	var description: String = ""
	var author: String = ""
	var bookPrice: String = ""
	var ownerName: String = ""
	var ownerImage: String = ""
    var rates: CGFloat = 0.0
    var rate_count: CGFloat = 0.0

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		userId <- map["user_id"] 
		categoryId <- map["category_id"] 
		isbn <- map["isbn"] 
		price <- map["price"] 
		saleType <- map["sale_type"] 
		status <- map["status"] 
		timezone <- map["timezone"] 
		isBest <- map["is_best"] 
		startDateTime <- map["start_date_time"] 
		endDateTime <- map["end_date_time"] 
		isBidActive <- map["is_bid_active"] 
		maxBidPrice <- map["max_bid_price"] 
		image <- map["image"] 
		images <- map["images"] 
		bookName <- map["book_name"] 
		description <- map["description"] 
		author <- map["author"] 
		bookPrice <- map["book_price"] 
		ownerName <- map["owner_name"] 
		ownerImage <- map["owner_image"]
        rates <- map["rates"]
        rate_count <- map["rate_count"]
        
	}
}

class Banner: Mappable { 

	var id: Int = 0
	var title: String = ""
	var image: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"] 
		image <- map["image"] 
	}
} 

