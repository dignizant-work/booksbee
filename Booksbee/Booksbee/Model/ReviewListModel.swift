import Foundation 
import ObjectMapper 

class ReviewListModel: Mappable { 

	var flag: Int = 0
	var reviewData: ReviewData? 
	var message: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		reviewData <- map["data"]
		message <- map["message"] 
	}
} 

class ReviewData: Mappable { 

	var currentPage: Int = 0
	var reviewDataList: [ReviewDataList]? 
	var firstPageUrl: String = ""
	var from: Int = 0
	var lastPage: Int = 0
	var lastPageUrl: String = ""
	var nextPageUrl: String = ""
	var path: String = ""
	var perPage: Int = 0
	var prevPageUrl: String = ""
	var to: Int = 0
	var total: Int = 0

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		currentPage <- map["current_page"] 
		reviewDataList <- map["data"] 
		firstPageUrl <- map["first_page_url"] 
		from <- map["from"] 
		lastPage <- map["last_page"] 
		lastPageUrl <- map["last_page_url"] 
		nextPageUrl <- map["next_page_url"] 
		path <- map["path"] 
		perPage <- map["per_page"] 
		prevPageUrl <- map["prev_page_url"] 
		to <- map["to"] 
		total <- map["total"] 
	}
} 

class ReviewDataList: Mappable { 

	var id: Int = 0
	var bookId: Int = 0
    var rate: CGFloat = 0.0
	var name: String = ""
	var title: String = ""
	var description: String = ""
    var image : String = ""
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		bookId <- map["book_id"] 
        rate <- map["rate"]
		name <- map["name"] 
		title <- map["title"] 
		description <- map["description"]
        image <- map["image"]
	}
} 

