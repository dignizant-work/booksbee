import Foundation 
import ObjectMapper 

class BookDetailsModel: Mappable { 

	var flag: Int = 0
	var bookDetails: BookDetails? 
	var message: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		bookDetails <- map["data"]
		message <- map["message"] 
	}
} 

class BookDetails: Mappable { 

	var id: Int = 0
	var userId: Int = 0
	var categoryId: Int = 0
	var isbn: String = ""
	var price: String = ""
	var saleType: Int = 0
	var status: String = ""
	var name: String = ""
    var rates: CGFloat = 0.0
	var rateCount: Int = 0
	var image: String = ""
	var images: [Images]? 
	var bookName: String = ""
	var description: String = ""
	var author: String = ""
	var bookPrice: String = ""
    var ownerImage: String = ""
    var ownerName: String = ""
    var isCartExists: Int = 0
    var endDateTime: String = ""
    var startDateTime: String = ""
    
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		userId <- map["user_id"] 
		categoryId <- map["category_id"] 
		isbn <- map["isbn"] 
		price <- map["price"] 
		saleType <- map["sale_type"] 
		status <- map["status"] 
		name <- map["name"] 
		rates <- map["rates"] 
		rateCount <- map["rate_count"] 
		image <- map["image"] 
		images <- map["images"] 
		bookName <- map["book_name"] 
		description <- map["description"] 
		author <- map["author"] 
		bookPrice <- map["book_price"]
        ownerImage <- map["owner_image"]
        ownerName <- map["owner_name"]
        isCartExists <- map["is_cartExists"]
        endDateTime <- map["end_date_time"]
        startDateTime <- map["start_date_time"]
	}
} 

class Images: Mappable { 

	var id: Int = 0
	var bookId: Int = 0
	var img: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		bookId <- map["book_id"] 
		img <- map["img"] 
	}
} 

