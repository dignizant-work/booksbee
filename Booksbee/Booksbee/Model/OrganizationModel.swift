import Foundation 
import ObjectMapper 

class OrganizationModel: Mappable { 
    
    var flag: Int = 0
    var data: OrgatizationData? = nil
    var message: String = ""
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        flag <- map["flag"]
        data <- map["data"]
        message <- map["message"]
    }
} 

class OrgatizationData: Mappable {
    
    var currentPage: Int = 0
    var bookdata: [Bookdata] = []
    var firstPageUrl: String = ""
    var from: Int = 0
    var lastPage: Int = 0
    var lastPageUrl: String = ""
    var nextPageUrl: String = ""
    var path: String = ""
    var perPage: Int = 0
    var prevPageUrl: String = ""
    var to: Int = 0
    var total: Int = 0
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        
        currentPage <- map["current_page"]
        bookdata <- map["data"]
        firstPageUrl <- map["first_page_url"]
        from <- map["from"]
        lastPage <- map["last_page"]
        lastPageUrl <- map["last_page_url"]
        nextPageUrl <- map["next_page_url"]
        path <- map["path"]
        perPage <- map["per_page"]
        prevPageUrl <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
    }
} 

class Bookdata: Mappable { 
    
    var id: Int = 0
    var contactNo: String = ""
    var image: String = ""
    var orgName: String = ""
    var description: String = ""
    var address: String = ""
    
    var userId: Int = 0
    var categoryId: Int = 0
    var isbn: String = ""
    var bookPrice: String = ""
    var saleType: Int = 0
    var status: String = ""
    var name: String = ""
    var rates: CGFloat = 0.0
    var rateCount: Int = 0
    var bookName: String = ""
    var author: String = ""
    var bookId: Int = 0
    var price: String = ""
    var images: [Images]? 
    var isConfirm: Int = 0
    var orderId: Int = 0
    var endDateTime: String = ""
    var startDateTime: String = ""
    var profile: String = ""
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        contactNo <- map["contact_no"]
        image <- map["image"]
        orgName <- map["org_name"]
        description <- map["description"]
        address <- map["address"]
        userId <- map["user_id"]
        categoryId <- map["category_id"]
        isbn <- map["isbn"]
        bookPrice <- map["book_price"]
        saleType <- map["sale_type"]
        status <- map["status"]
        name <- map["name"]
        rates <- map["rates"]
        rateCount <- map["rate_count"]
        bookName <- map["book_name"]
        author <- map["author"]
        bookId <- map["book_id"]
        price <- map["price"]
        images <- map["images"]
        isConfirm <- map["is_confirm"]
        orderId <- map["order_id"]
        endDateTime <- map["end_date_time"]
        startDateTime <- map["start_date_time"]
        profile <- map["profile"]
    }
} 
