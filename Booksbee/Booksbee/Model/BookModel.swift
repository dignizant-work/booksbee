import Foundation 
import ObjectMapper 

class BookModel: Mappable { 
    
    var flag: Int = 0
    var bookDataModel: BookDataModel? = nil
    var message: String = ""
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        flag <- map["flag"]
        bookDataModel <- map["data"]
        message <- map["message"]
    }
} 

class BookDataModel: Mappable { 
    
    var currentPage: Int = 0
    var bookData: [Bookdata] = []
    var myOrdeData: [MyOrdeData] = []
    var firstPageUrl: String = ""
    var from: Int = 0
    var lastPage: Int = 0
    var lastPageUrl: String = ""
    var nextPageUrl: String = ""
    var path: String = ""
    var perPage: Int = 0
    var prevPageUrl: String = ""
    var to: Int = 0
    var total: Int = 0
    var cartData: [Bookdata] = []
    var totalPrice: String = ""
    var isCartExists: Int = 0
    var status: String = ""
    var paymentType: Int = 0
    var paymentStatus: Int = 0
    var address: AddressData? = nil
    var addressId: Int = 0
    var paymentAmt: String = ""
    var userId: Int = 0
    var orderBookData: [Bookdata] = []
    var id: Int = 0
    var orderDate: String = ""
    var price = ""
    var ownerName: String = ""
    var description: String = ""
    var isWinner: Int? = 0
    var bidTime: String = ""
    var image: String = ""
    var startDateTime: String = ""
    var endDateTime: String = ""
    var bidPrice: String = ""
    var saleType: Int? = 0
    var author: String = ""
    var bookId: Int? = 0
    var timezone: String = ""
    var categoryId: Int? = 0
    var bookName: String = ""
    var isBidActive: Int? = 0
    var images: [Images]?
    var isbn: String = ""
    var ownerImage: String = ""
    var maxBidPrice: String = ""
    var rateCount: Int = 0
    var rates: CGFloat = 0.0
    var book_price = ""
    
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        
        status <- map["status"]
        paymentType <- map["payment_type"]
        paymentStatus <- map["payment_status"]
        address <- map["address"]
        addressId <- map["address_id"]
        paymentAmt <- map["payment_amt"]
        userId <- map["user_id"]
        orderBookData <- map["book_data"]
        id <- map["id"]
        orderDate <- map["order_date"]
        currentPage <- map["current_page"]
        bookData <- map["data"]
        firstPageUrl <- map["first_page_url"]
        from <- map["from"]
        lastPage <- map["last_page"]
        lastPageUrl <- map["last_page_url"]
        nextPageUrl <- map["next_page_url"]
        path <- map["path"]
        perPage <- map["per_page"]
        prevPageUrl <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
        cartData <- map["cart"]
        totalPrice <- map["total_price"]
        isCartExists <- map["is_cartExists"]
        myOrdeData <- map["data"]
        price <- map["price"]
        ownerName <- map["owner_name"]
        description <- map["description"]
        isWinner <- map["is_winner"]
        bidTime <- map["bid_time"]
        image <- map["image"]
        startDateTime <- map["start_date_time"]
        endDateTime <- map["end_date_time"]
        bidPrice <- map["bid_price"]
        saleType <- map["sale_type"]
        author <- map["author"]
        bookId <- map["book_id"]
        timezone <- map["timezone"]
        categoryId <- map["category_id"]
        bookName <- map["book_name"]
        isBidActive <- map["is_bid_active"]
        images <- map["images"]
        isbn <- map["isbn"]
        ownerImage <- map["owner_image"]
        maxBidPrice <- map["max_bid_price"]
        rateCount <- map["rate_count"]
        rates <- map["rates"]
        book_price <- map["book_price"]
    }
}

class MyOrdeData: Mappable {
    
    var image: String = ""
    var orderDate: String = ""
    var orderId: Int = 0
    var status: String = ""
    var totalPrice: String = ""
    var paymentType: Int = 0
    var timezone: String = ""
    var images: [Images] = []
    var rateCount: Int = 0
    var bookName: String = ""
    var bidPrice: String = ""
    var rates: CGFloat = 0.0
    var author: String = ""
    var description: String = ""
    var bookId: Int = 0
    var ownerName: String = ""
    var isWinner: Int = 0
    var bidTime: String = ""
    var price: CGFloat = 0.0
    var ownerImage: String = ""
    var id: Int = 0
    var notificationType: Int = 0
    var notification: String = ""
    var notificationText: String = ""
    var bookImage: String = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        orderDate <- map["order_date"]
        orderId <- map["order_id"]
        status <- map["status"]
        totalPrice <- map["total_price"]
        paymentType <- map["payment_type"]
        timezone <- map["timezone"]
        rateCount <- map["rate_count"]
        bookName <- map["book_name"]
        bidPrice <- map["bid_price"]
        image <- map["image"]
        rates <- map["rates"]
        author <- map["author"]
        description <- map["description"]
        bookId <- map["book_id"]
        ownerName <- map["owner_name"]
        isWinner <- map["is_winner"]
        bidTime <- map["bid_time"]
        price <- map["price"]
        ownerImage <- map["owner_image"]
        id <- map["id"]
        
        notificationType <- map["notification_type"]
        notification <- map["notification"]
        notificationText <- map["notification_text"]
        bookImage <- map["book_image"]

    }
}

