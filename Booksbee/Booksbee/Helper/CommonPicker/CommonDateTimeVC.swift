//
//  CommonDateTimeVC.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit


public protocol DateTimePickerDelegate {
    func setDateandTime(dateValue: Date, type: Int)
}

class CommonDateTimeVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var vwColor          : UIView!
    @IBOutlet weak var dateTimePicker   : UIDatePicker!
    @IBOutlet weak var btnCancel        : UIButton!
    @IBOutlet weak var btnDone          : UIButton!
    
    
    //MARK:- Variable
    
    var pickerDelegate : DateTimePickerDelegate?
    var controlType : Int = 0 //0 = Time / 1 = Date / 2 = Datetime
    
    var setMinimumDate : Date?
    var setMaximumDate : Date?

    var isSetMinimumDate : Bool = false
    var isSetMaximumDate : Bool = false

    var isSetDatePickerDate = false
    var datePickerDateValue : Date?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwColor.backgroundColor = UIColor.appDetailTextColor
        
        btnDone.setTitle("Done", for: .normal)
        btnCancel.setTitle("Cancel", for: .normal)
        
        [btnDone, btnCancel].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .medium)
            btn?.setTitleColor(.appTitleTextColor, for: .normal)
        }
        
        if isSetMinimumDate {
            if setMinimumDate != nil {
                dateTimePicker.minimumDate = setMinimumDate ?? Date()
            }
        }
        
        if isSetMaximumDate{
            if setMaximumDate != nil{
                dateTimePicker.maximumDate = setMaximumDate ?? Date()
            }
        }
        
        if controlType == 0 {
            dateTimePicker.datePickerMode = .time
        }
        else if controlType == 1 {
            dateTimePicker.datePickerMode = .date
        }
        else if controlType == 2 {
            dateTimePicker.datePickerMode = .dateAndTime
        }
        
        if #available(iOS 13.4, *) {
            dateTimePicker.preferredDatePickerStyle = .wheels
        }
        
    }
    
    
    //MARK:- Button Action
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        pickerDelegate?.setDateandTime(dateValue: dateTimePicker.date, type: controlType)
    }
    
}

