//
//  LanguageHelper.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit

let english = "en"
let arabic = "ar"
let appLanguage = "appLanguage"

extension String {
    
    var localized: String {
        if let _ = UserDefaults.standard.string(forKey: appLanguage) {} else {
            // we set a default, just in case
            UserDefaults.standard.set(english, forKey: appLanguage)
            UserDefaults.standard.synchronize()
        }

        let lang = UserDefaults.standard.string(forKey: appLanguage)

        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)

        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

func setLanguage(lng: String) {
    UserDefaults.standard.setValue(lng, forKey: appLanguage)
    UserDefaults.standard.synchronize()
}

func getLanguage() -> String {
    if UserDefaults.standard.value(forKey: appLanguage) == nil{
        setLanguage(lng: english)
    }
    return UserDefaults.standard.value(forKey: appLanguage) as! String
}
