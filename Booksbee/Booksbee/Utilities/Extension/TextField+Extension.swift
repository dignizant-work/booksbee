//
//  TextField+Extension.swift
//  Gymscanner
//
//  Created by jay on 21/07/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {
    func addBottomBorder(){
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
