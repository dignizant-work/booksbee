//
//  UINavigationController.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {

    func getReferenceVC<ViewController: UIViewController>(to viewController: ViewController.Type) -> ViewController? {
        return viewControllers.first { $0 is ViewController } as? ViewController
    }
}
