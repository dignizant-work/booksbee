//
//  Date + Extension.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit

func generateDatesArrayBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date) ->[Date] {
    var datesArray: [Date] =  [Date]()
    var startDate = startDate
    let calendar = Calendar.current
    
    let fmt = DateFormatter()
    fmt.dateFormat = OrinalDtFormater
    
    while startDate <= endDate {
        datesArray.append(startDate)
        startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
    }
    return datesArray
}

func DatesAvailableBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date,SelectedDate:Date) ->Bool {
    var startDate = startDate
    let calendar = Calendar.current
    let fmt = DateFormatter()
    fmt.dateFormat = OrinalDtFormater
    while startDate <= endDate {
        if(startDate == SelectedDate) {
            return true
        }
        startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
    }
    return false
}

//MARK: - Date and Time Formatter
func stringTodate(Formatter:String,strDate:String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Formatter
    let FinalDate = dateFormatter.date(from: strDate)!
    return FinalDate
}

func DateToString(Formatter:String,date:Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Formatter
    let FinalDate:String = dateFormatter.string(from: date)
    return FinalDate
}

func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String {
    
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = OrignalFormatter
    guard let convertedDate = dateformatter.date(from: strDate) else {
        return ""
    }
    dateformatter.dateFormat = YouWantFormatter
    let convertedString = dateformatter.string(from: convertedDate)
    return convertedString
}

func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String {
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = OrignalFormatter
    let strdate: String = dateFormatterGet.string(from: Date)
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = YouWantFormatter
    let date: Date = dateFormatterPrint.date(from: strdate)!
    return dateFormatterPrint.string(from: date)
}

