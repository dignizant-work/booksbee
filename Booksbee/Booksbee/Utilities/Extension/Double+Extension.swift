//
//  Double+Extension.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func toString() -> String {
        return String(format: "%.1f",self)
    }
}

extension CGFloat {
    /// Rounds the double to decimal places value
    
    func toString() -> String {
        return String(format: "%.1f",self)
    }
}

