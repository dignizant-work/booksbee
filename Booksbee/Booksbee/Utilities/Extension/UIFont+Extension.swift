//
//  UIFont+Extension.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String {
    
    case bold = "Helvetica-Bold"
    case regular = "HelveticaNeue"
    case medium = "HelveticaNeue-Medium"
}

func themeFont(size : Float,fontname : themeFonts) -> UIFont {
    
    if UIScreen.main.bounds.width <= 320 {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
}
