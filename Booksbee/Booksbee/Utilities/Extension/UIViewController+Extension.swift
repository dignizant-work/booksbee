//
//  UIViewController+Extension.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import SwiftyJSON
import HCSStarRatingView
import DropDown

extension UIViewController {
    
    @IBAction func btnDismissAction(_ sender:UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func configureReviewRatingView(ratingView : HCSStarRatingView) {
        ratingView.allowsHalfStars = false
        ratingView.maximumValue = 5
        ratingView.minimumValue = 0
        ratingView.emptyStarImage = #imageLiteral(resourceName: "ic_star_unselected_small")
        ratingView.filledStarImage = #imageLiteral(resourceName: "ic_star_selected_big")
    }
    
    func configureRatingView(ratingView : HCSStarRatingView) {
        ratingView.allowsHalfStars = false
        ratingView.maximumValue = 5
        ratingView.minimumValue = 0
        ratingView.emptyStarImage = #imageLiteral(resourceName: "ic_star_unselected_small")
        ratingView.filledStarImage = #imageLiteral(resourceName: "ic_star_selected_small")
    }
    
    //MARK: - DropDown Config
    
    func configureDropdown(dropdown : DropDown,sender:UIControl,isWidth:Bool = true) {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .bottom
        dropdown.dismissMode = .automatic
        dropdown.selectionBackgroundColor = UIColor.clear
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.size.width
//        if (self.view.frame.width == 320) {
//            
//            dropdown.width = sender.frame.size.width + 100
//        }
        
        dropdown.semanticContentAttribute = .spatial
        DispatchQueue.main.async {
            if getLanguage() == arabic {
                dropdown.semanticContentAttribute = .spatial
                dropdown.customCellConfiguration = {(index: Int, item: String, cell: DropDownCell) -> Void in
                    cell.optionLabel.textAlignment = .right
                }
            }else{
                dropdown.semanticContentAttribute = .spatial
            }
            
        }
        
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        
        dropdown.cancelAction = { [unowned self] in
        }
    }
    
    func addDoneButtonOnKeyboard(textfield : UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.gray
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor.white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    func setupNavigationbarBackButton(titleText:String) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
//        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.black
        HeaderView.font = themeFont(size: 16, fontname: .bold)
        
        self.navigationItem.titleView = HeaderView
        //        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        let backbutton = UIButton(type: .custom)
        backbutton.setImage(UIImage(named: "ic_arrow_back")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        backbutton.setImageTintColor(.black)
//        backbutton.setTitle("Back".localized, for: .normal)
//        backbutton.tintColor = .black
        backbutton.setTitleColor(backbutton.tintColor, for: .normal) // You can change the TitleColor
        backbutton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
        
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.appBackgroundColor
        self.navigationController?.navigationBar.barTintColor = UIColor.appBackgroundColor
        self.navigationController?.view.backgroundColor = .appBackgroundColor
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        setSwipeDirection()
    }
    
    func setSwipeDirection() {
        
        if getLanguage() == arabic {
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
            navigationController?.navigationBar.semanticContentAttribute =  .forceRightToLeft
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        else {
            navigationController?.view.semanticContentAttribute = .forceLeftToRight
            navigationController?.navigationBar.semanticContentAttribute =  .forceLeftToRight
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
    }
    
    func setupNavigationbarWithoutBackButton(titleText:String, barColor:UIColor? = nil) {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor = barColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        let lblName = UILabel()
        lblName.text = titleText
        lblName.textColor = .white
        lblName.textAlignment = .left
        lblName.font = themeFont(size: 25, fontname: .bold)
        let btnCustomLabel = UIBarButtonItem(customView: lblName)
        
        self.title = titleText
        
        self.navigationItem.leftBarButtonItems = [btnCustomLabel]
    }
    
    func setupNavigationbarTitle(titleText:String, barColor:UIColor? = nil) {
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor = barColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = titleText
        
        setSwipeDirection()
    }
    
        
    func setupTransparentNavigationBar() {
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    func setupNavigationbarBottomLineHidden() {
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.layer.masksToBounds = false
        
        setSwipeDirection()
    }
          
    func setNavigationShadow() {
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
        self.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismiss() {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK: Get Collection height
    func getCollectionViewContaintHeight(collectionView: UICollectionView) -> CGFloat {
        return collectionView.collectionViewLayout.collectionViewContentSize.height
    }
    
        ///--- Hours, minutes,  seconds
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    //MARK: - Images with String
    func getAttributedString(imgAttachment:UIImage,strPostText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(image1String)
        fullString.append(NSAttributedString(string: strPostText))
        return fullString
    }
    
    //MARK: - Call Method
    func callUser(strPhoneNumber:String) {
        
        let phone = strPhoneNumber
        
        var phoneStr: String = "telprompt://\(phone)"
        phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
        
        let urlPhone = URL(string: phoneStr)
        if UIApplication.shared.canOpenURL(urlPhone!) {
            //UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(urlPhone!)
            }
        }
        else {
            makeToast(strMessage: "Call facility is not available!!!")
        }
    }
    
    //MARK:- key board hide
    func hidekeyBoardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
    
    
    func showAlert(_ title: String, _ message: String, _ yesButton: String = "Done".localized ,_ noButton: String = "Cancel".localized , _ done: @escaping()->Void) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertDone = UIAlertAction(title: yesButton , style: .default) { alt in
            done()
        }
        let alertCancel = UIAlertAction(title: noButton, style: .default) { alt in
            print("Cancel Tapped")
        }
        alert.addAction(alertCancel)
        alert.addAction(alertDone)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showGuestUserAlert(){
        
        showAlert("Booksbee".localized, "You need to login first for access this functionality.\nAre you sure you want to login?".localized, "Login".localized) {
            
            let loginvc = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            loginvc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(loginvc, animated: true)
        }
    }

    func textShare(txt: String) {
        // text to share
        let text = "This is some text that I want to share."
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
//        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func showTableFooterIndicater(tableview: UITableView) {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.tintColor = .appGoldenColor
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableview.bounds.width, height: CGFloat(60))
        tableview.tableFooterView = spinner
        tableview.tableFooterView?.isHidden = false
    }
}

//If swipe to back not work then set this
extension UINavigationController {

    override public func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = nil
    }

}

