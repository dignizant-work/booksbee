//
//  UIButton + Extension.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func setupThemeButtonUI(backColor:UIColor=UIColor.red,font:UIFont=themeFont(size: 20, fontname: .regular),fontColor:UIColor = .white) {
        self.setTitleColor(fontColor, for: .normal)
        self.backgroundColor = backColor
        self.titleLabel?.font = font
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func setUpThemeBackButtonUI() {
        self.setTitleColor(UIColor.black, for: .normal)
        self.titleLabel?.font = themeFont(size: 16, fontname: .regular)
    }
    
    func setImageTintColor(_ color: UIColor) {
        let tintedImage = self.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.setImage(tintedImage, for: .normal)
        self.tintColor = color
    }

}
