//
//  BrowseListVM.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class BrowseListVM {
    
    //MARK: Variables
    var theController = BrowseListVC()
    var arrBookData: [JSON] = []
    var arrBookSearchData: [JSON] = []
    var refreshControl = UIRefreshControl()
    
    var intSelectedTabCategoryID = 0
    var dictBookData = BookModel(JSON: JSON().dictionaryValue)
    var activityIndicator = UIRefreshControl()
    var nxtPage = 1

    var searchText = ""
    
    init(theController: BrowseListVC) {
        self.theController = theController
    }
    
    func setUpBookData() -> [JSON] {
        var arr:[JSON] = []
        var dict = JSON()
        dict["img"].stringValue = "a"
        dict["title"].stringValue = "The Forgotten Hours"
        dict["author"].stringValue = "Sarah Perez"
        dict["rate"].stringValue = "4.5"
        dict["prize"].stringValue = "$ 4.99"
        dict["sub_prize"].stringValue = ""
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "aa"
        dict["title"].stringValue = "Whiskey in a Teacup"
        dict["author"].stringValue = "Frank Lee"
        dict["rate"].stringValue = "5.0"
        dict["prize"].stringValue = "$ 3.99"
        dict["sub_prize"].stringValue = "$ 12.99"
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "aaa"
        dict["title"].stringValue = "SHE: kate spade new york"
        dict["author"].stringValue = "Sarah Perez"
        dict["rate"].stringValue = "3.5"
        dict["prize"].stringValue = "$ 6.99"
        dict["sub_prize"].stringValue = "$ 12.99"
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "aaaa"
        dict["title"].stringValue = "The Killer Collective"
        dict["author"].stringValue = "Sarah Perez"
        dict["rate"].stringValue = "4.0"
        dict["prize"].stringValue = "$ 4.99"
        dict["sub_prize"].stringValue = "$ 14.99"
        arr.append(dict)
        
        return arr
    }
    
}


//MARK: - API Calling

extension BrowseListVM{
    
    func getBookListApi(page: Int,isShowLoader:Bool = true ,completionHandlor:@escaping(JSON)->Void) {
                    
        let param = ["category_id": self.intSelectedTabCategoryID,
                     "search_text" : searchText] as [String : Any]
        
        print("param:\(param)")
        
        var url = ""
        if page == 1  {
            url = bookListURL
            if isShowLoader {
                showIndicator()
            }
            
        }
        else {
            url = bookListURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            self.activityIndicator.endRefreshing()
            if (error == nil) {
                let dict = JSON(data!)
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
//                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    if page == 1 {
                        self.dictBookData = nil
                        self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = Bookdata(JSON: bookData.dictionaryObject!)
                            self.dictBookData?.bookDataModel?.bookData.append(data!)
                        }
                    }
                }
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
}
