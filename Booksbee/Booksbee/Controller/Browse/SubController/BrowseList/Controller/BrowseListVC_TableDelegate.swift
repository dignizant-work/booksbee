//
//  BrowseListVC_TableDelegate.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension BrowseListVC : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainVM.dictBookData?.bookDataModel?.bookData.count ?? 0 == 0 || self.mainVM.dictBookData?.bookDataModel?.bookData == nil{
            setTableViewEmptyMessage(self.mainVM.dictBookData?.message ?? "", tableView: tableView)
            return 0
        }
        
        tableView.backgroundView = nil
        return self.mainVM.dictBookData?.bookDataModel?.bookData.count ?? 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBookTableCell", for: indexPath) as! MyBookTableCell
        
//        let dict = self.mainVM.arrBookData[indexPath.row]
//
//        cell.imgCover.image = UIImage(named: dict["img"].stringValue)
//        cell.lblBookTitle.text = dict["title"].stringValue
//        cell.lblAvgRate.text = dict["rate"].stringValue
//        cell.lblPrize.text = dict["prize"].stringValue
//        cell.lblSubPrize.text = dict["sub_prize"].stringValue
//
//        let cgValue = CGFloat(truncating: NumberFormatter().number(from: dict["rate"].stringValue) ?? 0)
//        cell.vwStarRating.value = cgValue
        
        
        if let dict = self.mainVM.dictBookData?.bookDataModel?.bookData[indexPath.row]{
            cell.setupMyBookCell(bookData: dict)
        }
        
        cell.vwBottomLine.isHidden = true
        cell.btnEdit.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let dict = self.mainVM.dictBookData?.bookDataModel?.bookData[indexPath.row]{
            let vc = homeStoryboard.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
            vc.hidesBottomBarWhenPushed = true
            vc.mainVM.bookId = dict.id
            vc.mainVM.strBookName = dict.bookName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
