//
//  BrowseListVC.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class BrowseListVC: UIViewController {

    //MARK: Variables
    lazy var mainView: BrowseListView = {
        return self.view as! BrowseListView
    }()
    
    lazy var mainVM: BrowseListVM = {
        return BrowseListVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.registerXib()
        NotificationCenter.default.addObserver(self, selector: #selector(isBrowseSearchData), name: Notification.Name("isBrowseSearchData"), object: nil)
        setUpPullToRefresh()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.mainVM.getBookListApi(page: 1) { data in
            self.mainView.tblBrowserList.reloadData()
        }
    }
}


//MARK: SetUp
extension BrowseListVC {
    
    func setUpPullToRefresh() {
        self.mainVM.refreshControl.tintColor = .appTitleTextColor
        self.mainVM.refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        self.mainView.tblBrowserList.addSubview(self.mainVM.refreshControl)
    }
    
    @objc func refreshData(_ sender: UIRefreshControl) {
        
        self.mainVM.nxtPage = 1
        
        self.mainVM.getBookListApi(page: self.mainVM.nxtPage,isShowLoader: false) { data in
            self.mainView.tblBrowserList.backgroundView = nil
            self.mainView.tblBrowserList.reloadData()
            sender.endRefreshing()
        }
    }
    
}

//MARK: Search Data
extension BrowseListVC {
    
    @objc func isBrowseSearchData(notification: NSNotification){
        
        if let strSearchString = notification.object as? String{
            
            self.mainVM.searchText = strSearchString
            self.mainVM.getBookListApi(page: 1) { data in
                self.mainView.tblBrowserList.reloadData()
            }
        }
    }
}
