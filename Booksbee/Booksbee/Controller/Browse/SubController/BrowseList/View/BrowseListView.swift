//
//  BrowseListView.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class BrowseListView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var tblBrowserList: UITableView!
    
    
    func setUpUI() {
        registerXib()
    }
    
    func registerXib() {
        self.tblBrowserList.register(UINib(nibName: "MyBookTableCell", bundle: nil), forCellReuseIdentifier: "MyBookTableCell")
    }
    
}
