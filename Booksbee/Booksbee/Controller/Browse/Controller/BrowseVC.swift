//
//  BrowseVC.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import CarbonKit

class BrowseVC: UIViewController {

    //MARK: Variables
    lazy var mainView: BrowseView = {
        return self.view as! BrowseView
    }()
    
    lazy var mainVM: BrowseVM = {
        return BrowseVM(theController: self)
    }()
    
    var txtBrowseSearchString = ""
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUI()
        setUp()
        self.mainVM.getCategoryListApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
}

//MARK: SetUp
extension BrowseVC {
    
    func setUp() {
        
        self.mainView.setUpUI()
        self.title = "Browse".localized
        setupTransparentNavigationBar()
        
//        self.mainView.txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        txtBrowseSearchString = textField.text ?? ""
        NotificationCenter.default.post(name: Notification.Name("isBrowseSearchData"), object: txtBrowseSearchString)
    }
}

//MARK: Setup Carbon kit
extension BrowseVC: CarbonTabSwipeNavigationDelegate {
    
    func setUpCarbonKit() {
        
        let arrayHeader = self.mainVM.arrBooksCategory.map { (json) -> String in
            return json["category"].stringValue
        }
       
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: arrayHeader, delegate: self)
        carbonTabSwipeNavigation.setTabExtraWidth(40)
        carbonTabSwipeNavigation.setIndicatorColor(.appGoldenColor)
        carbonTabSwipeNavigation.setIndicatorHeight(3)
        carbonTabSwipeNavigation.setTabBarHeight(50)
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = .appBackgroundColor
        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = .appBackgroundColor
        carbonTabSwipeNavigation.setSelectedColor(.appGoldenColor, font: themeFont(size: 14, fontname: .medium))
        carbonTabSwipeNavigation.setNormalColor(.appDetailTextColor, font: themeFont(size: 14, fontname: .regular))
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.mainView.vwCarbonSwipe)
//        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(100, forSegmentAt: 4)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let vc = homeStoryboard.instantiateViewController(withIdentifier: "BrowseListVC") as! BrowseListVC
        vc.mainView.tblBrowserList.backgroundView = nil
        vc.mainVM.intSelectedTabCategoryID = self.mainVM.arrBooksCategory[Int(index)]["id"].intValue
        return vc
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        
        self.mainView.txtSearch.text = ""
        txtBrowseSearchString = ""
//        NotificationCenter.default.post(name: Notification.Name("isBrowseSearchData"), object: txtBrowseSearchString)
        self.view.endEditing(true)
    }
    
}

//MARK: TextField Delegate
extension BrowseVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        NotificationCenter.default.post(name: Notification.Name("isBrowseSearchData"), object: textField.text?.toTrim())
        return textField.resignFirstResponder()
        
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        self.view.endEditing(true)
//        if textField == self.mainView.txtSearch {
//            return false
//        }
//        return true
//    }
    
}
