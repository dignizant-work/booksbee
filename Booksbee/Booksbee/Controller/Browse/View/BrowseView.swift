//
//  BrowseView.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class BrowseView: UIView {
    
    //MARK:- Outlets
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var txtSearch: CustomTextField!
    @IBOutlet weak var vwCarbonSwipe: UIView!
    
    //MARK: SetUpUI
    func setUpUI() {
        
        [txtSearch].forEach { (txt) in
            txt?.placeholder = "Search here"
            txt?.textColor = .appTitleTextColor
            txt?.font = themeFont(size: 15, fontname: .regular)
            txt?.placeholderColor(.appTitleTextColor)
            
            if getLanguage() == arabic {
                txt?.textAlignment = .right
            }
        }
        
        textFieldClearButtonColorSet(txtSearch, .appTitleTextColor)
        
        
    }
        
}
