//
//  BrowseVM.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class BrowseVM {
    
    var theController = BrowseVC()
//    var arrTab = ["New released", "Recommended", "Adventure", "Classics", "Fantasy"]
    var arrBooksCategory: [JSON] = []

    
    init(theController: BrowseVC) {
        self.theController = theController
        
    }
}

//MARK: - API calling

extension BrowseVM{
    
    func getCategoryListApi() {
        
        let  url = categoryListURL
        
        WebServices().MakeGetAPI(name: url, params: [:]) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                let innerDataArray = dict["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    self.arrBooksCategory = []
                    self.arrBooksCategory = innerDataArray
                    
                    self.theController.setUpCarbonKit()
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
        
    }
    
    
}
