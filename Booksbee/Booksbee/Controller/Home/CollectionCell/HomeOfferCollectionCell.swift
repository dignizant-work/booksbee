//
//  HomeOfferCollectionCell.swift
//  Booksbee
//
//  Created by vishal on 08/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class HomeOfferCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()
    }
    
    func setUpUI() {
        
        self.imgBook.layer.cornerRadius = 10
        self.imgBook.layer.masksToBounds = true

        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = .white
            lbl?.font = themeFont(size: 34, fontname: .regular)
        }
        [lblSubTitle].forEach { (lbl) in
            lbl?.textColor = .white
            lbl?.font = themeFont(size: 46, fontname: .bold)
        }
    }
    

}
