//
//  HomeBookDetailsCollectionCell.swift
//  Booksbee
//
//  Created by vishal on 08/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SDWebImage

class HomeBookDetailsCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgBooks: CustomImageView!
    @IBOutlet weak var lblBookTitle: UILabel!
    @IBOutlet weak var bookRate: UIButton!
    @IBOutlet weak var lblBookPrize: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()
        
    }

    func setUpUI() {
        
        self.imgBooks.layer.cornerRadius = 10
        self.imgBooks.layer.masksToBounds = true
        
        [lblBookTitle].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [lblBookPrize].forEach { (lbl) in
            lbl?.textColor = .white
            lbl?.font = themeFont(size: 14, fontname: .bold)
        }
        
        [bookRate].forEach { (btn) in
            btn?.setTitleColor(.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .bold)
        }
    }
    
    func setUpData(bookData: Bookdata) {
        
        self.imgBooks.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgBooks.sd_setImage(with: bookData.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, completed: nil)
        self.lblBookTitle.text =  bookData.bookName
        bookRate.setTitle(String(bookData.rateCount), for: .normal)
        self.lblBookPrize.text =  bookData.bookPrice
    }
    
}
