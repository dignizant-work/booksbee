//
//  HomeVC_CollectionDelegate.swift
//  Booksbee
//
//  Created by vishal on 08/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 0 {
            return self.mainVM.dictHomeData?.homeData?.banner.count ?? 0
        }
        else if collectionView.tag == 1 {
            return self.mainVM.dictHomeData?.homeData?.bestBook.count ?? 0
        }
        else if collectionView.tag == 2 {
            return self.mainVM.dictHomeData?.homeData?.bestSeller.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
        if collectionView.tag == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeOfferCollectionCell", for: indexPath) as! HomeOfferCollectionCell
            
            if let dict = self.mainVM.dictHomeData?.homeData?.banner[indexPath.row] {
                
                cell.imgBook.sd_setImage(with: dict.image.toURL(), completed: nil)
                cell.lblTitle.text = ""
                cell.lblSubTitle.text = ""
            }
            
            return cell
        }
        else if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBookDetailsCollectionCell", for: indexPath) as! HomeBookDetailsCollectionCell
            
            if let dict = self.mainVM.dictHomeData?.homeData?.bestBook[indexPath.row] {
                
                cell.imgBooks.sd_setImage(with: dict.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, context: nil)
                cell.lblBookTitle.text = dict.bookName
                cell.lblBookPrize.text = dict.price
                cell.bookRate.setTitle(dict.rates.toString(), for: .normal)
                cell.lblBookPrize.isHidden = false
                cell.bookRate.isHidden = false
            }
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBookDetailsCollectionCell", for: indexPath) as! HomeBookDetailsCollectionCell
            
            if let dict = self.mainVM.dictHomeData?.homeData?.bestSeller[indexPath.row] {
                
                cell.imgBooks.sd_setImage(with: dict.profile.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, context: nil)
                cell.lblBookTitle.text = dict.name
                cell.lblBookPrize.isHidden = true
                cell.bookRate.isHidden = true
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 0 {
            
            let _ = self.mainVM.dictHomeData?.homeData?.banner[indexPath.row]
            
        }
        else if collectionView.tag == 1 {
            
            if let dict = self.mainVM.dictHomeData?.homeData?.bestBook[indexPath.row] {
                
                let vc = homeStoryboard.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
                vc.hidesBottomBarWhenPushed = true
                vc.mainVM.bookId = dict.id
                vc.mainVM.strBookName = dict.bookName
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if collectionView.tag == 2 {
            let dict = self.mainVM.dictHomeData?.homeData?.bestSeller[indexPath.row]
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
            vc.hidesBottomBarWhenPushed = true
            vc.mainVM.isControllerChecked = .sellerAllBook
            vc.mainVM.userName = dict?.name ?? ""
            vc.mainVM.userID = dict?.userId ?? 0
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
//        let vc = homeStoryboard.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
//        vc.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 0 {
            
            return CGSize(width: collectionView.frame.size.width-25, height: 150)
        }
        else {
            
            return CGSize(width: collectionView.frame.size.width/3, height: 210)
        }
    }
    
}
