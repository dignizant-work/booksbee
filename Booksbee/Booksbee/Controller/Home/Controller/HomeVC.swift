//
//  HomeVC.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    //MARK:- Variables
    
    lazy var mainView: HomeView = {
        return self.view as! HomeView
    }()
    
    lazy var mainVM: HomeVM = {
        return HomeVM(theDelegate: self)
    }()
    
    
    //MARK:- Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainVM.getHomeDataApi { data in
            self.mainView.tblHomeData.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mainView.setUpUI()
        setUp()
    }
    
}

//MARK: SetUp
extension HomeVC {
    
    func setUp() {
        
        setUpNavigation()
    }
    
    func setUpNavigation() {
                        
        let btnLeft = UIButton()
        btnLeft.setImage(UIImage(named: "ic_futter_menu_profile_Selected_left"), for: .normal)
        btnLeft.addTarget(self, action: #selector(btnLeftNavigationAction), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnLeft)
        
        let btnRight = UIButton()
        btnRight.setImage(UIImage(named: "ic_shopping_bag"), for: .normal)
        btnRight.setBackgroundImage(UIImage(named: "ic_cart"), for: .normal)
        btnRight.addTarget(self, action: #selector(btnRightNavigationAction), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnRight)
        
        setupTransparentNavigationBar()
//        self.title = "Book Worm".localized
        appdelgate.tabBarVC.tabBar.items?[0].title = "Home".localized
    }
    
    @objc func btnLeftNavigationAction() {
        
        if isGuest{
            
            self.showGuestUserAlert()
            
        }else{
            appdelgate.tabBarVC.selectedIndex = 4
        }
        
    }
    
    @objc func btnRightNavigationAction() {
        
        if isGuest {
            showGuestUserAlert()
        }
        else {   
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "MyBooksVC") as! MyBooksVC
            vc.mainVM.isControllerCheck = .cart
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
