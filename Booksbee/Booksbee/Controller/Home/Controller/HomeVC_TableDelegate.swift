//
//  HomeVC_TableDelegate.swift
//  Booksbee
//
//  Created by vishal on 08/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.mainVM.dictHomeData?.homeData?.toJSON().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell", for: indexPath) as! HomeTableCell
        
        cell.btnViewAll.isHidden = false
        if indexPath.row == 0 {
            cell.btnViewAll.isHidden = true
            cell.lblTitle.text = "Home".localized
        }
        else if indexPath.row == 1 {
            cell.lblTitle.text = "Our Top Books".localized
        }
        else if indexPath.row == 2 {
            cell.lblTitle.text = "Best sellers".localized
        }

        cell.registerXib()
        cell.collectionHome.dataSource = self
        cell.collectionHome.delegate = self
        
        let arr = self.mainVM.dictHomeData?.homeData?.toJSON().map{$0.key} ?? [String]()
        
        for i in 0..<arr.count {
            if arr[i] == "banner" && cell.lblTitle.text == "Home".localized {
                cell.collectionHome.tag = 0
                cell.btnViewAll.tag = indexPath.row
                cell.collectionHome.reloadData()
            }
            else if arr[i] == "best_book" && cell.lblTitle.text == "Our Top Books".localized {
                cell.collectionHome.tag = 1
                cell.btnViewAll.tag = indexPath.row
                cell.collectionHome.reloadData()
            }
            else if arr[i] == "best_seller" && cell.lblTitle.text == "Best sellers".localized  {
                cell.collectionHome.tag = 2
                cell.btnViewAll.tag = indexPath.row
                cell.collectionHome.reloadData()
            }
        }
        
        cell.btnViewAll.addTarget(self, action: #selector(btnViewAllActions(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 200
        }
        else {
            return 250
        }
    }
    
    @objc func btnViewAllActions(_ sender : UIButton) {
        
        
        if sender.tag == 0 {

            let dict = self.mainVM.dictHomeData?.homeData?.banner[sender.tag]
        }
        else if sender.tag == 1 {
            if let dict = self.mainVM.dictHomeData?.homeData?.bestBook[sender.tag] {
                let vc = profileStoryboard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
                vc.mainVM.isControllerChecked = .ourTopBooks
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        else if sender.tag == 2 {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
            vc.mainVM.isControllerChecked = .viewAllBestSellers
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
}
