//
//  BookUpperDetailsView.swift
//  Booksbee
//
//  Created by iMac on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SDWebImage

class BookUpperDetailsView: UIView {

    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var btnPrice: CustomButton!
    @IBOutlet weak var lblBookName: UILabel!
    @IBOutlet weak var lblBookRatting: UILabel!
    @IBOutlet weak var vwBookRatting: HCSStarRatingView!
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var vwUserRatting: HCSStarRatingView!
    
    @IBOutlet weak var vwStartAuction: UIView!
    @IBOutlet weak var lblStartAuction: UILabel!
    @IBOutlet weak var lblStartAuctionValue: UILabel!
    
    @IBOutlet weak var vwEndAuctionTime: UIView!
    @IBOutlet weak var lblEndAuction: UILabel!
    @IBOutlet weak var lblEndAuctionValue: UILabel!
    
    //MARK:- Variable
    var releaseDate = Date()
    
    func setUpUI(){
        
        imgBook.layer.cornerRadius = 8
        imgBook.layer.masksToBounds = true
        
        [lblBookName,lblUserName].forEach { (lbl) in
            lbl?.textColor = .black
        }
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.bounds.width/2
        imgBook.layer.masksToBounds = true
        
        lblBookName.font = themeFont(size: 20, fontname: .medium)
        
        lblBookRatting.textColor = .lightGray
        lblBookRatting.font = themeFont(size: 13, fontname: .regular)
        
        lblUserName.font = themeFont(size: 22, fontname: .medium)
        
        btnPrice.backgroundColor = UIColor.appGoldenColor
        btnPrice.titleLabel?.font = themeFont(size: 20, fontname: .medium)
        btnPrice.setTitleColor(.white, for: .normal)
        
        [lblEndAuction, lblStartAuction].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = themeFont(size: 18, fontname: .medium)
            lbl?.text = "End Auction".localized
        }
        
        lblStartAuction.text = "Start Auction".localized
        
        [lblEndAuctionValue, lblStartAuctionValue].forEach { (lbl) in
            lbl?.textColor = .red
            lbl?.font = themeFont(size: 18, fontname: .bold)
        }
    }
    
    func setupData(data:BookDetails){
        
        self.lblBookName.text = data.bookName
        self.lblUserName.text = data.author
        self.btnPrice.setTitle(data.bookPrice, for: .normal)
        
        let totalRateCount = "\(data.rateCount)" + " " + "Reviews".localized
        self.lblBookRatting.text = "\(data.rates.toString()) (\(totalRateCount))"
        
        self.imgBook.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgBook.sd_setImage(with: data.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, completed: nil)
        
        self.vwBookRatting.value = data.rates
        self.vwBookRatting.accurateHalfStars = true
        self.vwBookRatting.allowsHalfStars = true
        
        self.imgUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgUserProfile.sd_setImage(with: data.ownerImage.toURL(), placeholderImage: UIImage(named: "ic_my_profile_nig_splash_holder"), options: .lowPriority, completed: nil)
        self.lblUserName.text = data.ownerName
        
        self.vwEndAuctionTime.isHidden = true
        self.vwStartAuction.isHidden = true
        
        if data.saleType == 0 {
            
            self.vwEndAuctionTime.isHidden = false
            self.vwStartAuction.isHidden = false
            
            self.lblStartAuctionValue.text = data.startDateTime
            self.lblEndAuctionValue.text = data.endDateTime
            
//            let releaseDateFormatter = DateFormatter()
//            releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//
//            if !data.endDateTime.toTrim().isEmpty {
//
//                releaseDate = releaseDateFormatter.date(from:data.endDateTime)!
//                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDownDate), userInfo: nil, repeats: true)
//            }
        }
    }
 
    @objc func countDownDate() {
        
        self.vwEndAuctionTime.isHidden = false
        
        let date = Date()
        let calendar = Calendar.current

        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: date, to: releaseDate)
        
        let days = "\(diffDateComponents.day ?? 0)".count == 1 ? "0\(diffDateComponents.day ?? 0)" : "\(diffDateComponents.day ?? 0)"
        let hours = "\(diffDateComponents.hour ?? 0)".count == 1 ? "0\(diffDateComponents.hour ?? 0)" : "\(diffDateComponents.hour ?? 0)"
        let minutes = "\(diffDateComponents.minute ?? 0)".count == 1 ? "0\(diffDateComponents.minute ?? 0)" : "\(diffDateComponents.minute ?? 0)"
        let seconds = "\(diffDateComponents.second ?? 0)".count == 1 ? "0\(diffDateComponents.second ?? 0)" : "\(diffDateComponents.second ?? 0)"
        
        let countdown = "\(days) Days \(hours) : \(minutes) : \(seconds)"
        self.lblEndAuctionValue.text = countdown
    }
}
