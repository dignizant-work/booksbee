//
//  BookUperDetailsVc.swift
//  Booksbee
//
//  Created by iMac on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class BookUpperDetailsVc: UIViewController {

    //MARK:Variables
    lazy var mainView: BookUpperDetailsView = {
        return self.view as! BookUpperDetailsView
    }()
    
    lazy var mainVM: BookUpperDetailsVM = {
        return BookUpperDetailsVM(theController: self)
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()

        // Do any additional setup after loading the view.
    }
    
}
//MARK: - Set dataa

extension BookUpperDetailsVc{
    
    func setupData(data: BookDetails?){
        
        if let dataValue = data{
            
            self.mainView.setupData(data: dataValue)
                        
        }
        
    }
    
}
