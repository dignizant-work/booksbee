//
//  BookBidingView.swift
//  Booksbee
//
//  Created by iMac on 18/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class BookBidingView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var lblBookBiding: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblBookBodingSubDetails: UILabel!
    @IBOutlet weak var lblEnteryouramount: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    func setUpUI(){
        
        vwBG.clipsToBounds = true
        vwBG.layer.cornerRadius = 30
        vwBG.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        lblBookBiding.font = themeFont(size: 16, fontname: .bold)
        lblBookBiding.textColor = .black
        
        lblBookBodingSubDetails.font = themeFont(size: 12, fontname: .medium)
        lblBookBodingSubDetails.textColor = .gray
        
        lblEnteryouramount.font = themeFont(size: 13, fontname: .medium)
        lblEnteryouramount.textColor = .black

        txtAmount.font = themeFont(size: 16, fontname: .medium)
        txtAmount.textColor = .appTitleTextColor
        txtAmount.placeholderColor(.appDetailTextColor)
        if getLanguage() == arabic {
            txtAmount.textAlignment = .right
        }
        
        btnSubmit.titleLabel?.font = themeFont(size: 11, fontname: .medium)
        btnSubmit.setTitleColor(.black, for: .normal)
        
        lblBookBiding.text = "Book Biding".localized
        lblEnteryouramount.text = "Enter your amount".localized
        txtAmount.placeholder = "Enter here".localized
        
        [btnSubmit].forEach { (btn) in
            btn?.setTitleColor(.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .medium)
            btn?.backgroundColor = .appTitleTextColor
            btn?.setTitle("Submit", for: .normal)
        }
        txtAmount.addDoneOnKeyboardWithTarget(self, action: #selector(txtFieldHide))
    }

    @objc func txtFieldHide() {
        self.txtAmount.resignFirstResponder()
    }
    
}
