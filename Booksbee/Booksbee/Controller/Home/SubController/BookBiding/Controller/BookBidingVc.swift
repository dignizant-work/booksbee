//
//  BookBidingVc.swift
//  Booksbee
//
//  Created by iMac on 18/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class BookBidingVc: UIViewController {
    
    //MARK:Variables
    lazy var mainView: BookBidingView = {
        return self.view as! BookBidingView
    }()
    
    lazy var mainVM: BookBidingVM = {
        return BookBidingVM(theController: self)
    }()

        
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()

        // Do any additional setup after loading the view.
    }

}

//MARK: - IBAction
extension BookBidingVc{

    @IBAction func btnCloseTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSubmitTapped(_ sender: Any) {
        if self.mainView.txtAmount.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter your Biding Amout".localized)
            return
        }
        else {
            
            let param = ["book_id" : self.mainVM.selectBookId,
                         "price" : self.mainView.txtAmount.text!,
                         "timezone":currentTimeZone] as [String:Any]
            
            self.mainVM.bookBidingApi(param: param) { response in
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
