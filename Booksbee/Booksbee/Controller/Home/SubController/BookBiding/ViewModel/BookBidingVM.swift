//
//  BookBidingVM.swift
//  Booksbee
//
//  Created by iMac on 18/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import SwiftyJSON

class BookBidingVM {
    
    var theController = BookBidingVc()
    var selectBookId = 0
    
    init(theController: BookBidingVc) {
        self.theController = theController
    }
    
    
}

extension BookBidingVM {
    
    func bookBidingApi(param: [String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = bookBidingURL
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
}
