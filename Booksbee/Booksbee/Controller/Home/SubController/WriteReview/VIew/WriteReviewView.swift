//
//  IntroductionView.swift
//  Booksbee
//
//  Created by vishal on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import HCSStarRatingView
import SDWebImage

class WriteReviewView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var btnPrice: CustomButton!
    @IBOutlet weak var lblBookName: UILabel!
    @IBOutlet weak var lblBookRatting: UILabel!
    @IBOutlet weak var vwBookRatting: HCSStarRatingView!
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var vwUserRatting: HCSStarRatingView!
    
    
    @IBOutlet weak var vwReview: HCSStarRatingView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var lblEnterValue: UILabel!
    @IBOutlet weak var txtVwDescription: UITextView!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    func setUpUI() {
        
        [txtTitle].forEach { (txt) in
            txt?.placeholderColor(.appDetailTextColor)
            txt?.textColor = .appTitleTextColor
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.placeholder = "Title".localized
        }
        
        [txtVwDescription].forEach { (txtvw) in
            txtvw?.textColor = .appTitleTextColor
            txtvw?.font = themeFont(size: 16, fontname: .medium)
        }
        
        [lblEnterValue].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.text = "Write your Review".localized

        }
        
        [btnSubmit].forEach { (btn) in
            btn?.setTitle("Submit".localized, for: .normal)
            btn?.backgroundColor = .appTitleTextColor
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .medium)
            btn?.setTitleColor(.white, for: .normal)
        }
        
        imgBook.layer.cornerRadius = 8
        imgBook.layer.masksToBounds = true
        
        [lblBookName,lblUserName].forEach { (lbl) in
            lbl?.textColor = .black
        }
        
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.bounds.width/2
        imgBook.layer.masksToBounds = true
        
        lblBookName.font = themeFont(size: 20, fontname: .medium)
        
        lblBookRatting.textColor = .lightGray
        lblBookRatting.font = themeFont(size: 13, fontname: .regular)
        
        lblUserName.font = themeFont(size: 22, fontname: .medium)
        
        btnPrice.backgroundColor = UIColor.appGoldenColor
        btnPrice.titleLabel?.font = themeFont(size: 20, fontname: .medium)
        btnPrice.setTitleColor(.white, for: .normal)
        
        if getLanguage() == arabic {
            txtTitle.textAlignment = .right
            txtVwDescription.textAlignment = .right
            lblEnterValue.textAlignment = .right
        }
        
    }
    
    func checkValidation() -> Bool {
        
        if self.vwReview.value == 0{
         
            makeToast(strMessage: "Please select star ratting".localized)
            return false
        }
        else if txtTitle.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter title".localized)
            return false
        }
        else if txtVwDescription.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter description".localized)
            return false
        }
        return true
    }

    
    func setupData(data:BookDetails){
        
        self.lblBookName.text = data.bookName
        self.lblUserName.text = data.author
        self.btnPrice.setTitle(data.bookPrice, for: .normal)
        
        let totalRateCount = "\(data.rateCount)" + " " + "Reviews".localized
        self.lblBookRatting.text = "\(data.rates.toString()) (\(totalRateCount))"
        
        self.imgBook.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgBook.sd_setImage(with: data.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, completed: nil)
        
        self.vwBookRatting.value = data.rates
        self.vwBookRatting.accurateHalfStars = true
        self.vwBookRatting.allowsHalfStars = true
        
        self.imgUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgUserProfile.sd_setImage(with: data.ownerImage.toURL(), placeholderImage: UIImage(named: "ic_my_profile_nig_splash_holder"), options: .lowPriority, completed: nil)
        self.lblUserName.text = data.ownerName
    }

    
}
