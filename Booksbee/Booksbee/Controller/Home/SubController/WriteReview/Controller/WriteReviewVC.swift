//
//  WriteReviewVC.swift
//  Booksbee
//
//  Created by vishal on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import HCSStarRatingView

class WriteReviewVC: UIViewController {

    //MARK: Variables
    lazy var mainView: WriteReviewView = {
        return self.view as! WriteReviewView
    }()
    
    lazy var mainVM: WriteReviewVM = {
        return WriteReviewVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad() 
     
        setUp()
        self.mainView.setUpUI()
        self.mainVM.bookDetailsApi()
    }

    func setUp() {
        
        self.setupNavigationbarBackButton(titleText: "Write your Review")
        self.navigationController?.navigationBar.barTintColor = .appBackgroundColor
        let statusBarFrame = UIApplication.shared.statusBarFrame
        let statusBarView = UIView(frame: statusBarFrame)
        self.view.addSubview(statusBarView)
        statusBarView.backgroundColor = .appBackgroundColor
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if mainView.checkValidation(){
            writeReViewAPI()
        }
    }
    
}

//MARK: TextView Delegate
extension WriteReviewVC : UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        self.mainView.lblEnterValue.isHidden = self.mainView.txtVwDescription.text == "" ? false : true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
}

//MARK: WriteReview API Calling

extension WriteReviewVC{
    
    func writeReViewAPI(){
        
        let dict = [
                    "book_id":self.mainVM.bookId,
                    "rating": self.mainView.vwReview.value ,
                    "title": self.mainView.txtTitle.text ?? "",
                    "review_desc": self.mainView.txtVwDescription.text ?? "",
                    ] as [String : Any]
        
        self.mainVM.writeReviewAPI(param: dict) { (data) in
                        
            self.mainVM.handlerReviewRefreshAPI()
            
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func setupData(data:BookDetails?){
        
        if let dataValue = data{
            self.mainView.setupData(data: dataValue)
        }
    }
}

