//
//  IntroductionVM.swift
//  Booksbee
//
//  Created by vishal on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class WriteReviewVM {
    
    //MARK: Variables
    var theController = WriteReviewVC()
    
    init(theController: WriteReviewVC) {
        self.theController = theController
    }
    
    var bookDetails = BookDetailsModel(JSON: JSON().dictionaryValue)
    var bookId = 0
    var handlerReviewRefreshAPI: () -> Void = {}
    
}

//MARK: - API calling

extension WriteReviewVM{
    
    func writeReviewAPI(param:[String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = writeReviewURL
        
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                
                let dict = JSON(data!)
                print("Dict:\(dict)")
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
            
        }
        
    }
    
    func bookDetailsApi() {
        
        let param = ["book_id": self.bookId]
        
        let url = bookDetailsURL
        
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                
                let dict = JSON(data!)
                print("Dict:\(dict)")
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    self.bookDetails = nil
                    self.bookDetails = BookDetailsModel(JSON: dict.dictionaryObject!)
                    
                    self.theController.setupData(data: self.bookDetails?.bookDetails)
                    
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
            
        }
        
    }
}

