//
//  ReviewVC.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import MXScroll

class ReviewVC: UIViewController {

    //MARK:Variables
    lazy var mainView: ReviewView = {
        return self.view as! ReviewView
    }()
    
    lazy var mainVM: ReviewVM = {
        return ReviewVM(theController: self)
    }()
    var scrollDelegateFunc: ((UIScrollView)->Void)?
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainVM.getReviewListApi(page: 1) { data in
            self.mainView.tblReview.reloadData()
        }
        self.setUpPulltorefresh()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mainView.setUpUI()
        
    }
    
    @objc func updateReviewList(){
        
        self.mainVM.getReviewListApi(page: self.mainVM.dictReviewList?.reviewData?.currentPage ?? 0) { data in
            self.mainView.tblReview.reloadData()
        }
        self.setUpPulltorefresh()

    }
    
}

//MARK: - IBAction method

extension ReviewVC{

    @IBAction func btnWriteReviewTapped(_ sender: UIButton) {
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "WriteReviewVC") as! WriteReviewVC
        vc.mainVM.bookId = self.mainVM.bookId
        vc.mainVM.handlerReviewRefreshAPI = {[weak self] in
            self?.mainVM.handlerCallBookUpperDetails()
            self?.updateReviewList()
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK: - MXScroll Delegate

extension ReviewVC:MXViewControllerViewSource{
    func viewForMixToObserveContentOffsetChange() -> UIView {
        return self.mainView.tblReview
    }
}


extension ReviewVC{
    
    func setUpPulltorefresh() {
        
        self.mainVM.activityIndicator = UIRefreshControl()
        self.mainVM.activityIndicator.tintColor = .appGoldenColor
        self.mainVM.activityIndicator.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblReview.addSubview(self.mainVM.activityIndicator)
    }
    
    @objc func pullToRefresh() {
        self.mainVM.nxtPage = 1
        self.mainVM.getReviewListApi(page: self.mainVM.nxtPage) { data in
            self.mainView.tblReview.reloadData()
        }
    }

    
}

