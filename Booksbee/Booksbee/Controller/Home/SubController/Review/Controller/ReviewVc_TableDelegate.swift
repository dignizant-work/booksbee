//
//  ReviewVc_TableDelegate.swift
//  Booksbee
//
//  Created by iMac on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension ReviewVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainVM.dictReviewList?.reviewData?.reviewDataList?.count ?? 0 == 0 || self.mainVM.dictReviewList?.reviewData?.reviewDataList?.count == nil{
            
            setTableViewEmptyMessage(self.mainVM.dictReviewList?.message ?? "", tableView: tableView)
            return 0
        }
        
        tableView.backgroundView = nil
        return mainVM.dictReviewList?.reviewData?.reviewDataList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTblCell", for: indexPath) as! ReviewTblCell
        
        if let dict = self.mainVM.dictReviewList?.reviewData?.reviewDataList?[indexPath.row]{
            cell.setupReviewCell(reviewData: dict)
        }
        return cell

    }
    
}


