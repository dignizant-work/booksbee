//
//  ReviewTblCell.swift
//  Booksbee
//
//  Created by iMac on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SDWebImage

class ReviewTblCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var vwStarRatting: HCSStarRatingView!
    @IBOutlet weak var lblRattingValue: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
        
        lblName.textColor = .black
        lblName.font = themeFont(size: 16, fontname: .medium)
        
        lblDate.textColor = .black
        lblDate.font = themeFont(size: 12, fontname: .regular)
        
        lblRattingValue.textColor = .lightGray
        lblRattingValue.font = themeFont(size: 14, fontname: .regular)
        
        lblDescription.textColor = .black
        lblDescription.font = themeFont(size: 12, fontname: .regular)
        
        vwStarRatting.accurateHalfStars = true
        vwStarRatting.allowsHalfStars = true


        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupReviewCell(reviewData: ReviewDataList){
        
        self.lblName.text = reviewData.name
        self.lblDescription.text = reviewData.description
        self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgProfile.sd_setImage(with: reviewData.image.toURL(), placeholderImage: UIImage(named: "ic_futter_menu_profile_splash_holder"), options: .lowPriority, completed: nil)
        self.vwStarRatting.value = reviewData.rate
        self.lblDate.text = ""
        self.lblRattingValue.text =  ""
    }
    
}
