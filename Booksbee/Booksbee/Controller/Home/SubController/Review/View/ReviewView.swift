//
//  ReviewView.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit


class ReviewView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var tblReview: UITableView!
    @IBOutlet weak var btnWriteReview: CustomButton!
    
    //MARK: - SetupUI
    func setUpUI() {
        
        btnWriteReview.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        btnWriteReview.setTitle("Write Review".localized, for: .normal)
        
        registerXib()
        self.tblReview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        
    }
    
    func registerXib() {
        tblReview.register(UINib(nibName: "ReviewTblCell", bundle: nil), forCellReuseIdentifier: "ReviewTblCell")
    }

}
