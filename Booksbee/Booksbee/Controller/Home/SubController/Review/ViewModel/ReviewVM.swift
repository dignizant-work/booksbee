//
//  ReviewVM.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ReviewVM {
    
    var theController = ReviewVC()
    
    init(theController: ReviewVC) {
        self.theController = theController
    }
    
    var dictReviewList = ReviewListModel(JSON: JSON().dictionaryValue)
    var activityIndicator = UIRefreshControl()
    var nxtPage = 1
    
    var handlerCallBookUpperDetails: () -> Void = {}
    
    var bookId = 6
}


//MARK: - API calling

extension ReviewVM{
    

    func getReviewListApi(page: Int, completionHandlor:@escaping(JSON)->Void) {
        
        let param = ["book_id": self.bookId]
        
        var url = ""
        if page == 1  {
            url = reviewListsURL
            showIndicator()
        }
        else {
            url = reviewListsURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            self.activityIndicator.endRefreshing()
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 {
//                    makeToast(strMessage: dict["message"].stringValue)
                    self.dictReviewList = nil
                    self.dictReviewList = ReviewListModel(JSON: dict.dictionaryObject!)
                    completionHandlor(dict)
                }
                else {
                    if page == 1 {
                        self.dictReviewList = nil
                        self.dictReviewList = ReviewListModel(JSON: dict.dictionaryObject!)
                        
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = ReviewDataList(JSON: bookData.dictionaryObject!)
                            self.dictReviewList?.reviewData?.reviewDataList?.append(data!)
                        }
                    }
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
}
