//
//  BookDetailVC.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import MXScroll
import RxCocoa
import RxSwift

class BookDetailVC: UIViewController {

    //MARK: Variables
    lazy var mainView: BookDetailView = {
        return self.view as! BookDetailView
    }()
    
    lazy var mainVM: BookDetailVM = {
        return BookDetailVM(theController: self)
    }()
    
    var mx: MXViewController<MSSegmentControl>!
    var introVC: IntroductionVC!
    var reviewVC: ReviewVC!
    
    @IBOutlet var containerView: UIView!
    
    //MARK: Controller Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.setupNavigationbarBackButton(titleText: self.mainVM.strBookName)
        self.mainVM.bookDetailsApi{
            self.setUpNavigationShareButton()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
}

//MARK: - MXScroll Full setup

extension BookDetailVC{
    
    func setupCaregotyDetails(){
        
        let objHeaderDetails = homeStoryboard.instantiateViewController(withIdentifier: "BookUpperDetailsVc") as! BookUpperDetailsVc
        objHeaderDetails.setupData(data: self.mainVM.bookDetails?.bookDetails)
        
        var headerTitle = [String]()
        var mx = MXViewController<MSSegmentControl>()
        
        headerTitle = ["Introduction".localized,"Reviews".localized]
        
        let introVc = homeStoryboard.instantiateViewController(withIdentifier: "IntroductionVC") as! IntroductionVC
//        introVc.mainVM.dictBookdata = self.mainVM.bookDetails?.bookDetails
        introVc.setupData(data: self.mainVM.bookDetails?.bookDetails)
        
        let rwvc = homeStoryboard.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
        rwvc.mainVM.handlerCallBookUpperDetails = {[weak self] in
            self?.mainVM.bookDetailsApi{}
        }
        rwvc.mainVM.bookId = self.mainVM.bookId
        
        //        if getLanguage() == arabic{
        
        //            let segment = MSSegmentControl(sectionTitles: headerTitle.reversed())
        //
        //            setupSegment(segmentView: segment)
        //
        //            mx = MXViewController<MSSegmentControl>.init(headerViewController: objHeaderDetails, segmentControllers: [rwvc,introVc], segmentView: segment)
        //
        //            segment.selectedSegmentIndex = 1
        
        //        }
        //        else{
        //
        let segment = MSSegmentControl(sectionTitles: headerTitle)
        
        setupSegment(segmentView: segment)
        
        mx = MXViewController<MSSegmentControl>.init(headerViewController: objHeaderDetails, segmentControllers: [introVc,rwvc], segmentView: segment)
        
        //        }
        
        mx.headerViewOffsetHeight = 0
        mx.shouldScrollToBottomAtFirstTime = false
        mx.showsHorizontalScrollIndicator = false
        mx.showsVerticalScrollIndicator = false
        mx.segmentViewHeight = 40
        
        addChild(mx)
        
        self.mainView.contentView.addSubview(mx.view)
        mx.view.frame = self.mainView.contentView.bounds
        mx.didMove(toParent: self)
        
    }
    
    func setupSegment(segmentView: MSSegmentControl) {
        
        segmentView.borderType = .bottom
        segmentView.borderColor = .lightGray
        segmentView.backgroundColor = UIColor.white
        segmentView.selectionIndicatorColor = UIColor.appGoldenColor
        segmentView.selectionIndicatorHeight = 1
        
        segmentView.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: themeFont(size: 14, fontname: .bold)]
        segmentView.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: themeFont(size: 14, fontname: .regular)]
        
        segmentView.segmentWidthStyle = .fixed
        segmentView.selectionStyle = .fullWidth
        segmentView.verticalDividerWidth = 0.7
        segmentView.verticalDividerColor = .lightGray
        segmentView.verticalDividerEnabled = true
        segmentView.selectedSegmentIndex = 0
        
    }
}


//MARK: Button Action
extension BookDetailVC {
        
    func setUpNavigationShareButton() {
        
        let btnShare = UIButton()
        btnShare.setImage(UIImage(named: "ic_share_btn"), for: .normal)
        btnShare.addTarget(self, action: #selector(btnShareAction(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnShare)
    }
    
    @objc func btnShareAction(_ sender: UIButton) {
     
        let text = self.mainVM.bookDetails?.bookDetails?.bookName.capitalized
        
        let myWebsite = NSURL(string:"https://apps.apple.com/us/app")
        let textShare = [ text ?? "", myWebsite ?? NSURL() ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }

}
