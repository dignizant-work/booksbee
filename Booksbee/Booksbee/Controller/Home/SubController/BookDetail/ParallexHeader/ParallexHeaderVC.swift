//
//  ParallaxVC.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import MXScroll

class ParallexHeaderVC: UIViewController
{
    @IBOutlet var vwTopHeader: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        vwTopHeader.backgroundColor = .red
    }
}

extension ParallexHeaderVC: MXViewControllerViewSource {
    func headerViewForMixObserveContentOffsetChange() -> UIView? {
        return vwTopHeader
    }
}
