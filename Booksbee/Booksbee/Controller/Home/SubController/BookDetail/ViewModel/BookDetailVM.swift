//
//  BookDetailVM.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class BookDetailVM {
    
    //MARK: Variables
    var theController = BookDetailVC()
    
    init(theController: BookDetailVC) {
        self.theController = theController
    }
    
    var bookDetails = BookDetailsModel(JSON: JSON().dictionaryValue)
    var bookId = 0
    var strBookName = ""
    
    var isAlreadySetup = false
    
}

//MARK: - API calling

extension BookDetailVM{
    
    func bookDetailsApi(_ completionHandlor:@escaping()->Void) {
        
        let param = ["book_id": self.bookId]
        
        let url = bookDetailsURL
        
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                
                let dict = JSON(data!)
                print("Dict:\(dict)")
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    self.bookDetails = nil
                    self.bookDetails = BookDetailsModel(JSON: dict.dictionaryObject!)
                    self.theController.setupCaregotyDetails()
                    completionHandlor()
                }
                
            }
            else {
                makeToast(strMessage: error ?? "")
            }
            
        }
        
    }
    
}
