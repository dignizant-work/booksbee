//
//  NotificationVc_TableDelegate.swift
//  Booksbee
//
//  Created by iMac on 18/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension NotificationVc: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainVM.dictNotificationData?.bookDataModel?.myOrdeData.count ?? 0 == 0 || self.mainVM.dictNotificationData?.bookDataModel?.myOrdeData == nil {
            
            setTableViewEmptyMessage(self.mainVM.dictNotificationData?.message ?? "", tableView: tableView)
            return 0
        }
    
        tableView.backgroundView = nil
        return self.mainVM.dictNotificationData?.bookDataModel?.myOrdeData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTblCell", for: indexPath) as! NotificationTblCell
        
        cell.lblSubDetails.isHidden = true
        cell.vwRattingBook.isHidden = true
        
        if let dict = self.mainVM.dictNotificationData?.bookDataModel?.myOrdeData[indexPath.row] {
            cell.lblBookName.text = dict.notificationText
            cell.imgBookCover.sd_setImage(with: dict.bookImage.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder_details_page"), options: .lowPriority, context: nil)
        }
        
//        if indexPath.row % 2 == 0{
//            cell.lblSubDetails.isHidden = true
//            cell.vwRattingBook.isHidden = false
//        }else{
//            cell.lblSubDetails.isHidden = false
//            cell.vwRattingBook.isHidden = true
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let dict = self.mainVM.dictNotificationData?.bookDataModel?.myOrdeData[indexPath.row] {
            
            if dict.notificationType == 1 {
                
                if dict.orderId == 0 {
                    makeToast(strMessage: "Data is not available")
                    return
                }
                
                let vc = profileStoryboard.instantiateViewController(withIdentifier: "BidDetailsVC") as! BidDetailsVC
                vc.mainVM.bookId = dict.bookId
                self.navigationController?.pushViewController(vc, animated: false)
            }
            if dict.notificationType == 2 {
                
            }
            if dict.notificationType == 3 {
                
                if dict.bookId == 0 {
                    makeToast(strMessage: "Data is not available")
                    return
                }
                
                let vc = profileStoryboard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
                vc.mainVM.orderId = dictPushData["order_id"].intValue
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1) {
            if tableView.visibleCells.contains(cell){
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && self.mainVM.dictNotificationData?.bookDataModel?.lastPage != self.mainVM.nxtPage {
                    self.mainVM.nxtPage += 1
                    self.showTableFooterIndicater(tableview: tableView)
                    self.mainVM.getNotifacationListAPI(page: self.mainVM.nxtPage) { data in
                        tableView.tableFooterView?.isHidden = true
                        tableView.reloadData()
                    }
                }
            }
        }
    }
}

