//
//  NotificationVc.swift
//  Booksbee
//
//  Created by iMac on 18/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationVc: UIViewController {
    
    //MARK:Variables
    lazy var mainView: NotificationView = {
        return self.view as! NotificationView
    }()
    
    lazy var mainVM: NotificationVM = {
        return NotificationVM(theController: self)
    }()

    //MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        redirectPushData()
        setUpPulltorefresh()
        self.mainVM.getNotifacationListAPI(page: 1) { data in
            self.mainView.tblNotification.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarBackButton(titleText: "Notification".localized)
    }

    func redirectPushData() {
        
        if isBookBidWinner {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "BidDetailsVC") as! BidDetailsVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if isOrderStatusNotification {
            isOrderStatusNotification = false
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
            vc.mainVM.orderId = dictPushData["order_id"].intValue
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK: SetUp
extension NotificationVc {
    
    func setUpPulltorefresh() {
        
        self.mainVM.activityIndicator = UIRefreshControl()
        self.mainVM.activityIndicator.tintColor = .appGoldenColor
        self.mainVM.activityIndicator.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblNotification.addSubview(self.mainVM.activityIndicator)
    }
    
    @objc func pullToRefresh() {
        
        self.mainVM.nxtPage = 1
        
        self.mainVM.getNotifacationListAPI(page: self.mainVM.nxtPage) { data in
            self.mainVM.activityIndicator.endRefreshing()
            self.mainView.tblNotification.reloadData()
        }
    }
}
