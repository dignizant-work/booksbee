//
//  NotificationVM.swift
//  Booksbee
//
//  Created by iMac on 18/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import SwiftyJSON


class NotificationVM {
    
    var theController = NotificationVc()
    var dictNotificationData: BookModel? = nil
    var activityIndicator = UIRefreshControl()
    var nxtPage = 1
    
    init(theController: NotificationVc) {
        self.theController = theController
    }
    
    func getNotifacationListAPI(page: Int, completionHandlor:@escaping(JSON)->Void) {
        
        var url = ""
        if page == 1  {
            url = notificationListURL
            showIndicator()
        }
        else {
            url = notificationListURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Dict:-\(dict)")
                
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 && page == 1 {
                    self.dictNotificationData = BookModel(JSON: dict.dictionaryObject!)
                }
                else {
                    if page == 1 {
                        self.dictNotificationData = nil
                        self.dictNotificationData = BookModel(JSON: dict.dictionaryObject!)
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = MyOrdeData(JSON: bookData.dictionaryObject!)
                            self.dictNotificationData?.bookDataModel?.myOrdeData.append(data!)
                        }
                    }
                }
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
