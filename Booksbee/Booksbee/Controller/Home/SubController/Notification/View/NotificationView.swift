//
//  NotificationView.swift
//  Booksbee
//
//  Created by iMac on 18/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class NotificationView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var tblNotification: UITableView!
    
    func setUpUI(){
        registerXib()
    }
    
    func registerXib() {
        tblNotification.register(UINib(nibName: "NotificationTblCell", bundle: nil), forCellReuseIdentifier: "NotificationTblCell")
    }
    
}
