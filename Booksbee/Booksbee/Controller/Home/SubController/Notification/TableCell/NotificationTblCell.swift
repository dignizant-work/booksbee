//
//  NotificationTblCell.swift
//  Booksbee
//
//  Created by iMac on 18/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import HCSStarRatingView

class NotificationTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var imgBookCover: CustomImageView!
    @IBOutlet weak var lblBookName: UILabel!
    @IBOutlet weak var lblSubDetails: UILabel!
    @IBOutlet weak var vwRattingBook: HCSStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblBookName].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
    }
    
    override func layoutSubviews() {
        
        imgBookCover.layer.cornerRadius = 5
        imgBookCover.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
