//
//  IntroductionView.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit


class IntroductionView: UIView {
    
    //MARK: Outlets
    
    //Uninstall from Stroyboard
    @IBOutlet weak var tblIntroduction: UITableView!
//    @IBOutlet weak var vwtblFooter: UIView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var lblIntroduction: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnBidNow: CustomButton!
    
    //MARK: - SetupUI
    
    func setUpUI() {
        
        btnBidNow.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        btnBidNow.setTitle("Bid Now".localized, for: .normal)

        registerXib()
//        tblIntroduction.tableFooterView = vwtblFooter
    }
    
    func registerXib() {
        tblIntroduction.register(UINib(nibName: "IntroductionTblCell", bundle: nil), forCellReuseIdentifier: "IntroductionTblCell")
    }

    func setupData(data:BookDetails){
        
        self.lblIntroduction.text = data.description
        
//        0 = aution , 1 = direct sale
//
//        0 hoy to bid nu button & cart nu hide
//        1 hoy to bid nu hide & cart nu show

        if data.saleType == 0 {
            self.btnCart.isHidden = true
        }else if data.saleType == 1{
            self.btnBidNow.isHidden = true
        }
        
        if data.isCartExists == 1 {
            btnCart.isSelected = true
        }
        else {
            btnCart.isSelected = false
        }

        
    }

    
    
}
