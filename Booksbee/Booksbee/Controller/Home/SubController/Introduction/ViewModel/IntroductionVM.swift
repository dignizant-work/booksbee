//
//  IntroductionVM.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class IntroductionVM {
    
    var theController = IntroductionVC()
    
    init(theController: IntroductionVC) {
        self.theController = theController
    }
    var dictBookdata: BookDetails? = nil
    
    var bookId = 0
    
}


extension IntroductionVM {
    
    func addToCartsApi(completionHandlor:@escaping(JSON)->Void) {
        
        let url = addToCartURL
        showIndicator()
        let param = ["book_id":bookId] as [String:Any]
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func removeItemInCartListApi(bookId:String, completionHandlor:@escaping(JSON)->Void) {
        
        let param: [String:Any] = ["book_id":bookId]
        
        let url = itemRemovefromCartURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
//                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
