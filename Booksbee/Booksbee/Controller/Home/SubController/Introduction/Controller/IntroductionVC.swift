//
//  IntroductionVC.swift
//  Booksbee
//
//  Created by vishal on 09/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import MXScroll

class IntroductionVC: UIViewController {
    
    //MARK:- Variables
    lazy var mainView: IntroductionView = {
        return self.view as! IntroductionView
    }()
    
    lazy var mainVM: IntroductionVM = {
        return IntroductionVM(theController: self)
    }()
    
    var scrollDelegateFunc: ((UIScrollView)->Void)?
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }

}

//MARK: Setup
extension IntroductionVC {
    
    func setupData(data: BookDetails?){
        
        if let dataValue = data {
            self.mainView.setupData(data: dataValue)
            self.mainVM.bookId = dataValue.id
        }
    }
}

//MARK: Button Action
extension IntroductionVC {
    
    @IBAction func btnBidNowTapped(_ sender: UIButton) {
        
        if isGuest{
            showGuestUserAlert()
        }else{
            let vc = homeStoryboard.instantiateViewController(withIdentifier: "BookBidingVc") as! BookBidingVc
            vc.mainVM.selectBookId = self.mainVM.bookId
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnCartTapped(_ sender: UIButton) {
        
        if isGuest {
            showGuestUserAlert()
        }
        else {
            
            self.showAlert("", "Are you sure you want to add this book in to cart?".localized, "Add".localized, "Cancel".localized) {
                self.mainVM.addToCartsApi { response in
                    sender.isSelected = !sender.isSelected
                }
            }
            
           /* if self.mainVM.dictBookdata?.isCartExists == 1 {
                self.showAlert("", "Are you sure you want to remove this item?".localized, "Remove".localized, "Cancel".localized) {
                    self.mainVM.removeItemInCartListApi(bookId: "\(self.mainVM.dictBookdata?.id ?? 0)") { response in
                        sender.isSelected = false
                    }
                }
            }
            else {
                self.showAlert("", "Are you sure you want to add this book in to cart?".localized, "Add".localized, "Cancel".localized) {
                    self.mainVM.addToCartsApi { response in
                        sender.isSelected = !sender.isSelected
                    }
                }
            }*/
        }
    }
    
    @IBAction func btnDownloadTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnShareTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}


//MARK: - MXScroll Delegate

extension IntroductionVC:MXViewControllerViewSource{
    func viewForMixToObserveContentOffsetChange() -> UIView {
        return self.mainView.mainScrollView
    }
}
