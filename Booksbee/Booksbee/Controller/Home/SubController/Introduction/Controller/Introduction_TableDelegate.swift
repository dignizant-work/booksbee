//
//  Introduction_TableDelegate.swift
//  Booksbee
//
//  Created by iMac on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension IntroductionVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "IntroductionTblCell", for: indexPath) as! IntroductionTblCell
        
        return cell
    }
    
}

