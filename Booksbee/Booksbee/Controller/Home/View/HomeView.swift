//
//  HomeView.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class HomeView: UIView {
    
    //MARK:- Outlets
    @IBOutlet weak var imgTopBG: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblHomeData: UITableView!
    @IBOutlet weak var tblHomeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTitleDdescription: UILabel!
    
    
    func setUpUI() {
        
        [lblTitleDdescription].forEach { (lbl) in
            lbl?.font = themeFont(size: 24, fontname: .medium)
            lbl?.textColor = .white
        }
        
        lblTitleDdescription.text = "Good friends, good books, and a sleepy conscience: this is the ideal life".localized
        imgTopBG.image = UIImage(named: "ic_home_header")
        
        if getLanguage() == arabic {
            lblTitleDdescription.textAlignment = .right
            imgTopBG.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        self.registerXib()
    }
    
    func registerXib() {
        
        self.tblHomeData.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
        self.tblHomeHeightConstraint.constant = 700
    }
    
}
