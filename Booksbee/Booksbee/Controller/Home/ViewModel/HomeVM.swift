//
//  HomeVM.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class HomeVM {
    
    var theController = HomeVC()
    var dictHomeData : HomeModel? = nil
    
    init(theDelegate: HomeVC) {
        self.theController = theDelegate
    }
    
    func getHomeDataApi(completionHandlor:@escaping(JSON)->Void) {
        
        let url = homepageURL
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    self.dictHomeData = HomeModel(JSON: dict.dictionaryObject!)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
