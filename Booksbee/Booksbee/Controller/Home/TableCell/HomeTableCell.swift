//
//  HomeTableCell.swift
//  Booksbee
//
//  Created by vishal on 08/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class HomeTableCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionHome: UICollectionView!
    @IBOutlet weak var btnViewAll: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 24, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        
        [btnViewAll].forEach { (btn) in
            btn?.setTitle("View All".localized, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 20, fontname: .medium)
            btn?.setTitleColor(.appTitleTextColor, for: .normal)
        }
        
        registerXib()
    }
    
    func registerXib() {
        
        self.collectionHome.register(UINib(nibName: "HomeBookDetailsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeBookDetailsCollectionCell")
        self.collectionHome.register(UINib(nibName: "HomeOfferCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeOfferCollectionCell")
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
