//
//  OrganizationVM.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class OrganizationVM {
    
    //MARK: Variables
    var theController = OrganizationVC()
    var dictOrganizationData = OrganizationModel(JSON: JSON().dictionaryValue)
    var activityIndicator = UIRefreshControl()
    var nxtPage = 1
    
    
    
    init(theController: OrganizationVC) {
        self.theController = theController
    }
    
    
    func getOrganizationListApi(page: Int, completionHandlor:@escaping(JSON)->Void) {
        
        var url = ""
        if page == 0  {
            url = organizationListURL
            showIndicator()
        }
        else {
            url = organizationListURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            self.activityIndicator.endRefreshing()
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    if page == 0 {
                        self.dictOrganizationData = nil
                        self.dictOrganizationData = OrganizationModel(JSON: dict.dictionaryObject!)
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = Bookdata(JSON: bookData.dictionaryObject!)
                            self.dictOrganizationData?.data?.bookdata.append(data!)
                        }
                    }
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
