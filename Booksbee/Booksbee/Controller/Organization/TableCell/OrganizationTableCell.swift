//
//  OrganizationTableCell.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SDWebImage

class OrganizationTableCell: UITableViewCell {

    //MARK:outlets
    @IBOutlet weak var imgBookCover: UIImageView!
    @IBOutlet weak var lblBookTitle: UILabel!
    @IBOutlet weak var lblDesscription: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgBookCover.layer.cornerRadius = 10
        imgBookCover.layer.masksToBounds = true
        
        [lblBookTitle].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 19, fontname: .medium)
        }
        
        [lblDesscription, lblAddress, lblPhoneNumber].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
    }
    
    func setUpData(data: Bookdata) {
        lblBookTitle.text = data.orgName
        lblDesscription.text = data.description
        lblAddress.text = data.address
        lblPhoneNumber.text = data.contactNo
        imgBookCover.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgBookCover.sd_setImage(with: data.image.toURL(), completed: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
