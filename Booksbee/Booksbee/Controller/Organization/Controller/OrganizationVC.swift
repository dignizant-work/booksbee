//
//  OrganizationVC.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class OrganizationVC: UIViewController {

    //MARK: Variables
    lazy var mainView: OrganizationView = {
        return self.view as! OrganizationView
    }()
    
    lazy var mainVM: OrganizationVM = {
        return OrganizationVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        setUpVC()
        
        self.mainVM.getOrganizationListApi(page: 0) { data in
            self.mainView.tblOrganization.reloadData()
        }
        self.setUpPulltorefresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
}

//MARK: Setup data
extension OrganizationVC {
    
    func setUpVC() {
        self.title = "Organization".localized
        setupTransparentNavigationBar()
    }
    
    func setUpPulltorefresh() {
        
        self.mainVM.activityIndicator = UIRefreshControl()
        self.mainVM.activityIndicator.tintColor = .appGoldenColor
        self.mainVM.activityIndicator.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblOrganization.addSubview(self.mainVM.activityIndicator)
    }
    
    @objc func pullToRefresh() {
        self.mainVM.nxtPage = 0
        self.mainVM.getOrganizationListApi(page: self.mainVM.nxtPage) { data in
            self.mainView.tblOrganization.reloadData()
        }
    }
    
}
