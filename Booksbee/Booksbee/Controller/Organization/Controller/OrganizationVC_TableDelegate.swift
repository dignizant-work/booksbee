//
//  OrganizationVC_TableDelegate.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension OrganizationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.mainVM.dictOrganizationData?.data?.bookdata.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrganizationTableCell", for: indexPath) as! OrganizationTableCell
        
        if let dict = self.mainVM.dictOrganizationData?.data?.bookdata[indexPath.row] {
            cell.setUpData(data: dict)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = homeStoryboard.instantiateViewController(withIdentifier: "OrganizationDetailsVC") as! OrganizationDetailsVC
        if let dict = self.mainVM.dictOrganizationData?.data?.bookdata[indexPath.row] {
            vc.mainVM.dictBookData = dict
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            let data = self.mainVM.dictOrganizationData?.data?.bookdata
            if tableView.visibleCells.contains(cell) {
                if (indexPath.row == (data?.count ?? 0)-1) && self.mainVM.dictOrganizationData?.data?.lastPage != self.mainVM.nxtPage {
                    self.mainVM.nxtPage += 1
                    self.showTableFooterIndicater(tableview: tableView)
                    self.mainVM.getOrganizationListApi(page: self.mainVM.nxtPage) { data in
                        tableView.tableFooterView?.isHidden = true
                        self.mainView.tblOrganization.reloadData()
                    }
                }
            }
        }
    }
    
}
