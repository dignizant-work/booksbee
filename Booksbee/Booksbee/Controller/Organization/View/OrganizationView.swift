//
//  OrganizationView.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class OrganizationView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var tblOrganization: UITableView!
    
    
    func setUpUI() {
        
        registerXib()
    }
    
    func registerXib() {
        self.tblOrganization.register(UINib(nibName: "OrganizationTableCell", bundle: nil), forCellReuseIdentifier: "OrganizationTableCell")
    }
    
}
