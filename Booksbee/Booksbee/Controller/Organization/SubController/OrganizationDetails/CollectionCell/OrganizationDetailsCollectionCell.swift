//
//  OrganizationDetailsCollectionCell.swift
//  Booksbee
//
//  Created by vishal on 11/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class OrganizationDetailsCollectionCell: UICollectionViewCell {

    //MARK:Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwBG: CustomView!
    @IBOutlet weak var btnSelected: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
    }

}
