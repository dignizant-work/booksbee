//
//  OrganizationDetailsVC_CollectionViewDelegate.swift
//  Booksbee
//
//  Created by vishal on 11/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

//MARK: CollectionView delegate methods
extension OrganizationDetailsVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mainVM.arrOrganization.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrganizationDetailsCollectionCell", for: indexPath) as! OrganizationDetailsCollectionCell
        
        let dict = self.mainVM.arrOrganization[indexPath.row]
        cell.lblTitle.text = dict["title"].stringValue
        
        if dict["isselected"].stringValue == "0" {
            cell.vwBG.backgroundColor = .white
            cell.lblTitle.textColor = .appDetailTextColor
        }
        else {
            cell.vwBG.backgroundColor = .appGoldenColor
            cell.lblTitle.textColor = .white
        }
        cell.btnSelected.tag = indexPath.row
        cell.btnSelected.addTarget(self, action: #selector(btnIsSelectedAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str: String = self.mainVM.arrOrganization[indexPath.row]["title"].stringValue
        let font: UIFont = themeFont(size: 14, fontname: .regular)
        let size: CGSize = str.size(withAttributes: [NSAttributedString.Key.font: font])

        return CGSize(width: size.width + 45, height: 50)
    }
    
    @objc func btnIsSelectedAction(_ sender: UIButton) {
        
        var dict = self.mainVM.arrOrganization[sender.tag]
        if dict["isselected"].stringValue == "1" {
            dict["isselected"].stringValue = "0"
        }
        else {
            dict["isselected"].stringValue = "1"
        }
        self.mainVM.arrOrganization[sender.tag] = dict
        self.mainView.collectionSelectBook.reloadData()
    }
    
}
