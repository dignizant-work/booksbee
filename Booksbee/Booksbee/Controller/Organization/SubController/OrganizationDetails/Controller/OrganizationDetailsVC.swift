//
//  OrganizationDetailsVC.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class OrganizationDetailsVC: UIViewController {

    //MARK: Variables
    lazy var mainView: OrganizationDetailsView = {
        return self.view as! OrganizationDetailsView
    }()
    
    lazy var mainVM: OrganizationDetailsVM = {
        return OrganizationDetailsVM(theController: self)
    }()
    
    
    //MARK:- Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.mainView.setUpUI()
        self.setUpDropDown()
        self.mainView.setUpData(data: self.mainVM.dictBookData!)
        setupNavigationbarBackButton(titleText: "Organization Details".localized)
        
        if !isGuest {
            self.mainVM.getMyBookListApi { }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let height = getCollectionViewContaintHeight(collectionView: self.mainView.collectionSelectBook)
        
        if height > 300 {
            self.mainView.heightCollectionSelectBook.constant = 300
        }
        else {
            self.mainView.heightCollectionSelectBook.constant = height
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        
        configureDropdown(dropdown: self.mainVM.selectBookDD, sender: self.mainView.txtBookDropDown, isWidth: true)
    }
    
}

//MARK: SetUp
extension OrganizationDetailsVC {
    
    func setUpDropDown() {
        
        self.mainVM.selectBookDD.selectionAction = { [weak self] (index, item) in
            self?.mainView.txtBookDropDown.text = item
            
            let book_id = self?.mainVM.innerDataArray[index]["book_id"].stringValue
            self?.mainVM.donetaBookID = book_id ?? ""
        }
    }
    
}


//MARK: Button action
extension OrganizationDetailsVC {
    
    @IBAction func btnSeeOnMapAction(_ sender: Any) {
        
        if self.mainVM.donetaBookID == "" {
            makeToast(strMessage: "Please select book for Donate".localized)
        }
        else {
            self.mainVM.donateBookApi {
                self.backButtonTapped()
            }
        }
    }
}
