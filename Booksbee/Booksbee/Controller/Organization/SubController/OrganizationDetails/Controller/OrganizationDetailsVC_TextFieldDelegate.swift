//
//  OrganizationDetailsVC_TextFieldDelegate.swift
//  Booksbee
//
//  Created by vishal on 11/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension OrganizationDetailsVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == mainView.txtBookDropDown{
            self.view.endEditing(true)
            self.mainVM.selectBookDD.show()
            return false
        }
        return true
    }
    
}
