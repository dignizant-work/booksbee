//
//  OrganizationDetailsView.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import KTCenterFlowLayout
import SDWebImage

class OrganizationDetailsView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var imgBookCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblDescriptionValue: UILabel!
    @IBOutlet weak var txtBookDropDown: CustomTextField!
    @IBOutlet weak var lblSelectBook: UILabel!
    @IBOutlet weak var btnSeeOnMap: CustomButton!
    @IBOutlet weak var collectionSelectBook: UICollectionView!
    @IBOutlet weak var heightCollectionSelectBook: NSLayoutConstraint!
    @IBOutlet weak var vwTxtBook: CustomView!
    
    //MARK: SetUp
    func setUpUI() {
        
        [lblTitle,lblDescriptionTitle].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 20, fontname: .medium)
        }
        
        [lblAddress,lblPhoneNumber,lblDescriptionValue].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [lblSelectBook].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
        
        [txtBookDropDown].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = .appTitleTextColor
            txt?.placeholderColor(.appDetailTextColor)
            txt?.text = ""
            txt?.placeholder = "Donate book for organization".localized
            if getLanguage() == arabic {
                txt?.textAlignment = .right
            }
        }
        
        [btnSeeOnMap].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .medium)
            btn?.setTitleColor(.white, for: .normal)
            btn?.setTitle("Donate Book".localized, for: .normal)
        }
        
        lblDescriptionTitle.text = "".localized
        
        registerXib()
        
        let layout = KTCenterFlowLayout()
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5

        self.collectionSelectBook.collectionViewLayout = layout
//        UICollectionViewController(collectionViewLayout: layout)
        
    }
    
    
    func registerXib() {
        self.collectionSelectBook.register(UINib(nibName: "OrganizationDetailsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "OrganizationDetailsCollectionCell")
    }
    
    func setUpData(data: Bookdata) {
        
        lblSelectBook.isHidden = true
        
        self.imgBookCover.layer.cornerRadius = 10
        self.imgBookCover.clipsToBounds = true
        self.imgBookCover.contentMode = .scaleToFill
        self.imgBookCover.sd_setImage(with: data.image.toURL(), completed: nil)
        
        self.lblTitle.text = data.orgName
        
        self.lblAddress.text = data.address
        self.lblPhoneNumber.text = data.contactNo
        
        self.lblDescriptionValue.text = data.description
        
        if isGuest {
            self.vwTxtBook.isHidden = true
            self.btnSeeOnMap.isHidden = true
        }
    }
    
}
