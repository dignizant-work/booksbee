//
//  OrganizationDetailsVM.swift
//  Booksbee
//
//  Created by vishal on 10/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import DropDown

class OrganizationDetailsVM {
    
    //MARK: Variables
    var theController = OrganizationDetailsVC()
    var arrOrganization: [JSON] = []
    var selectBookDD = DropDown()
    var dictBookData: Bookdata? = nil
    var innerDataArray: [JSON] = []
    var dictCategoryData = JSON()
    var donetaBookID = ""
    
    
    init(theController: OrganizationDetailsVC) {
        self.theController = theController
    }
    
    
    func getMyBookListApi(handlor:@escaping()->Void) {
        
        let  url = myDonateBookURL
        WebServices().MakePostAPI(name: url, params: [String : Any]()) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                self.innerDataArray = dict["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    self.selectBookDD.dataSource =  self.innerDataArray.map({ (json) -> String in
                        return json["book_name"].stringValue
                    })
                    handlor()
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func donateBookApi(handlor:@escaping()->Void) {
        
        let param = ["book_id":Int(self.donetaBookID)!,
                     "org_id":dictBookData?.id ?? 0] as [String:Any]
        print("PARAM:- \(param)")
        let  url = donateBookURL
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
//                print("Dict:- \(dict)")
                self.innerDataArray = dict["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    handlor()
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
}
