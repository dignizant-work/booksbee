//
//  SearchVC.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {

    //MARK: Variables
    lazy var mainView: SearchView = {
        return self.view as! SearchView
    }()
    
    lazy var mainVM: SearchVM = {
        return SearchVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUI()
        self.mainView.txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        setUpPullToRefresh()
        self.mainView.txtSearch.delegate = self
        
        if let arr = Defaults.value(forKey: "isSearchString") as? [String] {
            
            self.mainVM.arrSearch = arr.reversed()
            self.mainVM.arrBooksCategory = arr.reversed()
            
            self.mainView.tblSearch.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

}



//MARK: SetUp
extension SearchVC {
    
    //Text search value
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField.text?.count ?? 0 >= 2 {
            self.mainVM.arrBooksCategory = []
            let data = self.mainVM.arrSearch.filter { (str) in
                let title = str.lowercased().range(of: textField.text?.lowercased() ?? "")
                return (title != nil)
            }
            self.mainVM.arrBooksCategory = data
            
        }
        else {
            self.mainVM.arrBooksCategory = self.mainVM.arrSearch
        }
        self.mainView.tblSearch.reloadData()
    }
    
    func setUpPullToRefresh() {
        self.mainVM.refreshControl.tintColor = .appTitleTextColor
        self.mainVM.refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        self.mainView.tblSearch.addSubview(self.mainVM.refreshControl)
    }
    
    @objc func refreshData(_ sender: UIRefreshControl) {
        
        self.mainVM.arrBooksCategory = []
        self.mainVM.arrBooksCategory = self.mainVM.arrSearch
        self.mainView.tblSearch.reloadData()
        sender.endRefreshing()
    }
}


//MARK: Button Action
extension SearchVC {
    
    @IBAction func btnFilterAction(_ sender: Any) {
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        
        vc.modalPresentationStyle = .overCurrentContext
        
        vc.mainVM.handlorFiltorClose = {
            
        }
        
        vc.mainVM.handlorFiltorApply = {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
            vc.mainVM.isControllerChecked = .filter
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        vc.modalTransitionStyle = .coverVertical
        appdelgate.tabBarVC.present(vc, animated: false, completion: nil)
    }
}


//MARK: Textfield Delegate
extension SearchVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        if textField.text?.toTrim().count ?? 0 <= 2 {
            return true
        }
        
        var arr: [String] = []
        if let arrDefault = Defaults.value(forKey: "isSearchString") as? [String] {
            
            arr = arrDefault
            let txt = textField.text?.toTrim()
            if txt?.count ?? 0 > 2 {
                if !arr.contains(textField.text?.capitalized ?? "") {
                    arr.append(txt ?? "")
                }
            }
            
            if arr.count > 9 {
                arr.removeFirst()
            }
        }
        else {
            let txt = textField.text?.toTrim()
            if txt?.count ?? 0 > 2 {
                arr.append(txt ?? "")
            }
        }
        Defaults.setValue(arr, forKey: "isSearchString")
        
        if let arr = Defaults.value(forKey: "isSearchString") as? [String] {
            self.mainVM.arrSearch = arr.reversed()
            self.mainVM.arrBooksCategory = arr.reversed()
            self.mainView.tblSearch.reloadData()
        }
        
        textField.resignFirstResponder()
        
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
        vc.mainVM.isControllerChecked = .search
        vc.mainVM.txtSearch = textField.text ?? ""
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: false)
        
        return true
    }
    
}
