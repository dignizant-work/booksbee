//
//  SearchVC_TableDelegate.swift
//  Booksbee
//
//  Created by vishal on 14/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit


extension SearchVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return self.mainVM.arrBooksCategory.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableCell", for: indexPath) as! SearchTableCell
        
        cell.lblTitle.text = self.mainVM.arrBooksCategory[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
        vc.mainVM.isControllerChecked = .search
        vc.mainVM.txtSearch = self.mainVM.arrBooksCategory[indexPath.row]
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
}
