//
//  SearchVM.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class SearchVM {
    
    //MARK: Variables
    var theController = SearchVC()
    var arrSearch: [String] = []
    var arrBooksCategory: [String] = []
    var refreshControl = UIRefreshControl()
    
    init(theController: SearchVC) {
        self.theController = theController
    }
    
}
