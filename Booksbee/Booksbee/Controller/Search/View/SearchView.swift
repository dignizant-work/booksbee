//
//  SearchView.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class SearchView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tblSearch: UITableView!
    
    
    //MARK: SetUpUI
    func setUpUI() {
        
        [txtSearch].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.textColor = .appTitleTextColor
            txt?.placeholderColor(.appDetailTextColor)
            textFieldClearButtonColorSet(txt!, .appTitleTextColor)
            txt?.placeholder = "Search here".localized
        }
        
        [btnFilter].forEach { (btn) in
            btn?.setTitleColor(.appGoldenColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .medium)
            btn?.setTitle("Filter".localized, for: .normal)
        }
        
        if getLanguage() == arabic {
            self.txtSearch.textAlignment = .right
        }
        
        registerXib()
    }
    
    func registerXib() {
        self.tblSearch.register(UINib(nibName: "SearchTableCell", bundle: nil), forCellReuseIdentifier: "SearchTableCell")
    }
    
    
}
