//
//  FilterVM.swift
//  Booksbee
//
//  Created by vishal on 12/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import DropDown

class FilterVM {
    
    //MARK:Variables
    var theController = FilterVC()
    var handlorFiltorClose:()->Void = {}
    var categoryDeropDown = DropDown()
    var innerDataArray: [JSON] = []
    var dictCategoryData = JSON()
    
    var selectRangePrize = ""
    var handlorFiltorApply:()->Void = {}
    
    init(theController: FilterVC) {
        self.theController = theController
        filterDict = self.setUpBookCategoryFilterData()
    }
    
    func setUpBookCategoryFilterData() -> JSON {
        
        var dict = JSON()
        if filterDict["isFilterApply"].stringValue == "0" || filterDict.isEmpty {
            
            filterDict["isFilterApply"].stringValue = "0" // 1 means filter apply when Reset filter then isFilterApply = 0 set
            filterDict["isAtoZSelected"].stringValue = "1"
            filterDict["isZtoASelected"].stringValue = "0"
            filterDict["isSelectCategoryID"].stringValue = ""
            filterDict["isSelectCategoryValue"].stringValue = ""
            filterDict["selectPrize"].stringValue = ""
            filterDict["isBuySelected"].stringValue = "0"
            filterDict["isBidListingSeleted"].stringValue = "0"
            
            dict = filterDict
            return dict
        }
        
        return filterDict
    }
    
    
    func getCategoryListApi(handlor:@escaping()->Void) {
        
        let  url = categoryListURL
//        showIndicator()
        WebServices().MakeGetAPI(name: url, params: [:]) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                self.innerDataArray = dict["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    self.dictCategoryData = dict
                    self.categoryDeropDown.dataSource =  self.innerDataArray.map({ (json) -> String in
                        return json["category"].stringValue
                    })
                    handlor()
//                    print("Data Source:\(self.categoryDeropDown.dataSource)")
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
        
    }
    
}
