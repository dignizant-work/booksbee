//
//  FilterView.swift
//  Booksbee
//
//  Created by vishal on 12/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

class FilterView: UIView {
    
    //MARK:- Outlets
    @IBOutlet weak var vwBG: UIView!
    
    @IBOutlet weak var btnRese: UIButton!
    @IBOutlet weak var lblFilter: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var btnAtoZ: UIButton!
    @IBOutlet weak var btnZtoA: UIButton!
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var txtSelectCategory: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblPriceRange: UILabel!
    @IBOutlet weak var lblMinRange: UILabel!
    @IBOutlet weak var lblMaxRange: UILabel!
    @IBOutlet weak var selectRangeSider: RangeSeekSlider!
    
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var btnBidListing: UIButton!
    
    @IBOutlet weak var btnApplyFilter: CustomButton!
    
    
    //MARK: SetUp
    func setUpUI() {
        
        vwBG.clipsToBounds = true
        vwBG.layer.cornerRadius = 25
        vwBG.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner respectively

        
        [btnRese].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .medium)
            btn?.setTitleColor(.appDetailTextColor, for: .normal)
        }
        
        [btnAtoZ, btnZtoA, btnBuy, btnBidListing].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 20, fontname: .bold)
            btn?.setTitleColor(.appTitleTextColor, for: .normal)
        }
        
        [lblFilter,lblCategory,lblPriceRange].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        
        [btnRese].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .medium)
            btn?.setTitleColor(.appDetailTextColor, for: .normal)
        }
        
        [lblFilter].forEach { (lbl) in
            lbl?.font = themeFont(size: 20, fontname: .bold)
            lbl?.textColor = .appTitleTextColor
        }
        
        [lblCategory, lblPriceRange].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .bold)
            lbl?.textColor = .appTitleTextColor
        }
        
        [lblMinRange,lblMaxRange].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .bold)
            lbl?.textColor = .black
        }
        
        [txtSelectCategory].forEach { (txt) in
            txt?.placeholder = "Select here"
            txt?.title = "Select here"
            txt?.titleLabel.textColor = .appTitleTextColor
            txt?.titleLabel.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = .appTitleTextColor
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.placeholderFont = themeFont(size: 16, fontname: .medium)
            
            if getLanguage() == arabic {
                txt?.isLTRLanguage = false
            }

        }
        
        btnAtoZ.isSelected = true
        btnBuy.isSelected = true
        
        lblFilter.text = "Filter".localized
        btnRese.setTitle("Reset".localized, for: .normal)
        btnAtoZ.setTitle("A to Z".localized, for: .normal)
        btnZtoA.setTitle("Z to A".localized, for: .normal)
        lblCategory.text = "Category".localized
        lblPriceRange.text = "Prize Range".localized
        btnBuy.setTitle("Buy".localized, for: .normal)
        btnBidListing.setTitle("Bid Listings".localized.localized, for: .normal)
        btnApplyFilter.setTitle("Apply Filter".localized.localized.localized, for: .normal)
        
//        lblMinRange.text = "$ 50"
//        lblMaxRange.text = "$ 1500"
        
        
    }
    
    func setUpFilterData() {
        
        self.btnAtoZ.isSelected = false
        if filterDict["isAtoZSelected"].stringValue == "1" {
            self.btnAtoZ.isSelected = true
        }
        
        self.btnZtoA.isSelected = false
        if filterDict["isZtoASelected"].stringValue == "1" {
            self.btnZtoA.isSelected = true
        }
        
        self.btnBuy.isSelected = false
        if filterDict["isBuySelected"].stringValue == "1" {
            self.btnBuy.isSelected = true
        }
//        isSelectCategoryValue
        
        if filterDict["selectPrize"].stringValue != "" {
            self.selectRangeSider.selectedMaxValue = CGFloat(filterDict["selectPrize"].floatValue)
            self.selectRangeSider.maxLabel.string = "\(filterDict["selectPrize"].intValue) KD"
            print("Value:- \(filterDict["isSelectCategoryValue"].stringValue)")
        }
        
        self.txtSelectCategory.text = filterDict["isSelectCategoryValue"].stringValue
        
        self.btnBidListing.isSelected = false
        if filterDict["isBidListingSeleted"].stringValue == "1" {
            self.btnBidListing.isSelected = true
        }
    }
    
    func setUpSideRanger(minPrize: String,maxPrize:String) {
        
//        selectRangeSider.selectedMaxValue = 1
        selectRangeSider.positionMaxLabelAtBottom = false
        selectRangeSider.maxLabelColor = UIColor.appGoldenColor
        selectRangeSider.maxLabel.font = themeFont(size: 12, fontname: .medium)
        if #available(iOS 13.0, *) {
            selectRangeSider.minLabel.accessibilityRespondsToUserInteraction = false
        }
        selectRangeSider.maxLabel.string = "\(minPrize) KD"
        selectRangeSider.maxValue = CGFloat(Int(maxPrize) ?? 0)
//        selectRangeSider.selectedMaxValue = CGFloat(Int(maxPrize) ?? 0)
        selectRangeSider.numberFormatter.positivePrefix = "KD"
        selectRangeSider.leftHandle.isHidden = true
        
        if getLanguage() == arabic {
            
            [selectRangeSider].forEach { (slider) in
                
                slider?.transform = CGAffineTransform(scaleX: -1, y: 1)
                slider?.minLabel.transform = CATransform3DMakeRotation(.pi, 0, 1, 0)
                slider?.maxLabel.transform = CATransform3DMakeRotation(.pi, 0, 1, 0)
            }
        }

    }
    
}
