//
//  FilterVC.swift
//  Booksbee
//
//  Created by vishal on 12/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SwiftyJSON

var filterDict = JSON()
class FilterVC: UIViewController {

    //MARK: Variables
    lazy var mainView: FilterView = {
        return self.view as! FilterView
    }()
    
    lazy var mainVM: FilterVM = {
        return FilterVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        setUpData()
        self.mainVM.getCategoryListApi {
            
            self.mainView.setUpSideRanger(minPrize: self.mainVM.dictCategoryData["min_price"].stringValue, maxPrize: self.mainVM.dictCategoryData["max_price"].stringValue)
            self.mainView.lblMinRange.text = "\(self.mainVM.dictCategoryData["min_price"].stringValue) KD"
            self.mainView.lblMaxRange.text = "\(self.mainVM.dictCategoryData["max_price"].stringValue) KD"
            
            self.mainView.setUpFilterData()
            if filterDict["isFilterApply"].stringValue == "0" {
                filterDict["isSelectCategoryID"].stringValue = self.mainVM.innerDataArray.first?["id"].stringValue ?? ""
                filterDict["isSelectCategoryValue"].stringValue = self.mainVM.innerDataArray.first?["category"].stringValue ?? ""
                self.mainView.txtSelectCategory.text = filterDict["isSelectCategoryValue"].stringValue
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        configureDropdown(dropdown: self.mainVM.categoryDeropDown, sender: self.mainView.txtSelectCategory)
        self.mainVM.categoryDeropDown.selectionAction = { [weak self] (index, item) in
            self?.mainView.txtSelectCategory.text = item
        }
    }
    
}

//MARK: Setup
extension FilterVC {
    
    func setUpData() {
        self.mainView.selectRangeSider.delegate = self
    }
    
}

//MARK: Button Action
extension FilterVC {
    
    @IBAction func btnCloseAction(_ sender: Any) {
        self.mainVM.handlorFiltorClose()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnresetAction(_ sender: Any) {
        
        filterDict = JSON()
        filterDict = self.mainVM.setUpBookCategoryFilterData()
        self.mainView.setUpFilterData()
        filterDict["isSelectCategoryID"].stringValue = self.mainVM.innerDataArray.first?["id"].stringValue ?? ""
        filterDict["isSelectCategoryValue"].stringValue = self.mainVM.innerDataArray.first?["category"].stringValue ?? ""
        filterDict["selectPrize"].stringValue = ""
        self.mainView.selectRangeSider.maxLabel.string = "1 KD"
        self.mainView.selectRangeSider.selectedMaxValue = 1.0
        self.mainView.txtSelectCategory.text = filterDict["isSelectCategoryValue"].stringValue
        
    }
    
    @IBAction func btnAtoZAction(_ sender: Any) {
        
        if filterDict["isAtoZSelected"].stringValue == "0" {
            filterDict["isZtoASelected"].stringValue = "0"
            filterDict["isAtoZSelected"].stringValue = "1"
            self.mainView.btnAtoZ.isSelected = true
            self.mainView.btnZtoA.isSelected = false
        }
    }
    
    @IBAction func btnZtoAAction(_ sender: Any) {
        
        if filterDict["isZtoASelected"].stringValue == "0" {
            filterDict["isZtoASelected"].stringValue = "1"
            filterDict["isAtoZSelected"].stringValue = "0"
            self.mainView.btnAtoZ.isSelected = false
            self.mainView.btnZtoA.isSelected = true
        }
    }
    
    @IBAction func btnBuyAction(_ sender: Any) {
        if filterDict["isBuySelected"].stringValue == "0" {
            filterDict["isBuySelected"].stringValue = "1"
            filterDict["isBidListingSeleted"].stringValue = "0"
            self.mainView.btnBuy.isSelected = true
            self.mainView.btnBidListing.isSelected = false
        }
    }
    
    @IBAction func btnBidListingAction(_ sender: Any) {
        if filterDict["isBidListingSeleted"].stringValue == "0" {
            filterDict["isBuySelected"].stringValue = "0"
            filterDict["isBidListingSeleted"].stringValue = "1"
            self.mainView.btnBuy.isSelected = false
            self.mainView.btnBidListing.isSelected = true
        }
    }
    
    @IBAction func btnApplyFilterAction(_ sender: Any) {

        let selectCategory = self.mainVM.innerDataArray.filter { data in
            let category = (data["category"].stringValue).lowercased() == (self.mainView.txtSelectCategory.text ?? "").lowercased() ? true : false
            
            return category
        }
        
        var categoryId = ""
        
        if selectCategory.count > 0 {
            categoryId = selectCategory.first?["id"].stringValue ?? ""
        }
        
        filterDict["isFilterApply"].stringValue = "1" // 1 means filter apply when Reset filter then isFilterApply = 0 set
        filterDict["isAtoZSelected"].stringValue = self.mainView.btnAtoZ.isSelected ? "1" : "0"
        filterDict["isZtoASelected"].stringValue = self.mainView.btnZtoA.isSelected ? "1" : "0"
        filterDict["isSelectCategoryID"].stringValue = categoryId
        filterDict["isSelectCategoryValue"].stringValue = self.mainView.txtSelectCategory.text ?? ""
        filterDict["selectPrize"].stringValue = self.mainVM.selectRangePrize
        filterDict["isBuySelected"].stringValue = self.mainView.btnBuy.isSelected ? "1" : "0"
        filterDict["isBidListingSeleted"].stringValue = self.mainView.btnBidListing.isSelected ? "1" : "0"
        
        print("data:- \(filterDict)")
        self.mainVM.handlorFiltorApply()
        self.dismiss(animated: false, completion: nil)
    }

}

//MARK: Slider setup
extension FilterVC: RangeSeekSliderDelegate {

    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === self.mainView.selectRangeSider {
            self.mainVM.selectRangePrize = "\(Int(maxValue))"
            self.mainView.selectRangeSider.maxLabel.string = "\(Int(maxValue)) KD"
//            print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        }
    }

    func didStartTouches(in slider: RangeSeekSlider) {
//        print("did start touches")
    }

    func didEndTouches(in slider: RangeSeekSlider) {
//        print("did end touches")
    }
}


//MARK: Rextfield delegate
extension FilterVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == mainView.txtSelectCategory{
            self.view.endEditing(true)
            self.mainVM.categoryDeropDown.show()
            return false
        }
        return true
    }
}
