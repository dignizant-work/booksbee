//
//  SearchResultVM.swift
//  Booksbee
//
//  Created by vishal on 28/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class SearchResultVM {
    
    //MARK: Variables
    var theController = SearchResultVC()
    var dictBookData = BookModel(JSON: JSON().dictionaryValue)
    var isControllerChecked = enumSearchListDataCheck.filter
    var txtSearch = ""
    var nxtPage = 0
    var userID = 0
    var userName = ""
    
    init(theController: SearchResultVC) {
        self.theController = theController
    }
    
}

//MARK: - API Calling
extension SearchResultVM {
    
    func getBookListApi(page: Int,isShowLoader:Bool = true ,completionHandlor:@escaping(JSON)->Void) {
        
        var firstFilter = ""
        if filterDict["isAtoZSelected"].stringValue == "1" {
            firstFilter = "1"
        }
        else if filterDict["isZtoASelected"].stringValue == "1" {
            firstFilter = "2"
        }
        
        var secondOptionFilter = ""
        if filterDict["isBuySelected"].stringValue == "1" {
            secondOptionFilter = "1"
        }
        else if filterDict["isBidListingSeleted"].stringValue == "1" {
            secondOptionFilter = "2"
        }
        
        var param = NSDictionary() as! [String : Any]
        
        if isControllerChecked == .search {
            param = ["category_id": "0",
                     "search_text":txtSearch] as [String : Any]
        }
        else if isControllerChecked == .filter {
            param = ["category_id": filterDict["isSelectCategoryID"].stringValue,
                     "first_option_filter":firstFilter,
                     "second_option_filter":filterDict["selectPrize"].stringValue == "" ? "" : "1",
                     "min_price":"",
                     "max_price":filterDict["selectPrize"].stringValue,
                     "search_text":"",
                     "third_option_filter":secondOptionFilter] as [String : Any]
        }
        else if isControllerChecked == .ourTopBooks {
            param = ["is_best": "1",
                     "category_id":"0"] as [String : Any]
        }
        
        print("param:\(param)")
        var url = ""
        if page == 1  {
            url = bookListURL
            if isShowLoader {
                showIndicator()
            }
        }
        else {
            url = bookListURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    //                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    if page == 1 {
                        self.dictBookData = nil
                        self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = Bookdata(JSON: bookData.dictionaryObject!)
                            self.dictBookData?.bookDataModel?.bookData.append(data!)
                        }
                    }
                }
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    
    func getViewAllBestSallersApi(page: Int,isShowLoader:Bool = true ,completionHandlor:@escaping(JSON)->Void) {
        
        var url = ""
        if page == 1  {
            url = viewAllBestsellersURL
            if isShowLoader {
                showIndicator()
            }
        }
        else {
            url = viewAllBestsellersURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    //                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    if page == 1 {
                        self.dictBookData = nil
                        self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = Bookdata(JSON: bookData.dictionaryObject!)
                            self.dictBookData?.bookDataModel?.bookData.append(data!)
                            print("")
                        }
                    }
                }
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func getBestSellersAllBookApi(page: Int,isShowLoader:Bool = true ,completionHandlor:@escaping(JSON)->Void) {
        
        var url = ""
        if page == 1  {
            url = bestSellerBooksURL
            if isShowLoader {
                showIndicator()
            }
        }
        else {
            url = bestSellerBooksURL+"?page=\(page)"
        }
        
        let param = ["user_id":self.userID] as [String:Any]
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    //                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    if page == 1 {
                        self.dictBookData = nil
                        self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = Bookdata(JSON: bookData.dictionaryObject!)
                            self.dictBookData?.bookDataModel?.bookData.append(data!)
                            print("")
                        }
                    }
                }
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
}
