//
//  SearchResultVC.swift
//  Booksbee
//
//  Created by vishal on 28/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class SearchResultVC: UIViewController {

    //MARK: Variables
    lazy var mainView: SearchResultView = {
        return self.view as! SearchResultView
    }()
    
    lazy var mainVM: SearchResultVM = {
        return SearchResultVM(theController: self)
    }()
    var isLoadMore = true
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        
        if self.mainVM.isControllerChecked == .viewAllBestSellers {
            self.mainVM.getViewAllBestSallersApi(page: 1, isShowLoader: true) { data in
                self.mainView.collectionSearchResultes.reloadData()
            }
            setupNavigationbarBackButton(titleText: "All Sellers".localized)
        }
        else if self.mainVM.isControllerChecked == .sellerAllBook {
            self.mainVM.getBestSellersAllBookApi(page: 1, isShowLoader: true) { data in
                self.mainView.collectionSearchResultes.reloadData()
            }
            setupNavigationbarBackButton(titleText: self.mainVM.userName + "'s All Books".localized)
        }
        else {
            self.mainVM.getBookListApi(page: 1, isShowLoader: true) { result in
                self.mainView.collectionSearchResultes.reloadData()
            }
            setupNavigationbarBackButton(titleText: "Book List".localized)
        }
        setupNavigationbarBottomLineHidden()
                
    }
    
}
