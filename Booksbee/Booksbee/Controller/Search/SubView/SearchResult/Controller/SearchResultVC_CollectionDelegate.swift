//
//  SearchResultVC_CollectionDelegate.swift
//  Booksbee
//
//  Created by vishal on 28/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

//MARK:Collection delegate/datasource
extension SearchResultVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.mainVM.dictBookData?.bookDataModel?.bookData.count ?? 0 == 0 || self.mainVM.dictBookData?.bookDataModel?.bookData == nil{
            collectionView.showCollectionEmptyEmptyMessage(self.mainVM.dictBookData?.message ?? "")
            return 0
        }
        
        collectionView.backgroundView = nil
        return self.mainVM.dictBookData?.bookDataModel?.bookData.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBookDetailsCollectionCell", for: indexPath) as! HomeBookDetailsCollectionCell
        
        if self.mainVM.isControllerChecked == .viewAllBestSellers {
            
            if let data = self.mainVM.dictBookData?.bookDataModel?.bookData[indexPath.row] {
                cell.lblBookPrize.isHidden = true
                cell.bookRate.isHidden = true
                cell.imgBooks.sd_setImage(with: data.profile.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, context: nil)
                cell.lblBookTitle.text = data.name
            }
        }
        else if self.mainVM.isControllerChecked == .sellerAllBook {
            if let data = self.mainVM.dictBookData?.bookDataModel?.bookData[indexPath.row] {
                cell.setUpData(bookData: data)
            }
        }
        else {
            if let data = self.mainVM.dictBookData?.bookDataModel?.bookData[indexPath.row] {
                cell.setUpData(bookData: data)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dict = self.mainVM.dictBookData?.bookDataModel?.bookData[indexPath.row]
        
        if self.mainVM.isControllerChecked == .viewAllBestSellers {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
            vc.mainVM.isControllerChecked = .sellerAllBook
            vc.mainVM.userID = dict?.userId ?? 0
            vc.mainVM.userName = dict?.name ?? ""
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if self.mainVM.isControllerChecked == .sellerAllBook {
            let vc = homeStoryboard.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
            vc.mainVM.bookId = dict?.id ?? 0
            vc.mainVM.strBookName = dict?.bookName ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let vc = homeStoryboard.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
            vc.mainVM.bookId = dict?.id ?? 0
            vc.mainVM.strBookName = dict?.bookName ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width/3
        
        return CGSize(width: width, height: collectionView.frame.size.height/3-15)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let lastSectionIndex = collectionView.numberOfSections - 1
        let lastRowIndex = collectionView.numberOfItems(inSection: lastSectionIndex) - 1
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1) {
            if collectionView.visibleCells.contains(cell){
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && self.mainVM.dictBookData?.bookDataModel?.lastPage != self.mainVM.nxtPage {
                    
                    self.mainVM.nxtPage += 1
//                    self.showTableFooterIndicater(tableview: collectionView)
                    if self.mainVM.isControllerChecked == .viewAllBestSellers {
                        self.mainVM.getViewAllBestSallersApi(page: self.mainVM.nxtPage, isShowLoader: false) { data in
                            collectionView.reloadData()
                        }
                    }
                    else if self.mainVM.isControllerChecked == .sellerAllBook {
                        self.mainVM.getBestSellersAllBookApi(page: self.mainVM.nxtPage, isShowLoader: false) { data in
                            collectionView.reloadData()
                        }
                    }
                    else {
                        self.mainVM.getBookListApi(page: self.mainVM.nxtPage, isShowLoader: false) { data in
                            collectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
}
