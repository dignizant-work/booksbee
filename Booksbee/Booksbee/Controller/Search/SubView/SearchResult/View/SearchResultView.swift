//
//  SearchResultView.swift
//  Booksbee
//
//  Created by vishal on 28/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class SearchResultView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var collectionSearchResultes: UICollectionView!
    
    
    //MARK: SetUpUI
    func setUpUI() {
        
        registerXib()
    }
    
    //MARK: Register Xib
    func registerXib() {
        
        self.collectionSearchResultes.register(UINib(nibName: "HomeBookDetailsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeBookDetailsCollectionCell")
    }
    
}
