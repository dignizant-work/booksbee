//
//  SearchTableCell.swift
//  Booksbee
//
//  Created by vishal on 14/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class SearchTableCell: UITableViewCell {

    //MARK:Outlets
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = .appTitleTextColor
        }
        
        [imgBG].forEach { (img) in
//            img?.layer.cornerRadius = img?.frame.size.height ?? 0/2
//            img?.clipsToBounds = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
