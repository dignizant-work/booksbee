//
//  SelectLanguageView.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class LoginSignupSelectionView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    
    func setUpUI() {
        
        backgroundColor = .appBackgroundColor
        
        btnLogin.titleLabel?.font = themeFont(size: 16, fontname: .bold)
        btnLogin.setTitleColor(.white, for: .normal)
        
        
        btnSignup.titleLabel?.font = themeFont(size: 16, fontname: .bold)
        btnSignup.setTitleColor(.appTitleTextColor, for: .normal)

        
        btnLogin.setTitle("Login".localized.capitalized, for: .normal)
        btnSignup.setTitle("Sign Up".localized.capitalized, for: .normal)
    }
    
}
