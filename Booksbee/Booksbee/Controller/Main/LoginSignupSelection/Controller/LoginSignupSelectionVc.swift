//
//  SelectLanguageVC.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class LoginSignupSelectionVc: UIViewController {

    //MARK:- Variables
    lazy var mainView: LoginSignupSelectionView = {
        return self.view as! LoginSignupSelectionView
    }()
    
    lazy var mainVM: LoginSignupSelectionVM = {
        return LoginSignupSelectionVM()
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarTitle(titleText: "")
        setupNavigationbarBottomLineHidden()
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    @IBAction func btnSignupAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
}
