//
//  SelectLanguageVC.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class SelectLanguageVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: SelectLanguageView = {
        return self.view as! SelectLanguageView
    }()
    
    lazy var mainVM: SelectLanguageVM = {
        return SelectLanguageVM()
    }()
    
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isVisitTour()
        self.mainView.setUpUI()
    }
    
    func isVisitTour(){
        
        if let _ = Defaults.value(forKey: "isTourVisited") as? Bool{
         
            if getUserData()?.data == nil{
                //loginvc
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as!LoginVC
                self.navigationController?.pushViewController(vc, animated: false)
                
            }else{
                //home screen
                setUpTabbarRoot()
            }
        }
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarTitle(titleText: "Select App Language".localized)
        setupNavigationbarBottomLineHidden()
    }
    
    @IBAction func btnEnglishAction(_ sender: Any) {
        
        setLanguage(lng: english)
        navigationController?.view.semanticContentAttribute = .forceLeftToRight
        navigationController?.navigationBar.semanticContentAttribute =  .forceLeftToRight
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "TourVC") as! TourVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnArabicAction(_ sender: Any) {
        
        setLanguage(lng: arabic)
        navigationController?.view.semanticContentAttribute = .forceRightToLeft
        navigationController?.navigationBar.semanticContentAttribute =  .forceRightToLeft
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "TourVC") as! TourVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}
