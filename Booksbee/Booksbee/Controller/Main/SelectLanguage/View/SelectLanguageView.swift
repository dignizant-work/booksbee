//
//  SelectLanguageView.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class SelectLanguageView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnArabic: UIButton!
    
    
    func setUpUI() {
        
        backgroundColor = .appBackgroundColor
        
        [btnArabic, btnEnglish].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .bold)
            btn?.setTitleColor(.appTitleTextColor, for: .normal)
        }
        
    }
    
}
