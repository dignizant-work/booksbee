//
//  CountryCodeVC.swift
//  RantASki
//
//  Created by Khushbu on 13/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol CountryCodeDelegate : class {
    func CountryCodeDidFinish(data: JSON)
}

class CountryCodeVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: CountryCodeView = {
        return self.view as! CountryCodeView
    }()
    
    lazy var mainViewModel: CountryCodeViewModel = {
        return CountryCodeViewModel(theDelegate: self)
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.getCode(theDelegate: self)
    }

    @IBAction func bttnDismissAction(_ sender: Any) {
        self.dismissScreen()
    }
}
