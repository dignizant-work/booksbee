//
//  TourCollectionCell.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class TourCollectionCell: UICollectionViewCell {

    //MARK:- Outlets
    @IBOutlet weak var imgSlider: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()
    }
    
    func setUpUI() {
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 24, fontname: .bold)
        }
        
        [lblDescription].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 22, fontname: .regular)
        }
    }

}
