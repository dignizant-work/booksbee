//
//  TourVC.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class TourVC: UIViewController {

    //MARK: Variables
    lazy var mainView: TourView = {
        return self.view as! TourView
    }()
    
    lazy var mainVM: TourVM = {
        return TourVM()
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUp()
        self.mainView.setUpUI()
    }
    
    func setUp() {
        
        self.mainVM.setUpTourData()
        
        self.mainView.pageController.numberOfPages = self.mainVM.arrayTourData.count
        
        mainView.collectionTour.delegate = self
        mainView.collectionTour.dataSource = self
        mainView.collectionTour.reloadData()
        
        setupTransparentNavigationBar()
        
        let btnLeft = UIButton()
        btnLeft.setImage(UIImage(named: "ic_arrow_back")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnLeft.setImageTintColor(.black)
        btnLeft.addTarget(self, action: #selector(btnBackAction), for: .touchUpInside)
        
        let btnRight = UIButton()
        btnRight.setTitle("skip".localized, for: .normal)
        btnRight.setTitleColor(UIColor.appTitleTextColor, for: .normal)
        btnRight.addTarget(self, action: #selector(btnSkipAction), for: .touchUpInside)
        btnRight.titleLabel?.font = themeFont(size: 14, fontname: .bold)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnLeft)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnRight)
    }
    
}


//MARK:- Button Action
extension TourVC {
    
    @IBAction func btnNextAction(_ sender: Any) {
        
        if self.mainVM.selectedIndex == (self.mainVM.arrayTourData.count-1) {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as!LoginVC
            self.navigationController?.pushViewController(vc, animated: false)
            return
        }
        self.mainVM.selectedIndex += 1
        let indexPath = IndexPath(row: self.mainVM.selectedIndex, section: 0)
        self.mainView.collectionTour.scrollToItem(at: indexPath, at: .right, animated: false)
    }
    
    @objc func btnBackAction() {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func btnSkipAction() {
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as!LoginVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
