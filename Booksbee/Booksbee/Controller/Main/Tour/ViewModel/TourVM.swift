//
//  TourVM.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class TourVM {
    
    let theController = TourVC()
    var arrayTourData : [JSON] = []
    var selectedIndex = 0
    
    
    func setUpTourData() {
        
        var dict = JSON()
        dict["img"].stringValue = "ic_login_slider"
        dict["title"].stringValue = "Convenient Use"
        dict["description"].stringValue = "Everything for comfortable reading of your favprite books"
        self.arrayTourData.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_search_book"
        dict["title"].stringValue = "Search Books"
        dict["description"].stringValue = "Easy search by all categories and authors from all over the wrold"
        self.arrayTourData.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_buy_book"
        dict["title"].stringValue = "Bay books"
        dict["description"].stringValue = "Buy books in three formats ebook, autdibook paper book"
        self.arrayTourData.append(dict)
    }
    
}
