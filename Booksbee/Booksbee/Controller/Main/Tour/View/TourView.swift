//
//  TourView.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class TourView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var collectionTour: UICollectionView!
    @IBOutlet weak var btnNext: CustomButton!
    @IBOutlet weak var pageController: UIPageControl!
    
    
    func setUpUI() {
        
        isArabic()
        self.registerXIB()
    }
    
    func registerXIB() {
        
        collectionTour.register(UINib(nibName: "TourCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TourCollectionCell")
        collectionTour.reloadData()
    }
    
    func isArabic() {
//        if getLanguage() == arabic {
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//        }
    }
}
