//
//  OtpVC.swift
//  Booksbee
//
//  Created by vishal on 25/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import FirebaseAuth

class OtpVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: OtpView = {
        return self.view as! OtpView
    }()
    
    lazy var mainVM: OtpVM = {
        return OtpVM()
    }()
    
    var enteredOTP = ""
    
    //MARK:- Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        self.setUp()
    }
    
    
}

//MARK:- SetUp
extension OtpVC {
    
    func setUp() {
        
        if self.mainVM.isCheckController == .email || self.mainVM.isCheckController == .editEmail {
            self.mainView.setUpEmailUI()
        }
        else {
            self.mainView.setUpNumberUI(enterNumber: self.mainVM.strPhoneNumber)
        }
        
        self.mainView.vwPin.didFinishCallback = { [weak self] pin in
            print("The pin entered is \(pin)")
            self?.enteredOTP = pin
        }

    }
    
}


//MARK:- Button Action
extension OtpVC {
    
    @IBAction func btnCloseAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }

    @IBAction func btnResentOTPTapped(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.dismiss(animated: false, completion: nil)
        }
        self.resendOTP()
    }

    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if mainVM.isCheckController == .email {
            DispatchQueue.main.async {
                self.mainVM.handlorEmailVarify()
            }
        }
        else if mainVM.isCheckController == .number {
            
            if self.enteredOTP.count < 6{
                makeToast(strMessage: "Please enter 6 digits charachter.")
            }else{
                verifyOtp {
                    DispatchQueue.main.async {
                        self.mainVM.handlorNumberVarify()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
        else if mainVM.isCheckController == .editPhone {
            
            if self.enteredOTP.count < 6{
                makeToast(strMessage: "Please enter 6 digits charachter.")
            }else{
                verifyOtp {
                    DispatchQueue.main.async {
                        self.mainVM.handlorNumberVarify()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }

    }
    
    

}

//MARK: - OTP verification

extension OtpVC{
    
    func verifyOtp(reesponse: @escaping ()-> Void) {

        let verificationID = Defaults.value(forKey: "verificationId")
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID as! String,
            verificationCode: enteredOTP)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            if error == nil {
                if user != nil {
                    user?.user.getIDToken(completion: { (data, error) in
                        if error == nil {
                                                        
                            Defaults.removeObject(forKey: "verificationId")
                            Defaults.synchronize()
                            reesponse()

                        }
                    })
                                        
                    let firebaseAuth = Auth.auth()
                    do {
                        try firebaseAuth.signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                }
            }
            else {
                makeToast(strMessage: error?.localizedDescription ?? "")
                print("Error:-\(error?.localizedDescription as Any)")
            }
        }
        
//        Auth.auth().signIn(with: credential) { (user, error) in
//            if let error = error {
//                print(error.localizedDescription)
//                APPDEL.window?.makeToast("OTP entered is incorrect")
//                complition(false)
//                return
//            }
//            complition(true)
//        }
    }
    
    func resendOTP() {
        
        let phoneNumber = self.mainVM.strPhoneNumber
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            
            if error == nil {
                print("verificationId:-",verificationId!)
                guard let verify = verificationId else {
                    return
                }
                Defaults.set(verify, forKey: "verificationId")
                self.mainVM.handlorResendOTPVarify()
            }
            else {
                print("Error:-\(error?.localizedDescription ?? "")")
            }
        }
    }

}
