//
//  OtpView.swift
//  Booksbee
//
//  Created by vishal on 25/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SVPinView

class OtpView: UIView {
    
    //MARK:- Outlets
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var lblTopTitleVerify: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgOtp: UIImageView!
    @IBOutlet weak var lblVerification: UILabel!
    @IBOutlet weak var lblEnterOtpSense: UILabel!
    
    @IBOutlet weak var vwPin: SVPinView!
    
    @IBOutlet weak var lblDontReceveOtp: UILabel!
    @IBOutlet weak var btnResendOtp: UIButton!
    
    @IBOutlet weak var btnSubmit: CustomButton!
    
    //MARK:- SetUpUI
    func setUpUI() {
        
        vwBG.clipsToBounds = true
        vwBG.layer.cornerRadius = 30
        vwBG.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
     
        vwPin.style = .none
        vwPin.font = themeFont(size: 14, fontname: .medium)
        
        [lblTopTitleVerify, lblVerification].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 12, fontname: .medium)
        }
        
        [lblEnterOtpSense].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [lblDontReceveOtp].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [btnResendOtp].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .medium)
            btn?.setTitleColor(.appGoldenColor, for: .normal)
        }
        
        [btnSubmit].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .medium)
            btn?.setTitleColor(.white, for: .normal)
            btn?.backgroundColor = .appTitleTextColor
        }
    }
    
    func setUpEmailUI() {
        
        lblTopTitleVerify.text = "Email Verify OTP"
        lblEnterOtpSense.text = "Enter the OTP send to SRS@gmail.com"
        imgOtp.image = UIImage(named: "ic_email_otp")
    }
    
    func setUpNumberUI(enterNumber:String) {
        
        lblTopTitleVerify.text = "Phone Number Verify OTP"
        lblEnterOtpSense.text = "Enter the OTP send to \(enterNumber)"
        imgOtp.image = UIImage(named: "ic_phone_otp")
    }
    
}
