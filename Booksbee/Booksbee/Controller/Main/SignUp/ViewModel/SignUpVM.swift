//
//  SignUpVM.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class SignUpVM {
    
    var theController = SignUpVC()
    
    init(theController: SignUpVC) {
        self.theController = theController
    }

    var countryDialCode = ""
}


//MARK: - API calling

extension SignUpVM{
    
    
    func signUpAPI(param:[String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = register
        
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                
                let dict = JSON(data!)
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    savedUserData(user: dict)
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
            
        }
        
    }
    
}
