//
//  SignUpVC.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import FirebaseAuth

class SignUpVC: UIViewController {

    //MARK: Variables
    lazy var mainView: SignUpView = {
        return self.view as! SignUpView
    }()
    
    lazy var mainVM: SignUpVM = {
        return SignUpVM( theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    @IBAction func btnCountryCodeTapped(_ sender: UIButton) {
        
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overFullScreen
        obj.mainView.delegate = self
        self.present(obj, animated: false, completion: nil)

    }
}

//MARK: Setup
extension SignUpVC {
    
    func setUp() {
        
        self.mainView.setUpUI()
        
        let btnLeft = UIButton()
        btnLeft.setImage(UIImage(named: "ic_arrow_back")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnLeft.setImageTintColor(.black)
        btnLeft.addTarget(self, action: #selector(btnBackAction), for: .touchUpInside)
        
        let btnRight = UIButton()
        btnRight.setTitle("skip".localized, for: .normal)
        btnRight.titleLabel?.font = themeFont(size: 14, fontname: .bold)
        btnRight.setTitleColor(UIColor.appTitleTextColor, for: .normal)
        btnRight.addTarget(self, action: #selector(btnSkipAction), for: .touchUpInside)
        
        if isGuest{
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnLeft)
        }else{
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnRight)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnLeft)
        }
 
        setupTransparentNavigationBar()
        
//        self.mainView.txtEmail.addTarget(self, action: #selector(textfieldValueChanged(txtField:)), for: .editingDidEnd)
//        self.mainView.txtPhonenumber.addTarget(self, action: #selector(textfieldValueChanged(txtField:)), for: .editingDidEnd)
    }
}

//MARK:- Button Action
extension SignUpVC {
    
    @objc func btnSkipAction() {
        isGuest = true
        setUpTabbarRoot()
    }
    
    @objc func btnBackAction() {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnTopSignUpAction(_ sender: Any) {
        
    }
    
    @IBAction func btnTopLoginAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        
        if self.mainView.checkValidation() {
          
            let param = ["phone_no":self.mainView.txtPhonenumber.text ?? "",
                         "email":self.mainView.txtEmail.text ?? ""] as [String:Any]
            
            CommonServices().checkUniqueValueAPI(param: param) { data in
                
                self.setupMobileNumberOTP()
            }
        }
        
//
        
//        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
//        vc.modalPresentationStyle = .overCurrentContext
//        vc.modalTransitionStyle = .crossDissolve
//        vc.mainVM.isCheckController = .email
//
//        vc.mainVM.handlorEmailVarify = {
//            let vc = mainStoryboard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
//            vc.modalPresentationStyle = .overCurrentContext
//            vc.modalTransitionStyle = .crossDissolve
//            vc.mainVM.isCheckController = .number
//            vc.mainVM.handlorNumberVarify = {
//                setUpTabbarRoot()
//            }
//            self.present(vc, animated: true, completion: nil)
//        }
//
//        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btncheckMarkTermsandConditionAction(_ sender: Any) {
        
        if self.mainView.btncheckMarkTermsandCondition.isSelected {
            self.mainView.btncheckMarkTermsandCondition.isSelected = false
        }
        else {
            self.mainView.btncheckMarkTermsandCondition.isSelected = true
        }
        
    }
    
    @IBAction func btnAgreeTermsConditionAction(_ sender: Any) {
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
        vc.mainVM.isControllerCheck = .termsCondition
        self.navigationController?.pushViewController(vc, animated: false)
    }

}


//MARK: - SignUpAPI

extension SignUpVC{
    
    func signUpAPI(){
        
        let dict = ["full_name":self.mainView.txtFullName.text ?? "",
                    "email":self.mainView.txtEmail.text ?? "",
                    "username": self.mainView.txtUsername.text ?? "",
                    "mobile_no": self.mainView.txtPhonenumber.text ?? "",
                    "password" : self.mainView.txtPassword.text ?? "",
                    "country_code" : self.mainVM.countryDialCode,
                    "device_type" : deviceType,
//                    "device_id" : deviceId,
                    "push_token" : fcmToken
        ] as [String : Any]
        
        self.mainVM.signUpAPI(param: dict) { (data) in
            isGuest = false
            setUpTabbarRoot()
        }

    }

}


//MARK: - OTP
extension SignUpVC {
    
    func setupMobileNumberOTP() {
        
        let phoneNumber = "\(self.mainVM.countryDialCode)"+"\(self.mainView.txtPhonenumber.text!)"
        print("phoneNumber:-\(phoneNumber)")
        
        showIndicator()
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            
            hideIndicator()
            
            if error == nil {
                print("verificationId:-",verificationId!)
                guard let verify = verificationId else { return }
                Defaults.set(verify, forKey: "verificationId")
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
                vc.mainVM.strPhoneNumber = phoneNumber
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.mainVM.isCheckController = .number
                vc.mainVM.handlorNumberVarify = {
//                    setUpTabbarRoot()
                    self.signUpAPI()
                }
                vc.mainVM.handlorResendOTPVarify = {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
                    vc.mainVM.strPhoneNumber = phoneNumber
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    vc.mainVM.isCheckController = .number
                    vc.mainVM.handlorNumberVarify = {
                        self.signUpAPI()
                    }
                    self.present(vc, animated: true, completion: nil)
                }
                
                self.present(vc, animated: true, completion: nil)
                
            }
            else {
                makeToast(strMessage: error?.localizedDescription ?? "")
                print("Error:-\(error?.localizedDescription ?? "")")
            }
        }
    }
    
}


// MARK:- CountryCode Delegate
extension SignUpVC : CountryCodeDelegate {
    
    func CountryCodeDidFinish(data: JSON) {
        
        print("data:\(data)")
        self.mainVM.countryDialCode = data["dial_code"].stringValue
        self.mainView.lblCountryCode.text = data["dial_code"].stringValue
    }
}
