//
//  SignUpVC_TextFieldDelegate.swift
//  Booksbee
//
//  Created by vishal on 25/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension SignUpVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == self.mainView.txtFullName {
            self.mainView.txtUsername.becomeFirstResponder()
        }
        else if textField == self.mainView.txtUsername {
            self.mainView.txtEmail.becomeFirstResponder()
        }
        else if textField == self.mainView.txtEmail {
            self.mainView.txtPhonenumber.becomeFirstResponder()
        }
        else if textField == self.mainView.txtPassword {
            self.mainView.txtConfirmPassword.becomeFirstResponder()
        }
        else if textField == self.mainView.txtConfirmPassword {
            self.mainView.txtConfirmPassword.resignFirstResponder()
        }
        else {
            self.view.endEditing(true)
        }
        return true
    }
    
    @objc func textfieldValueChanged(txtField: UITextField) {
        
        if txtField == self.mainView.txtEmail {
            
            let param = ["phone_no":"",
                         "email":self.mainView.txtEmail.text ?? ""] as [String:Any]
            
            CommonServices().checkUniqueValueAPI(param: param) { data in
                
            }
        }
        else if txtField == self.mainView.txtPhonenumber {
            
            let param = ["phone_no":self.mainView.txtPhonenumber.text ?? "",
                         "email":""] as [String:Any]
            
            CommonServices().checkUniqueValueAPI(param: param) { data in
                
            }
        }
    }
}
