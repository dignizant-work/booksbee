//
//  SignUpView.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class SignUpView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var btnTopSignUp: UIButton!
    @IBOutlet weak var btnTopLogin: UIButton!
    
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhonenumber: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var lblCountryCode: UITextField!
    
    @IBOutlet weak var btncheckMarkTermsandCondition: UIButton!
    
    @IBOutlet weak var btnAgreeTermsCondition: UIButton!
    
    @IBOutlet weak var btnSignUp: CustomButton!
    
    
    func setUpUI() {
        
        [btnTopSignUp].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .bold)
            btn?.setTitleColor(.appGoldenColor, for: .normal)
            btn?.setTitle("Sign Up".localized, for: .normal)
        }
        
        [btnTopLogin].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .bold)
            btn?.setTitleColor(.appDetailTextColor, for: .normal)
            btn?.setTitle("Login".localized, for: .normal)
        }
        
        [txtFullName, txtUsername, txtEmail, txtPhonenumber, txtPassword, txtConfirmPassword].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = .appTitleTextColor
            txt?.placeholderColor(.appDetailTextColor)
        }
        
        txtFullName.placeholder = "Full Name".localized
        txtUsername.placeholder = "User Name".localized
        txtEmail.placeholder = "Email Address".localized
        txtPhonenumber.placeholder = "Phone Number".localized
        txtPassword.placeholder = "Password".localized
        txtConfirmPassword.placeholder = "Confirm Password".localized
        
        [btnAgreeTermsCondition].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .medium)
            btn?.setTitleColor(.appDetailTextColor, for: .normal)
            btn?.setTitle("Agree to Terms and Conditions".localized, for: .normal)
        }

        btncheckMarkTermsandCondition.isSelected = false
        
        self.isArabic()
    }
    
    func checkValidation() -> Bool {
        
        if self.txtFullName.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter full name".localized)
            return false
        }
        else if self.txtUsername.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter user name".localized)
            return false
        }
        else if self.txtEmail.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter email address".localized)
            return false
        }
        else if !isValidEmail(emailAddressString: txtEmail.text ?? "") {
            makeToast(strMessage: "Please enter valid email address".localized)
            return false
        }
        else if self.txtPhonenumber.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter phone number".localized)
            return false
        }
        else if !self.txtPhonenumber.text!.isNumeric {
            makeToast(strMessage: "Please enter valid phone number".localized)
            return false
        }
        else if txtPassword.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter password".localized)
            return false
        }
        else if txtPassword.text?.count ?? 0 < 6 {
            makeToast(strMessage: "Password must be atlist 6 digit long".localized)
            return false
        }
        else if !isValidPassword(passwordString: txtPassword.text ?? "") {
            makeToast(strMessage: "Password must contain mix of upper and lower case letters as well as digits and one special charecter".localized)
            return false
        }
        else if txtConfirmPassword.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter confirm password".localized)
            return false
        }
        else if txtConfirmPassword.text?.count ?? 0 < 6 {
            makeToast(strMessage: "Confirm password must be atlist 6 digit long".localized)
            return false
        }
        else if !isValidPassword(passwordString: txtConfirmPassword.text ?? "") {
            makeToast(strMessage: "Password must contain mix of upper and lower case letters as well as digits and one special charecter".localized)
            return false
        }
        else if txtPassword.text != txtConfirmPassword.text {
            makeToast(strMessage: "Password and confirm password are not same".localized)
            return false
        }
        else if !self.btncheckMarkTermsandCondition.isSelected {
            makeToast(strMessage: "Please allow Agree to Terms and Conditions".localized)
            return false
        }
        return true
    }
    
    func isArabic() {
        
        if getLanguage() == arabic {
            [txtFullName, txtUsername, txtEmail, txtPhonenumber, txtPassword, txtConfirmPassword].forEach { (txt) in
                txt?.textAlignment = .right
            }
        }
        
    }
    
}
