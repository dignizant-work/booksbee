//
//  LoginVC_TextFieldDelegate.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit


//MARK: Textfield delegate
extension LoginVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == self.mainView.txtEmail {
            self.mainView.txtPassword.becomeFirstResponder()
        }
        else if textField == self.mainView.txtPassword {
            self.mainView.txtPassword.resignFirstResponder()
        }
        else {
            self.view.endEditing(true)
        }
        return true
    }
    
}
