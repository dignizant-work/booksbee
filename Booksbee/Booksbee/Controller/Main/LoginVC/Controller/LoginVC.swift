//
//  LoginVC.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    //MARK:- Outlets
    
    lazy var mainView: LoginView = {
        return self.view as! LoginView
    }()
    
    lazy var mainVM: LoginVM = {
        return LoginVM(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        Defaults.setValue(true, forKey: "isTourVisited")
        Defaults.synchronize()
        
        self.mainView.setUpUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isGuest{
            setUpForBack()
        }else{
            self.setUpForSkip()
        }
        checkRemember()
    }
        
}

//MARK: Setup
extension LoginVC {
    
    func setUpForSkip() {
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: true)
        setupTransparentNavigationBar()
        
        let btnSkip = UIButton()
        btnSkip.setTitle("skip".localized, for: .normal)
        btnSkip.titleLabel?.font = themeFont(size: 14, fontname: .bold)
        btnSkip.setTitleColor(.appTitleTextColor, for: .normal)
        btnSkip.addTarget(self, action: #selector(btnSkipAction), for: .touchUpInside)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnSkip)
    }
    
    func setUpForBack() {
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: true)
        setupNavigationbarBackButton(titleText: "")
        setupTransparentNavigationBar()

//        let btnSkip = UIButton()
//        btnSkip.setTitle("skip".localized, for: .normal)
//        btnSkip.titleLabel?.font = themeFont(size: 14, fontname: .bold)
//        btnSkip.setTitleColor(.appTitleTextColor, for: .normal)
//        btnSkip.addTarget(self, action: #selector(btnSkipAction), for: .touchUpInside)
//
//        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnSkip)
    }

    
    func checkRemember(){
        
        if let isRemember = Defaults.value(forKey: "isRemember") as? Bool{
            
            if isRemember == true{
                
                self.mainView.btnRememberMe.isSelected = true
                self.mainView.txtEmail.text = Defaults.value(forKey: "rememberEmail") as? String
                self.mainView.txtPassword.text = Defaults.value(forKey: "rememberPassword") as? String
            }
        }else{
            self.mainView.btnRememberMe.isSelected = false
        }
    }
}

//MARK:- Button Action
extension LoginVC {
    
    @objc func btnSkipAction() {
        isGuest = true
        setUpTabbarRoot()
    }
    
    @IBAction func btnTopLoginAction(_ sender: Any) {
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnRememberMe(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        }
        else {
            sender.isSelected = true
        }
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        if self.mainView.checkValidation() {
            loginAPI()
        }
    }
    
}

//MARK: - LoginAPi

extension LoginVC{
    
    func loginAPI(){
        
        let dict = ["username":self.mainView.txtEmail.text ?? "",
                    "password":self.mainView.txtPassword.text ?? "",
                    "device_type" : deviceType,
//                    "device_id" : deviceId,
                    "push_token" : fcmToken
                    ]
        
        self.mainVM.loginApi(param: dict) { (data) in
            
            if self.mainView.btnRememberMe.isSelected {
                
                Defaults.setValue(true, forKey: "isRemember")
                Defaults.setValue(self.mainView.txtEmail.text, forKey: "rememberEmail")
                Defaults.setValue(self.mainView.txtPassword.text, forKey: "rememberPassword")
            }
            else {
                Defaults.removeObject(forKey: "isRemember")
                Defaults.removeObject(forKey: "rememberEmail")
                Defaults.removeObject(forKey: "rememberPassword")
            }
            
            Defaults.synchronize()
            isGuest = false
            setUpTabbarRoot()
        }

    }
}
