//
//  LoginVM.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class LoginVM {
    
    var theController = LoginVC()
    
    init(theController: LoginVC) {
        self.theController = theController
    }

}

//MARK: - API calling

extension LoginVM{
    
    
    func loginApi(param:[String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = login
        
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                
                let dict = JSON(data!)
                print("Dict:\(dict)")
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {	
                    savedUserData(user: dict)
//                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
            
        }
        
    }
    
}
