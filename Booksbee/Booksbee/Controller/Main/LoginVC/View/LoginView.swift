//
//  LoginView.swift
//  Booksbee
//
//  Created by vishal on 24/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class LoginView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var lblWelcomeBack: UILabel!
    @IBOutlet weak var btnTopLogin: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLogin: CustomButton!
    
    
    func setUpUI() {
        
        backgroundColor = .appBackgroundColor
        
        [lblWelcomeBack].forEach { (lbl) in
            lbl?.textColor = UIColor.appTitleTextColor
            lbl?.font = themeFont(size: 29, fontname: .bold)
            lbl?.text = "Welcome Back".localized
        }
        
        btnTopLogin.setTitle("Login".localized, for: .normal)
        
        [btnTopLogin, btnSignup].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .bold)
            btn?.setTitleColor(.appGoldenColor, for: .normal)
        }
        
        [btnSignup].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .bold)
            btn?.setTitleColor(.appDetailTextColor, for: .normal)
            btn?.setTitle("Sign Up".localized, for: .normal)
        }
        
        [btnLogin].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .bold)
            btn?.setTitleColor(.white, for: .normal)
            btn?.backgroundColor = .appTitleTextColor
            btn?.setTitle("Login".localized, for: .normal)
        }
        
        [btnRememberMe].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .medium)
            btn?.setTitleColor(.appDetailTextColor, for: .normal)
            btn?.setTitle("Remember me".localized, for: .normal)
        }
        
        [btnForgotPassword].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .medium)
            btn?.setTitleColor(.appTitleTextColor, for: .normal)
            btn?.setTitle("Forgot Password?".localized, for: .normal)
        }
        
        [txtEmail, txtPassword].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = .appTitleTextColor
            txt?.placeholderColor(.appDetailTextColor)
        }
        
        txtEmail.placeholder = "Username/Email address".localized
        txtPassword.placeholder = "Password".localized
        
        isArabic()
    }
    
    func isArabic() {
        
        if getLanguage() == arabic {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
            [txtEmail, txtPassword].forEach { (txt) in
                txt?.textAlignment = .right
            }
        }
        
    }
    
    func checkValidation() -> Bool {
        
        if txtEmail.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter username/email address".localized)
            return false
        }
//        else if  !isValidEmail(emailAddressString: txtEmail.text ?? "") {
//            makeToast(strMessage: "Please enter valid email address".localized)
//            return false
//        }
        else if txtPassword.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter your password".localized)
            return false
        }
        else if txtPassword.text?.toTrim().count ?? 0 < 6{
            makeToast(strMessage: "Password must be atlist 6 digit long".localized)
            return false
        }
//        else if !isValidPassword(passwordString: txtPassword.text ?? "") {
//            makeToast(strMessage: "Password must contain mix of upper and lower case letters as well as digits and one special charecter".localized)
//            return false
//        }
        return true
    }
    
}
