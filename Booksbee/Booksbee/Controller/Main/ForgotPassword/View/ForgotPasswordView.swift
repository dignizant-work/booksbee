//
//  ForgotPasswordView.swift
//  Booksbee
//
//  Created by vishal on 25/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordView: UIView {
    
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitleForgotPassword: UILabel!
    @IBOutlet weak var lblDescriptionForgotPassword: UILabel!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var btnSend: CustomButton!
    @IBOutlet weak var lblRememberpassword: UILabel!
    @IBOutlet weak var btnLogin: UIButton!

    
    func setUpUI() {
        
        [lblTitleForgotPassword].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 18, fontname: .bold)
        }
        
        [lblDescriptionForgotPassword, lblRememberpassword].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 18, fontname: .medium)
        }
        
        [txtEmailAddress].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = .appTitleTextColor
            txt?.placeholderColor(.appDetailTextColor)
            txt?.placeholder = "Email Address".localized
        }
        
        [btnSend].forEach { (btn) in
            btn?.backgroundColor = .appTitleTextColor
            btn?.setTitleColor(.white, for: .normal)
            btn?.setTitle("SEND".localized, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .bold)
        }
        
        [btnLogin].forEach { (btn) in
            btn?.backgroundColor = .clear
            btn?.setTitleColor(.appGoldenColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .medium)
        }
        
        lblTitleForgotPassword.text = "Forgot your Password?".localized
        lblDescriptionForgotPassword.text = "Enter your registered email below To receive password reset instruction".localized
        lblRememberpassword.text = "Remember password".localized
        
    }
    
}
