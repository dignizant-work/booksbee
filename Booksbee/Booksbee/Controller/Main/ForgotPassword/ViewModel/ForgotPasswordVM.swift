//
//  ForgotPasswordVM.swift
//  Booksbee
//
//  Created by vishal on 25/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ForgotPasswordVM {
    
    //MARK: Variables
    var theController = ForgotPasswordVC()
    
    init(theController: ForgotPasswordVC) {
        self.theController = theController
    }
    
}

//MARK: - API calling

extension ForgotPasswordVM{
    
    
    func forgotPasswordAPI(param:[String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = forgotPassword
        
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                
                let dict = JSON(data!)
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
            
        }
        
    }
    
}
