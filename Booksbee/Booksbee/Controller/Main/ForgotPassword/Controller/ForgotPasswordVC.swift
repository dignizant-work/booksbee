//
//  ForgotPasswordVC.swift
//  Booksbee
//
//  Created by vishal on 25/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    //MARK: Variables
    lazy var mainView: ForgotPasswordView = {
        return self.view as! ForgotPasswordView
    }()
    
    lazy var mainVM: ForgotPasswordVM = {
        return ForgotPasswordVM(theController: self)
    }()
    
    
    //MARK:- Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.mainView.setUpUI()
        setupNavigationbarBackButton(titleText: "")
        setupTransparentNavigationBar()
//        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
        
}

//MARK:- Button Action
extension ForgotPasswordVC {
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        if self.mainView.txtEmailAddress.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter email address")
            return
        }
        else if !isValidEmail(emailAddressString: self.mainView.txtEmailAddress.text ?? "") {
            makeToast(strMessage: "Please enter valid email address")
            return
        }
        else{
            self.forgotPasswordAPI()
        }
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK: - ForgotPasswordAPI

extension ForgotPasswordVC{
    
    func forgotPasswordAPI(){
        
        let dict = [
                    "email":self.mainView.txtEmailAddress.text ?? "",
                    ] as [String : Any]
        
        self.mainVM.forgotPasswordAPI(param: dict) { (data) in
            self.navigationController?.popViewController(animated: false)
        }

    }

}


