//
//  TabbarVC.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class TabbarVC: UITabBarController {
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        setSwipeDirection()
        
        //MARK: SetUp view Tabbar
        let home = homeStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let homeTabItem = UITabBarItem(title: "Home".localized, image: UIImage(named: "ic_futter_menu_home_unselect")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "ic_futter_menu_home_select")?.withRenderingMode(.alwaysOriginal))
        home.tabBarItem = homeTabItem
        
        let browse = homeStoryboard.instantiateViewController(withIdentifier: "BrowseVC") as! BrowseVC
        let browseTabItem = UITabBarItem(title: "Browse".localized, image: UIImage(named: "ic_futter_menu_browse_unselect")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "ic_futter_menu_browse_select")?.withRenderingMode(.alwaysOriginal))
        browse.tabBarItem = browseTabItem
        
        let search = profileStoryboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        
        let organization = homeStoryboard.instantiateViewController(withIdentifier: "OrganizationVC") as! OrganizationVC
        let organizationTabItem = UITabBarItem(title: "Organization".localized, image: UIImage(named: "ic_futter_menu_organization_unselect")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "ic_futter_menu_organization_select")?.withRenderingMode(.alwaysOriginal))
        organization.tabBarItem = organizationTabItem

        let myProfile = profileStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        let myProfileTabItem = UITabBarItem(title: "My Profile".localized, image: UIImage(named: "ic_futter_menu_profile_splash_holder")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "ic_futter_menu_profile_Selected")?.withRenderingMode(.alwaysOriginal))
        myProfile.tabBarItem = myProfileTabItem
        
        let n1 = UINavigationController(rootViewController: home)
        let n2 = UINavigationController(rootViewController: browse)
        let n3 = UINavigationController(rootViewController: search)
        n3.title = "Search".localized
        let n4 = UINavigationController(rootViewController: organization)
        let n5 = UINavigationController(rootViewController: myProfile)
        
        n1.isNavigationBarHidden = false
        n2.isNavigationBarHidden = false
        n3.isNavigationBarHidden = false
        n4.isNavigationBarHidden = false
        n5.isNavigationBarHidden = false
        
        let unselectedTitle = [NSAttributedString.Key.foregroundColor: UIColor.appDetailTextColor,
                               NSAttributedString.Key.font: themeFont(size: 13, fontname: .medium)]
        
        let selectedTitle = [NSAttributedString.Key.foregroundColor: UIColor.appTitleTextColor,
                               NSAttributedString.Key.font: themeFont(size: 13, fontname: .medium)]
        
        UITabBarItem.appearance().setTitleTextAttributes(unselectedTitle, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(selectedTitle, for: .selected)
        
        setUpTabUnderLin()
        
        self.viewControllers = [n1, n2, n3, n4, n5]
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.isTranslucent = false
        self.setMiddleSeachButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillLayoutSubviews() {
        if tabBar.isHidden {
            middleButton.isUserInteractionEnabled = false
            middleButton.frame.size = CGSize(width: 0, height: 0)
        }
        else {
            middleButton.isUserInteractionEnabled = true
            middleButton.frame.size = CGSize(width: 50, height: 50)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // TabBarButton – Setup Middle Button
    func setMiddleSeachButton() {
                
        middleButton.tag = 111
        middleButton.frame.size = CGSize(width: 50, height: 50)
        middleButton.layer.cornerRadius = 25
        middleButton.layer.masksToBounds = true
        middleButton.center = CGPoint(x: tabBar.frame.width / 2, y: 0)
        tabBar.addSubview(middleButton)
        
        middleButton.setImage(UIImage(named: "ic_search_selected"), for: UIControl.State.normal)
        middleButton.addTarget(self, action: #selector(menuButtonAction), for: .touchUpInside)
    }

    // Menu Button Touch Action
    @objc func menuButtonAction(sender: UIButton) {
        self.selectedIndex = 2
    }
    
    func setUpTabUnderLin() {
        
        UITabBar.appearance().selectionIndicatorImage = getImageWithColorPosition(color: UIColor.appGoldenColor, size: CGSize(width:(screenWidth)/10,height: 45), lineSize: CGSize(width:(screenWidth)/4, height:3))
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
    }
    
}

extension UITabBar{
    
//    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
//        let from = point
//        let to = middleButton.center
//        return sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)) <= 25 ? middleButton : super.hitTest(point, with: event)
//
//    }
    
}

//MARK: - Tabbar delegate methods
extension TabbarVC : UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let navController = viewController as? UINavigationController
        navController?.popToRootViewController(animated: true)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        
        guard
            let tab = tabBarController.viewControllers?.firstIndex(of: viewController), [4].contains(tab)
            else {
                if let vc = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController {
                    vc.popToRootViewController(animated: false)
                }
                return true
        }
        
        if isGuest
        {
            
            self.showGuestUserAlert()            
            return false
        }
        else
        {
            if let vc = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController {
                vc.popToRootViewController(animated: false)
            }
            return true
        }
        
    }

}
