//
//  MyProfileTableCell.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class MyProfileTableCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var vwBg: CustomView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var imgProfileWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnNotification: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
