//
//  MyProfileView.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class MyProfileView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgDP: UIImageView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var tblMyProfile: UITableView!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    
    func setUpUI() {
        
        
        imgBG.image = UIImage(named: "ic_profile_bg")?.imageFlippedForRightToLeftLayoutDirection()
                
        [lblEmail].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 19, fontname: .medium)
        }
        
        [lblUsername].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 22, fontname: .bold)
        }
        
        vwBG.clipsToBounds = true
        vwBG.layer.cornerRadius = 30
        vwBG.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        imgDP.layer.cornerRadius = imgDP.frame.size.height/2
        imgDP.layer.masksToBounds = true
        
        registerXIB()
    }
    
    func registerXIB() {
        tblMyProfile.register(UINib(nibName: "MyProfileTableCell", bundle: nil), forCellReuseIdentifier: "MyProfileTableCell")
    }
    
    func setData(){
        self.lblEmail.text = getUserData()?.data?.email
        self.lblUsername.text = getUserData()?.data?.name
        
        self.imgDP.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgDP.sd_setImage(with: (getUserData()?.data?.profile ?? "").toURL(), placeholderImage: UIImage(named: "ic_my_profile_nig_splash_holder"), options: .lowPriority, completed: nil)
    }
			    
}
