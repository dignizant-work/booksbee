//
//  MyProfileVM.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MyProfileVM {
    
    //MARK: Variables
    var theController = MyProfileVC()
    var arrayProfileData : [JSON] = []
    
    init(theController: MyProfileVC) {
        
        self.theController = theController
        setUpArray()
    }
    
    func setUpArray() {
        
        var dict = JSON()
        dict["imd"].stringValue = "ic_accounr_setting"
        dict["title"].stringValue = "Account Setting".localized
        arrayProfileData.append(dict)
        
        dict = JSON()
        dict["imd"].stringValue = "ic_accounr_setting"
        dict["title"].stringValue = "Change language".localized
        arrayProfileData.append(dict)
        
        dict = JSON()
        dict["imd"].stringValue = "ic_password"
        dict["title"].stringValue = "Password".localized
        arrayProfileData.append(dict)
        
        dict = JSON()
        dict["imd"].stringValue = "ic_terms_conditions"
        dict["title"].stringValue = "Terms and conditions".localized
        arrayProfileData.append(dict)
        
        dict = JSON()
        dict["imd"].stringValue = "ic_privacy_policy"
        dict["title"].stringValue = "Privacy policy".localized
        arrayProfileData.append(dict)
        
        dict = JSON()
        dict["imd"].stringValue = ""
        dict["title"].stringValue = ""
        arrayProfileData.append(dict)
        
        dict = JSON()
        dict["imd"].stringValue = ""
        dict["title"].stringValue = "Logout".localized
        arrayProfileData.append(dict)
        
    }
    
}

//MARK: - API calling

extension MyProfileVM{
    
    
    func logoutAPI(param:[String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = logout
        
        showIndicator()
        
        WebServices().MakePostAPI(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                
                let dict = JSON(data!)
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}

