//
//  MyProfileVC.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController {

    //MARK: Variables
    lazy var mainView: MyProfileView = {
        return self.view as! MyProfileView
    }()
    
    lazy var mainVM: MyProfileVM = {
        return MyProfileVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationbarBottomLineHidden()
        setupNavigationbarTitle(titleText: "My Profile".localized, barColor: .appBackgroundColor)
        
        self.setUpScreen()
        CommonServices().getUserProfileAPI { response in }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mainView.setData()
        redirectPushData()
    }
    
}

//MARK:- SetUp
extension MyProfileVC {
    
    func setUpScreen() {
        self.mainView.setUpUI()
        self.mainView.tblHeightConstraint.constant = CGFloat(self.mainVM.arrayProfileData.count*68)
        setUpLeftnavigationBtn()
    }
    
    func setUpLeftnavigationBtn() {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        btn.backgroundColor = .appGoldenColor
        btn.setImage(UIImage(named: "ic_white_notification"), for: .normal)
        btn.layer.cornerRadius = 20
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(btnLeftNavigationAction), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)
    }
    
    @objc func btnLeftNavigationAction() {
        let vc = homeStoryboard.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationVc
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func redirectPushData() {
        
        DispatchQueue.main.async {
            if isBookBidWinner {
                let vc = homeStoryboard.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationVc
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else if isAdminNotification {
                isAdminNotification = false
                let vc = homeStoryboard.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationVc
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else if isOrderStatusNotification {
                let vc = homeStoryboard.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationVc
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
}


//MARK: - SignUpAPI

extension MyProfileVC{
    
    func logoutAPI(){
        
        let dict = ["push_token" : fcmToken,
                    "user_id":getUserData()?.data?.id ?? 0] as [String : Any]
        
        self.mainVM.logoutAPI(param: dict) { (data) in
            removeUserData()
            let vc  = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let rearNavigation = UINavigationController(rootViewController: vc)
            appdelgate.window?.rootViewController = rearNavigation
        }

    }

}
