//
//  MyProfileVC_TableviewDelegate.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Tableview delegate/datasource
extension MyProfileVC: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.mainVM.arrayProfileData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableCell", for: indexPath) as! MyProfileTableCell
        
        let dict = self.mainVM.arrayProfileData[indexPath.row]
        cell.lblTitle.text = dict["title"].stringValue
        
        if (dict["imd"].stringValue != "") {
            cell.imgIcon.image = UIImage(named: dict["imd"].stringValue)?.imageFlippedForRightToLeftLayoutDirection()
            cell.imgProfileWidthConstraint.constant = 44
        }
        
        cell.vwBg.isHidden = false
        if dict["title"].stringValue == "" {
            cell.vwBg.isHidden = true
        }
        
        cell.imgArrow.tintImageColor(color: .appTitleTextColor)
        cell.vwBg.backgroundColor = .appBackgroundColor
        cell.lblTitle.textColor = .appTitleTextColor
        cell.imgArrow.image = UIImage(named: "ic_profile_left-arrow")?.imageFlippedForRightToLeftLayoutDirection()
        if dict["title"].stringValue == "Logout".localized {
            cell.imgArrow.tintImageColor(color: .white)
            cell.vwBg.backgroundColor = .appTitleTextColor
            cell.lblTitle.textColor = .white
            cell.imgProfileWidthConstraint.constant = 15
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.mainVM.arrayProfileData[indexPath.row]
        
        if dict["title"].stringValue == "Account Setting".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "AccountSettingVC") as! AccountSettingVC
            vc.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "Change language".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "ChangeLanguageVC") as! ChangeLanguageVC
            vc.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "Password".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "PasswordVC") as! PasswordVC
            vc.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "Terms and conditions".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
            vc.hidesBottomBarWhenPushed = true
            vc.mainVM.isControllerCheck = .termsCondition
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "Privacy policy".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
            vc.hidesBottomBarWhenPushed = true
            vc.mainVM.isControllerCheck = .privacyPolicy
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "Logout".localized {
            
            DispatchQueue.main.async {
                self.showAlert(appName, "Are you sure you want to logout?".localized, "Logout".localized) {
                    self.logoutAPI()
                }
            }
        }
    }
    
}
