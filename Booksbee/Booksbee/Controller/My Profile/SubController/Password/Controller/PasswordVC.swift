//
//  MyProfileVM.swift
//  Booksbee
//
//  Created by vishal on 28/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class PasswordVC: UIViewController {

    //MARK: Variables
    lazy var mainView: PasswordView = {
        return self.view as! PasswordView
    }()
    
    lazy var mainVM: PasswordVM = {
        return PasswordVM(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationbarBackButton(titleText: "Password".localized)
        self.mainView.setUpUI()
    }
    
    @IBAction func btnSavePasswordAction(_ sender: Any) {
        
        if self.mainView.checkValidation() {
            chanegPasswordAPI()
        }
        
    }
}

//MARK: - Change password API

extension PasswordVC{
    
    func chanegPasswordAPI(){
        
        let dict = ["password":self.mainView.txtOldPassword.text ?? "",
                    "newpassword":self.mainView.txtNewPassword.text ?? "",
                    "confirmpassword":self.mainView.txtConfirmPassword.text ?? "",
                    ]
        
        self.mainVM.changePasswordAPI(param: dict) { (data) in
            
            self.backButtonTapped()

        }

    }
}
