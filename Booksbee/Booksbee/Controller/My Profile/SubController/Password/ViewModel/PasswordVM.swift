//
//  MyProfileVM.swift
//  Booksbee
//
//  Created by vishal on 28/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PasswordVM {
    
    //MARK: Variables
    var theController = PasswordVC()
    
    init(theController: PasswordVC) {
        self.theController = theController
    }
    
}

//MARK: - API calling

extension PasswordVM{
    
    
    func changePasswordAPI(param:[String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = changePassword
        
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                
                let dict = JSON(data!)
                print("Dict:\(dict)")
                
                            
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
            
        }
        
    }
    
}
