//
//  MyProfileView.swift
//  Booksbee
//
//  Created by vishal on 28/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class PasswordView: UIView {
    
    //MARK: Variables
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var btnSavePassword: CustomButton!
    
    func setUpUI() {
        
        imgBG.image = UIImage(named: "ic_profile_bg")?.imageFlippedForRightToLeftLayoutDirection()
        [vwBG].forEach { (vw) in
            vw?.clipsToBounds = true
            vw?.layer.cornerRadius = 30
            vw?.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        
        [txtOldPassword, txtNewPassword, txtConfirmPassword].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.textColor = .appTitleTextColor
            txt?.placeholderColor(.appDetailTextColor)
            if getLanguage() == arabic {
                txt?.textAlignment = .right
            }
        }
        
        txtOldPassword.placeholder = "Old Password".localized
        txtNewPassword.placeholder = "New Password".localized
        txtConfirmPassword.placeholder = "Confirm New Password".localized
    }
    
    
    func checkValidation() -> Bool {
        if txtOldPassword.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter old password".localized)
            return false
        }
        else if txtNewPassword.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter new password".localized)
            return false
        }
        else if !isValidPassword(passwordString: txtNewPassword.text ?? "") {
            makeToast(strMessage: "Password must contain mix of upper and lower case letters as well as digits and one special charecter".localized)
            return false
        }
        else if txtConfirmPassword.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter confirm password".localized)
            return false
        }
        else if !isValidPassword(passwordString: txtConfirmPassword.text ?? "") {
            makeToast(strMessage: "Password must contain mix of upper and lower case letters as well as digits and one special charecter".localized)
            return false
        }
        else if txtNewPassword.text ?? "" != txtConfirmPassword.text ?? "" {
            makeToast(strMessage: "New password and confirm password not match".localized)
            return false
        }
        return true
    }
    
}
