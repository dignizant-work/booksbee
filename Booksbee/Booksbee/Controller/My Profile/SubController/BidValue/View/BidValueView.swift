//
//  BoodValueView.swift
//  Booksbee
//
//  Created by vishal on 09/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class BidValueView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var tblBidValue: UITableView!
    
    //MARK: SetUpUI
    func setUpUI() {
        
        registerXib()
    }
    
    func registerXib() {
        
        self.tblBidValue.register(UINib(nibName: "BidListTableCell", bundle: nil), forCellReuseIdentifier: "BidListTableCell")
    }
}
