//
//  BidValueVC_UiTableDelegate.swift
//  Booksbee
//
//  Created by vishal on 09/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import Foundation
import UIKit

//MARK: tableView Delegate/Datasource
extension BidValueVC : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainVM.dictBookdata?.bidData.count ?? 0 == 0 {
            setTableViewEmptyMessage(self.mainVM.dictBookdata?.message ?? "", tableView: tableView)
            return self.mainVM.dictBookdata?.bidData.count ?? 0
        }
        tableView.backgroundView = nil
        return self.mainVM.dictBookdata?.bidData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BidListTableCell", for: indexPath) as! BidListTableCell
        
        if let dict = self.mainVM.dictBookdata?.bidData[indexPath.row] {
            cell.setUpData(dict: dict)
        }
        
        return cell
    }
    
}

