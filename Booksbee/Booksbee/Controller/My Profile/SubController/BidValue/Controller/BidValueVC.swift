//
//  BidValueVC.swift
//  Booksbee
//
//  Created by vishal on 09/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import UIKit

class BidValueVC: UIViewController {

    //MARK: Variables
    lazy var mainView: BidValueView = {
        return self.view as! BidValueView
    }()
    
    lazy var mainVM: BidValueVM = {
        return BidValueVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationbarBackButton(titleText: "Book All Bidding".localized)
        self.mainView.setUpUI()
        self.mainVM.getAllBidListingValueAPI { data in
            self.mainView.tblBidValue.reloadData()
        }
    }
    
}
