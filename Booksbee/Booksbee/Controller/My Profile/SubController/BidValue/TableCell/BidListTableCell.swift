//
//  BidListTableCell.swift
//  Booksbee
//
//  Created by vishal on 09/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import UIKit
import SDWebImage

class BidListTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserPrize: UILabel!
    @IBOutlet weak var lblBidDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutIfNeeded() {
        [imgUserProfile].forEach { (img) in
            img?.layer.cornerRadius = (img?.frame.size.height ?? 0.0)/2
            img?.clipsToBounds = true
        }
    }
    
    func setUpUI() {
        
        [lblUserName].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        
    }
    
    func setUpData(dict: BidData) {
        
        imgUserProfile.sd_setImage(with: dict.profile.toURL(), completed: nil)
        lblUserName.text = dict.name
        lblUserPrize.attributedText = stringMultipleColorSet(first: "Bid Prize :".localized, firstTxtFont: themeFont(size: 12, fontname: .medium), second: " \(dict.bidPrice)", secondTxtFont: themeFont(size: 12, fontname: .regular))
        lblBidDate.attributedText = stringMultipleColorSet(first: "Bid Date :".localized, firstTxtFont: themeFont(size: 12, fontname: .medium), second: " \(dict.bidTime)", secondTxtFont: themeFont(size: 12, fontname: .regular))
    }
    
}
