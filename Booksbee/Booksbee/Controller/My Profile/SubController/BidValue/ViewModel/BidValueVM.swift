//
//  BoodValueVM.swift
//  Booksbee
//
//  Created by vishal on 09/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class BidValueVM {
    
    //MARK: Variables
    var theController = BidValueVC()
    var bookId = 0
    var dictBookdata : BidListdata? = nil
    
    
    init(theController: BidValueVC) {
        self.theController = theController
    }
    
    func getAllBidListingValueAPI(completionHandlor:@escaping(JSON)->Void) {
        
        let url = bidListingURL
        let param = ["book_id":bookId] as [String:Any]
        
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
                print("Dict:-\(dict)")
                self.dictBookdata = BidListdata(JSON: dict.dictionaryObject!)
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
