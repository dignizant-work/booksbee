//
//  SuccessFailVc.swift
//  Booksbee
//
//  Created by YASH on 12/8/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class SuccessFailVc: UIViewController {

    //MARK: Variables
    lazy var mainView: SuccessFailView = {
        return self.view as! SuccessFailView
    }()
    
    lazy var mainVM: SuccessFailVM = {
        return SuccessFailVM(theDelegate: self)
    }()

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if self.mainVM.isCheckController == .success {
            self.mainView.successPopup()
        }
        else if self.mainVM.isCheckController == .fail {
            self.mainView.failPopup()
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackHomeOrClosreTapped(_ sender: UIButton) {
        setUpTabbarRoot()
    }
}
