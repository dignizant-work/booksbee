//
//  SuccessFailView.swift
//  Booksbee
//
//  Created by YASH on 12/8/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class SuccessFailView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var vwVackground: CustomView!
    @IBOutlet weak var lblSubMsg: UILabel!
    @IBOutlet weak var lblMainMsg: UILabel!
    @IBOutlet weak var btnBottom: CustomButton!
    
    //MARK: - setup
    
    func setupUI(){
        lblMainMsg.font = themeFont(size: 29, fontname: .medium)
        lblSubMsg.font = themeFont(size: 12, fontname: .regular)
        
        btnBottom.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        self.btnBottom.backgroundColor = UIColor.white
    }
    
    func successPopup(){
        
        self.img.image = UIImage(named: "ic_checked")
        self.lblMainMsg.textColor = UIColor.black
        self.lblSubMsg.textColor = UIColor.black
        
        self.btnBottom.setTitle("Back to Home".localized, for: .normal)
        self.btnBottom.setTitleColor(UIColor.black, for: .normal)
//        self.btnBottom.titleLabel?.textColor = UIColor.black
        self.vwVackground.backgroundColor = UIColor.hexStringToUIColor(hex: "#96FFB3")
    }
    
    func failPopup(){
        self.img.image = UIImage(named: "ic_closed")
        self.lblMainMsg.textColor = UIColor.white
        self.lblSubMsg.textColor = UIColor.white
        
        self.btnBottom.setTitle("Try Again".localized, for: .normal)
        self.btnBottom.setTitleColor(UIColor.hexStringToUIColor(hex: "#F55050"), for: .normal)
//        self.btnBottom.titleLabel?.textColor = UIColor.hexStringToUIColor(hex: "#F55050")
        self.vwVackground.backgroundColor = UIColor.hexStringToUIColor(hex: "#F55050")
    }
}
