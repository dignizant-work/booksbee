//
//  ContactUsVC.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {

    //MARK: Variables
    lazy var mainView: ContactUsView = {
        return self.view as! ContactUsView
    }()
    
    //MARK:Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationbarBackButton(titleText: "Contact Us".localized)
        self.mainView.setUpUI()
    }
    
    @IBAction func btnEmailAction(_ sender: Any) {
        print("Email tapped")
    }
    
    @IBAction func btnCallAction(_ sender: Any) {
        dialNumber(number: "0000000000")
    }
}
