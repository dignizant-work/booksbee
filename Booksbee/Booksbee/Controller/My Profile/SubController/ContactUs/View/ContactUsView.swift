//
//  ContactUsView.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class ContactUsView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var btnEmail: CustomButton!
    @IBOutlet weak var btnCall: CustomButton!
    
    func setUpUI() {
        
        imgBG.image = UIImage(named: "ic_profile_bg")?.imageFlippedForRightToLeftLayoutDirection()
        
        
        [vwBG].forEach { (vw) in
            vw?.clipsToBounds = true
            vw?.layer.cornerRadius = 30
            vw?.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        
        [btnCall].forEach { (btn) in
            btn?.setTitleColor(.appTitleTextColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .medium)
        }
        
        [btnEmail].forEach { (btn) in
            btn?.setTitleColor(.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .medium)
        }
        
    }
}
