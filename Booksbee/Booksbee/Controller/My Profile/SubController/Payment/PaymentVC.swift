//
//  PaymentVC.swift
//  Booksbee
//
//  Created by vishal on 07/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit
import Alamofire

class PaymentVC: UIViewController {

    //MARK: Variables
    var webView: WKWebView!
    var dictPaymentData = JSON()
    var bookId = ""
    var addressId = 0
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        setUpUI()
    }
    
    func setUpUI() {
        
        setupNavigationbarWithoutBackButton(titleText: "")
        setupTransparentNavigationBar()
        setUpRightNavigationButton()
        
        webView = WKWebView()
        showIndicator()
        webView.navigationDelegate = self
        webView.load(URLRequest(url: dictPaymentData["data"]["InvoiceURL"].url!))
        view = webView
    }
    
    func setUpRightNavigationButton() {
        let btn = UIButton()
        btn.setImage(UIImage(named: "ic_delete"), for: .normal)
        btn.addTarget(self, action: #selector(btnCloseAction), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)
    }
    
    @objc func btnCloseAction() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension PaymentVC : WKNavigationDelegate {
        
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        showIndicator()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        hideIndicator()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        print("navigation URL \(navigationAction.request.url!)")
        
        if let urlStr = navigationAction.request.url?.absoluteString {
            print("navigationAction URL \(urlStr)")
            if urlStr.contains("payment_transaction_status"){
                print("Got it")
                getJsonDecode(url: urlStr) {
                    
                }
                decisionHandler(.cancel)
                return
            }
        }
        decisionHandler(.allow)
    }
    
    func getJsonDecode(url:String,completionHandelor:@escaping()->Void) {
        
        print("URL:-\(url)")
        Alamofire.request(url, method: .get, parameters: [String : Any](), encoding: URLEncoding.default, headers: nil).validate().responseJSON { (data) in
            
            if let response = data.result.value as? NSDictionary {
                let dict = JSON(response)
                print("json: \(dict)")
                
                if dict["Data"]["InvoiceTransactions"][0]["TransactionStatus"].stringValue == "Succss" {
                    var param = ["payment_type":"1",
                                 "address_id":self.addressId,
                                 "fatoorah_payment_id":dict["Data"]["InvoiceTransactions"][0]["PaymentId"].stringValue,
                                 "payment_gateway":dict["Data"]["InvoiceTransactions"][0]["PaymentGateway"].stringValue] as [String:Any]
                    
                    if self.bookId != "" {
                        param["book_id"] = self.bookId
                    }
                    
                    self.addOrderAPI(param: param)
                }
                else {
                    hideIndicator()
                    
                    let vc = profileStoryboard.instantiateViewController(withIdentifier: "SuccessFailVc") as! SuccessFailVc
                    vc.mainVM.isCheckController = .fail
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                completionHandelor()
            }
        }
    }
    
    func addOrderAPI(param:[String:Any]) {
        
        let url = addOrderURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let vc = profileStoryboard.instantiateViewController(withIdentifier: "SuccessFailVc") as! SuccessFailVc
                vc.mainVM.isCheckController = .success
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
}
