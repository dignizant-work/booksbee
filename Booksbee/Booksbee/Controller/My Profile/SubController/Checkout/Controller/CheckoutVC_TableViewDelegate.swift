//
//  CheckoutVC_TableViewDelegate.swift
//  Booksbee
//
//  Created by vishal on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension CheckoutVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainVM.dictAddressData?.data.count ?? 0 == 0 || self.mainVM.dictAddressData?.data == nil{
            setTableViewEmptyMessage(self.mainVM.dictAddressData?.message ?? "", tableView: tableView)
            return 0
        }
    
        tableView.backgroundView = nil
        return self.mainVM.dictAddressData?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAddressTableCell", for: indexPath) as! MyAddressTableCell
        cell.vwBG.backgroundColor = .white
        if let dict = self.mainVM.dictAddressData?.data[indexPath.row] {
            cell.setUpData(data: dict)
        }
        cell.btnDefault.tag = indexPath.row
        cell.btnDefault.addTarget(self, action: #selector(setUpDefaultAddress(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func setUpDefaultAddress(_ sender: UIButton) {
        
        showAlert("Are you sure?".localized, "You want to change your delivery address".localized) {
            
            
            let arr : [AddressData] = self.mainVM.dictAddressData?.data ?? []
            for i in 0..<arr.count {
                let data = self.mainVM.dictAddressData?.data[i]
                data?.isDefault = 0
                self.mainVM.dictAddressData?.data[i] = data!
            }
            self.mainVM.dictAddressData?.data[sender.tag].isDefault = 1
            self.mainView.tblAddAddress.reloadData()
        }
    }
}
