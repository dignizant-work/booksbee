//
//  CheckoutVC.swift
//  Booksbee
//
//  Created by vishal on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SwiftyJSON

class CheckoutVC: UIViewController {

    //MARK: Variables
    lazy var mainView: CheckoutView = {
        return self.view as! CheckoutView
    }()
    
    lazy var mainVM: CheckoutVM = {
        return CheckoutVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        self.setupNavigationbarBackButton(titleText: "Checkout".localized)
        
        self.mainView.setUpData(data: self.mainVM.dictBookData!)
        
        self.mainVM.getAddressListAPI { data in
            
            if self.mainVM.dictAddressData?.data.count ?? 0 < 2 {
                self.mainView.tblAddressHeightConstraint.constant = 150
            }
            else {
                self.mainView.tblAddressHeightConstraint.constant = 290
            }
            
            self.mainView.tblAddAddress.reloadData()
        }
        
    }
    
    
    @IBAction func btnCODAction(_ sender: Any) {
        self.mainView.isCODSelected()
    }
    
    @IBAction func btnMyFatoorahAction(_ sender: Any) {
        self.mainView.isMyFatoorahSelected()
    }
    
    @IBAction func btnAddNewAction(_ sender: Any) {
        
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        vc.mainVM.handlorAddUpdateAddress = {
            self.mainVM.getAddressListAPI { data in
                self.mainView.tblAddAddress.reloadData()
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCheckoutAction(_ sender: Any) {
        
        if self.mainVM.dictAddressData?.data.count == 0 {
            makeToast(strMessage: "Add delivery address")
            return
        }
        
        let selectAddress = self.mainVM.dictAddressData?.data.filter({ (data) -> Bool in
            let isSelect = data.isDefault == 0 ? false : true
            return isSelect
        })
        
        self.showAlert(appName, "Are you sure you want to buy this book?".localized, "Order".localized, "Cancel".localized) {
            
            if self.mainView.btnCOD.isSelected {
                //MARK: COD payment selected
                let addressId = selectAddress?.first?.id ?? 0
                print("AddressId:-\(addressId)")
                
                var param = ["payment_type":0,
                             "address_id":addressId] as [String:Any]
                
                if self.mainVM.dictBookData?.bookDataModel?.isBidActive == 1 {
                    param["book_id"] = self.mainVM.dictBookData?.bookDataModel?.bookId
                }
                
                self.mainVM.addOrderAPI(param: param) {
                    let vc = profileStoryboard.instantiateViewController(withIdentifier: "SuccessFailVc") as! SuccessFailVc
                    vc.mainVM.isCheckController = .success
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else {
                //MARK: FATOORAH payment
                self.mainVM.setfatoorahPayment { data in
                    let vc = profileStoryboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    vc.addressId = selectAddress?.first?.id ?? 0
                    vc.dictPaymentData = data
                    if self.mainVM.dictBookData?.bookDataModel?.isBidActive == 1 {
                        vc.bookId = String(describing: self.mainVM.dictBookData?.bookDataModel?.bookId)
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
