//
//  CheckoutVM.swift
//  Booksbee
//
//  Created by vishal on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class CheckoutVM {
    
    //MARK: Variables
    var theController = CheckoutVC()
    var dictBookData:BookModel? = nil
    var dictAddressData = AddressModel(JSON: JSON().dictionaryValue)
    
    init(theController: CheckoutVC) {
        self.theController = theController
    }
    
    func getAddressListAPI(completionHandlor:@escaping(JSON)->Void) {
        
        let url = myAddressesURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
//                print("DICT:- \(dict)")
//                makeToast(strMessage: dict["message"].stringValue)
                self.dictAddressData = AddressModel(JSON: dict.dictionaryObject!)
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func addOrderAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = addOrderURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
                print("DICT:- \(dict)")
//                makeToast(strMessage: dict["message"].stringValue)
                completionHandlor()
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func setfatoorahPayment(completionHandlor:@escaping(JSON)->Void) {
        
        var arritem : [JSON] = []
        var dictitem = JSON()
        dictitem["ItemName"].stringValue = "\(appName) product"
        dictitem["Quantity"].intValue = 1
        dictitem["UnitPrice"].stringValue = self.dictBookData?.bookDataModel?.price ?? ""
        arritem.append(dictitem)
        
        let dict = self.dictBookData?.bookDataModel?.address
       
        var dictaddress = JSON()
        dictaddress["Block"].stringValue = self.dictBookData?.bookDataModel?.address?.block ?? ""
        dictaddress["Street"].stringValue = self.dictBookData?.bookDataModel?.address?.street ?? ""
        dictaddress["HouseBuildingNo"].stringValue = self.dictBookData?.bookDataModel?.address?.flat ?? ""
        dictaddress["Address"].stringValue = "\(dict?.avenue ?? ""), \(dict?.street ?? ""), \(dict?.area ?? "")"
        dictaddress["AddressInstructions"].stringValue = dict?.description ?? ""
        
        let parameter  = ["InvoiceValue" : self.dictBookData?.bookDataModel?.price ?? "",
                          "CustomerName" : getUserData()?.data?.name ?? "",
                          "CustomerAddress": dictaddress,
                          "CustomerMobile":getUserData()?.data?.mobileNo ?? "",
                          "CustomerEmail":getUserData()?.data?.email ?? "",
                          "CallBackUrl": "http://52.66.128.99/booksbee/api/payment_transaction_status",
                          "Language":getLanguage(),
                          "NotificationOption":"EML",
                          "MobileCountryCode":getUserData()?.data?.contryCode ?? "",
                          "DisplayCurrencyIso":"KWD",
                          "CustomerReference": "",
                          "CustomerCivilId": "",
                          "UserDefinedField": "",
                          "ExpiryDate": "",
                          "SupplierCode": 0,
                          "SupplierValue": 0,
                          "SourceInfo": "",
                          "InvoiceItems": arritem
        ] as [String : Any]
        
        print("PARAMS:- \(parameter)")
        
        let data = JSON(parameter)
        var strDict = data.rawString() ?? ""
        guard let jsonDict = data.dictionaryObject else {
            return
        }
        
        if let json = try? JSONSerialization.data(withJSONObject: jsonDict, options: []) {
            if let content = String(data: json, encoding: .utf8) {
                strDict = content
                print(strDict)
            }
        }
        
        let param:[String:String] = ["invoiceCreate":strDict]
        
        let url = fatoorahPaymentpageURL
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
                print("DICT:- \(dict)")
                if dict["flag"].boolValue == false {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
        
    }
}
