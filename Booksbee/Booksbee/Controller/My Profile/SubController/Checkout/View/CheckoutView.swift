//
//  CheckoutView.swift
//  Booksbee
//
//  Created by vishal on 17/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

class CheckoutView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var lblChoosePaymentTitle: UILabel!
    @IBOutlet weak var lblCod: UILabel!
    @IBOutlet weak var imgCod: UIImageView!
    @IBOutlet weak var btnCOD: UIButton!
    
    @IBOutlet weak var lblMyFatoorah: UILabel!
    @IBOutlet weak var imgMyFatoorah: UIImageView!
    @IBOutlet weak var btnMyFatoorah: UIButton!
    
    @IBOutlet weak var lblPersonalInformation: UILabel!
    
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhone: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblAddressDelivery: UILabel!
    @IBOutlet weak var lblAddNew: UILabel!
    @IBOutlet weak var btnAddNew: UIButton!
    
    @IBOutlet weak var tblAddAddress: UITableView!
    @IBOutlet weak var tblAddressHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblAmountValue: UILabel!
    
    @IBOutlet weak var btnCheckout: CustomButton!
    
    
    func setUpUI() {
        
        [lblChoosePaymentTitle,lblPersonalInformation,lblAddressDelivery].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 18, fontname: .medium)
        }
        
        [lblCod, lblMyFatoorah].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
        
        [lblTotalAmount].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        
        [lblAmountValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .medium)
            lbl?.textColor = .appGoldenColor
        }
        
        [lblAddNew].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .medium)
            lbl?.textColor = .white
        }
        
        [txtName,txtPhone].forEach { (txt) in
            txt?.titleColor = .appDetailTextColor
            txt?.placeholderColor = .appDetailTextColor
            txt?.textColor = .appTitleTextColor
            txt?.titleFont = themeFont(size: 15, fontname: .medium)
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.placeholderFont = themeFont(size: 15, fontname: .medium)
            txt?.selectedTitleColor = .appTitleTextColor
            txt?.isUserInteractionEnabled = false

            if getLanguage() == arabic {
                txt?.isLTRLanguage = false
            }
        }
        
        [btnCheckout].forEach { (btn) in
            btn?.setTitleColor(.white, for: .normal)
            btn?.backgroundColor = .appTitleTextColor
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .medium)
        }
        
        lblChoosePaymentTitle.text = "Choose a payment method".localized
        lblCod.text = "COD".localized
        lblMyFatoorah.text = "Myfatoorah".localized
        lblPersonalInformation.text = "Personal Information".localized
        lblAddressDelivery.text = "Address delivery".localized
        lblAddNew.text = "Add New".localized
        lblTotalAmount.text = "Total amount".localized
        btnCheckout.setTitle("Checkout".localized, for: .normal)
        
        isCODSelected()
        registerXib()
        
    }
    
    func setUpData(data: BookModel) {
        
        self.lblAmountValue.text = data.bookDataModel?.totalPrice

        txtName.text = getUserData()?.data?.name
        txtPhone.text = "\(getUserData()?.data?.contryCode ?? "") \(getUserData()?.data?.mobileNo ?? "")"
    }
    
        
    func isCODSelected() {
        btnCOD.isSelected = true
        btnMyFatoorah.isSelected = false
        imgCod.image = UIImage(named: "ic_redio_button_selected")
        imgMyFatoorah.image = UIImage(named: "ic_redio_button_unselect")
    }
    
    func isMyFatoorahSelected() {
        btnCOD.isSelected = false
        btnMyFatoorah.isSelected = true
        imgCod.image = UIImage(named: "ic_redio_button_unselect")
        imgMyFatoorah.image = UIImage(named: "ic_redio_button_selected")
    }
    
    func registerXib() {
        self.tblAddAddress.register(UINib(nibName: "MyAddressTableCell", bundle: nil), forCellReuseIdentifier: "MyAddressTableCell")
    }
    
}
