//
//  TermsConditionView.swift
//  Booksbee
//
//  Created by vishal on 28/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class TermsConditionView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var vwLoadData: UIView!
    
    
    //MARK: SetUpUI
    func setUpUI() {
        
        imgBG.image = UIImage(named: "ic_profile_bg")?.imageFlippedForRightToLeftLayoutDirection()
        
        vwBG.clipsToBounds = true
        vwBG.layer.cornerRadius = 30
        vwBG.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
    }
    
}
