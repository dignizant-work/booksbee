//
//  TermsConditionVM.swift
//  Booksbee
//
//  Created by vishal on 28/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class TermsConditionVm {
    
    //MARK: Variables
    var theDelegate = TermsConditionVC()
    var isControllerCheck = enumTermsConditionVCCheck.termsCondition
    
    init(theDelegate: TermsConditionVC) {
        self.theDelegate = theDelegate
    }
    
    func getTermsPrivacyAboutDataApi(completionHandlor:@escaping(JSON)->Void) {
        
        var url = ""
        if isControllerCheck == .termsCondition {
            url = termsURL
        }
        else if isControllerCheck == .privacyPolicy {
            url = privacyURL
        }
        else if isControllerCheck == .aboutAs {
            url = termsURL
        }
        
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
