//
//  TermsConditionVC.swift
//  Booksbee
//
//  Created by vishal on 28/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import WebKit

class TermsConditionVC: UIViewController, WKNavigationDelegate {

    //MARK:- Variables
    lazy var mainView: TermsConditionView = {
        return self.view as! TermsConditionView
    }()
    
    lazy var mainVM: TermsConditionVm = {
        return TermsConditionVm(theDelegate: self)
    }()
    
    @IBOutlet weak var webView: WKWebView!
//    var webView: WKWebView!
    
    //MARK: Controller lide cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        
//        webView = WKWebView()
        webView.navigationDelegate = self
//        view = webView
        
        if mainVM.isControllerCheck == .termsCondition {
            setupNavigationbarBackButton(titleText: "Terms and conditions".localized)
            self.mainVM.getTermsPrivacyAboutDataApi { data in
                self.webView.loadHTMLString(data["data"]["terms"].stringValue, baseURL: nil)
            }
        }
        else if mainVM.isControllerCheck == .privacyPolicy {
            setupNavigationbarBackButton(titleText: "Privacy policy".localized)
            self.mainVM.getTermsPrivacyAboutDataApi { data in
                self.webView.loadHTMLString(data["data"]["terms"].stringValue, baseURL: nil)
            }
        }
        else if mainVM.isControllerCheck == .aboutAs {
            setupNavigationbarBackButton(titleText: "About Us".localized)
            self.mainVM.getTermsPrivacyAboutDataApi { data in
                self.webView.loadHTMLString(data["data"]["terms"].stringValue, baseURL: nil)
            }
        }
    }
    
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        print("Finished loading frame")
    }
    
}
