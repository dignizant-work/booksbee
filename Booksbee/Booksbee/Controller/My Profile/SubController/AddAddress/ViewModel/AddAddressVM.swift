//
//  AddAddressVM.swift
//  Booksbee
//
//  Created by vishal on 01/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class AddAddressVM {
    
    //MARK: Variables
    var theController = AddAddressVC()
    var handlorAddUpdateAddress:()->Void = {}
    var dictAddressData: AddressData? = nil
    
    
    init(theController: AddAddressVC) {
        self.theController = theController
    }
    
    func addAddressApi(param: [String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = addAddressURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
}
