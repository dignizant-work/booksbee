//
//  AddAddressView.swift
//  Booksbee
//
//  Created by vishal on 01/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

class AddAddressView: UIView {
    
    
    //MARK: Outlets
    @IBOutlet weak var lbltitleAddress: UILabel!
    @IBOutlet weak var txtAddressName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtArea: SkyFloatingLabelTextField!
    @IBOutlet weak var txtBlock: SkyFloatingLabelTextField!
    @IBOutlet weak var txtStreet: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAvenue: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFloor: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFlat: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnHouse: UIButton!
    @IBOutlet weak var btnBuilding: UIButton!
    
    @IBOutlet weak var lbldirection: UILabel!
    @IBOutlet weak var lblTxtVwEnterHere: UILabel!
    @IBOutlet weak var txtVwDirection: UITextView!
    
    
    @IBOutlet weak var btnAddAddress: CustomButton!
    
    
    func setUpUI() {
        
        [txtAddressName,txtArea, txtBlock, txtStreet, txtAvenue, txtFloor, txtFlat].forEach { (txt) in
            txt?.selectedTitleColor = .appTitleTextColor
            txt?.textColor = .appTitleTextColor
            txt?.selectedLineColor = .appTitleTextColor
            txt?.titleLabel.font = themeFont(size: 15, fontname: .bold)
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.autocapitalizationType = .sentences
            if getLanguage() == arabic {
                txt?.isLTRLanguage = false
            }
            
        }
        
        [txtVwDirection].forEach { (txt) in
            txt?.textColor = .appTitleTextColor
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.autocapitalizationType = .sentences
            if getLanguage() == arabic {
                lblTxtVwEnterHere.textAlignment = .right
                txt?.textAlignment = .right
            }
        }
        
//        btnHouse.isSelected = true
//        btnHouse.setTitleColor(.black, for: .normal)
//        btnHouse.setImage(UIImage(named: "ic_round_redio_button_select"), for: .normal)
        
        [lblTxtVwEnterHere].forEach { (lbl) in
            lbl?.textColor = .lightGray
            lbl?.font = themeFont(size: 15, fontname: .medium)
            
            if getLanguage() == arabic {
                lblTxtVwEnterHere.textAlignment = .right
                lbl?.textAlignment = .right
            }
        }
        
    }
    
    func setUpData(data: AddressData) {
        
        btnAddAddress.setTitle("Update".localized, for: .normal)
        
        txtAddressName.text = data.addressName
        txtArea.text = data.area
        txtBlock.text = data.block
        txtStreet.text = data.street
        txtAvenue.text = data.avenue
        txtFloor.text = data.floor
        txtFlat.text = data.flat
        
        if data.houseType == "0" {
            btnHouse.isSelected = true
            btnBuilding.isSelected = false
            btnHouse.setTitleColor(.black, for: .normal)
        }
        else {
            btnHouse.isSelected = false
            btnBuilding.isSelected = true
            btnBuilding.setTitleColor(.black, for: .normal)
        }
        if data.description != "" {
            self.lblTxtVwEnterHere.isHidden = true
        }
        txtVwDirection.text = data.description
    }
    
    func checkValidation() -> Bool {
        
        if self.txtAddressName.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter Address name")
            return false
        }
        else if self.txtArea.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter Area name")
            return false
        }
        else if self.txtBlock.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter Block")
            return false
        }
        else if self.txtStreet.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter Street name")
            return false
        }
        else if self.txtAvenue.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter Avenue name")
            return false
        }
        return true
    }
    
}
