//
//  AddAddressVC.swift
//  Booksbee
//
//  Created by vishal on 01/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {

    //MARK: Variables
    lazy var mainView: AddAddressView = {
        return self.view as! AddAddressView
    }()
    
    lazy var mainVM: AddAddressVM = {
        return AddAddressVM(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUI()
        setupNavigationbarBackButton(titleText: "Add new address".localized)
        
        if self.mainVM.dictAddressData != nil {
            self.mainView.setUpData(data: self.mainVM.dictAddressData!)
            setupNavigationbarBackButton(titleText: "Update Address".localized)
        }
        
    }
    
    @IBAction func btnAddAddressAction(_ sender: Any) {
        if self.mainView.checkValidation() {
            addUpdateAddressApiSetup()
        }
    }
    
    
    @IBAction func btnHouseTapped(_ sender: UIButton) {
        self.mainView.btnBuilding.isSelected = false
        self.mainView.btnHouse.isSelected = true
        self.btnChangeColor(selectedButton: self.mainView.btnHouse, unselectedButton: self.mainView.btnBuilding)
    }
    
    @IBAction func btnBuildingTapped(_ sender: UIButton) {
        self.mainView.btnBuilding.isSelected = true
        self.mainView.btnHouse.isSelected = false
        self.btnChangeColor(selectedButton: self.mainView.btnBuilding, unselectedButton: self.mainView.btnHouse)
    }
    
    func btnChangeColor(selectedButton:UIButton, unselectedButton: UIButton){
        selectedButton.setTitleColor(.black, for: .normal)
        selectedButton.setImage(UIImage(named: "ic_round_redio_button_select"), for: .normal)
        unselectedButton.setTitleColor(.lightGray, for: .normal)
        unselectedButton.setImage(UIImage(named: "ic_radio_unselected"), for: .normal)
    }
    
}


//MARK: API Setup
extension AddAddressVC {
    
    func addUpdateAddressApiSetup() {
        
        let param = ["add_name": self.mainView.txtAddressName.text ?? "",
                     "area": self.mainView.txtArea.text ?? "",
                     "block": self.mainView.txtBlock.text ?? "",
                     "street": self.mainView.txtStreet.text ?? "",
                     "avenue": self.mainView.txtAvenue.text ?? "",
                     "floor": self.mainView.txtFloor.text ?? "",
                     "flat": self.mainView.txtFlat.text ?? "",
                     "house_type": self.mainView.btnHouse.isSelected ? "0" : "1",
                     "description": self.mainView.txtVwDirection.text ?? "",
                     "address_id": self.mainVM.dictAddressData?.id ?? 0] as [String:Any]
        
        print("Param:- \(param)")
        self.mainVM.addAddressApi(param: param) { data in
            self.mainVM.handlorAddUpdateAddress()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}


//MARK: - TextView delegate
extension AddAddressVC: UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        self.mainView.lblTxtVwEnterHere.isHidden = self.mainView.txtVwDirection.text == "" ? false : true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}
