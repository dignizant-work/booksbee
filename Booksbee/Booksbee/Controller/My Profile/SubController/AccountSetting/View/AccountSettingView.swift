//
//  AccountSetting.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class AccountSettingView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var tblAccount: UITableView!
    @IBOutlet weak var tblAccountHeightConstraint: NSLayoutConstraint!
    
    func setUpUI() {
        
        
        imgBG.image = UIImage(named: "ic_profile_bg")?.imageFlippedForRightToLeftLayoutDirection()
        [vwBG].forEach { (vw) in
            vw?.clipsToBounds = true
            vw?.layer.cornerRadius = 30
            vw?.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        
        registerXib()
    }
    
    func registerXib() {
        tblAccount.register(UINib(nibName: "MyProfileTableCell", bundle: nil), forCellReuseIdentifier: "MyProfileTableCell")
//        tblAccount.reloadData()
    }
    
}
