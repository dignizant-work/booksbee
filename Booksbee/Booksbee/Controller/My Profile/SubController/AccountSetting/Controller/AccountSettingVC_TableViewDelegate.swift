//
//  AccountSettingVC_TableViewDelegate.swift
//  Booksbee
//
//  Created by vishal on 28/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension AccountSettingVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.mainVM.arryAccountData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableCell", for: indexPath) as! MyProfileTableCell
        
        let dict = self.mainVM.arryAccountData[indexPath.row]
        
        cell.lblTitle.text = dict["title"].stringValue
        cell.imgIcon.image = UIImage(named: dict["img"].stringValue)?.imageFlippedForRightToLeftLayoutDirection()
        
        cell.btnNotification.tag = indexPath.row
        if cell.lblTitle.text == "Notification".localized {
            cell.btnNotification.isHidden = false
            cell.imgArrow.image = nil
            
            if getLanguage() == arabic{
                cell.btnNotification.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            if cell.btnNotification.isSelected {
                cell.btnNotification.setImage(UIImage(named: "ic_notification_on"), for: .normal)
            }
            else {
                cell.btnNotification.setImage(UIImage(named: "ic_notification_off"), for: .normal)
            }
            cell.btnNotification.addTarget(self, action: #selector(btnNotificationAction(_:)), for: .touchUpInside)
        }
        else {
            cell.btnNotification.isHidden = true
            cell.btnNotification.setImage(nil, for: .normal)
            cell.imgArrow.image = UIImage(named: "ic_profile_left-arrow")
        }
        
        cell.imgArrow.image = UIImage(named: "ic_profile_left-arrow")?.imageFlippedForRightToLeftLayoutDirection()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.mainVM.arryAccountData[indexPath.row]
        
        if dict["title"].stringValue == "Edit profile".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "My Books".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "MyBooksVC") as! MyBooksVC
            vc.mainVM.isControllerCheck = .myBook
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "Contact Us".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            vc.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "About Us".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
            vc.hidesBottomBarWhenPushed = true
            vc.mainVM.isControllerCheck = .aboutAs
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "Notification".localized {
            
        }
        else if dict["title"].stringValue == "My address".localized {
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressVC
            vc.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "My Order".localized {
            
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "MyOrderVc") as! MyOrderVc
            vc.hidesBottomBarWhenPushed = true
            vc.mainVM.isControllerCheck = .myOrder
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "My Bid List".localized {
            
            let vc = profileStoryboard.instantiateViewController(withIdentifier: "MyOrderVc") as! MyOrderVc
            vc.hidesBottomBarWhenPushed = true
            vc.mainVM.isControllerCheck = .mybidList
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if dict["title"].stringValue == "Share".localized {
            
            textShare(txt: "")
//            let vc = profileStoryboard.instantiateViewController(withIdentifier: "WriteReviewVC") as! WriteReviewVC
//            vc.hidesBottomBarWhenPushed = true
//
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @objc func btnNotificationAction(_ sender: UIButton) {
        
        let dict = self.mainVM.arryAccountData[sender.tag]
        if dict["title"].stringValue == "Notification".localized {
            let index = IndexPath(row: sender.tag, section: 0)
            let cell = self.mainView.tblAccount.cellForRow(at: index) as! MyProfileTableCell
            if cell.btnNotification.isSelected {
                cell.btnNotification.isSelected = false
            }
            else {
                cell.btnNotification.isSelected = true
            }
            self.mainView.tblAccount.reloadData()
        }
    }
    
    func btnShareAction() {
//        // Setting description
//            let firstActivityItem = "Description you want.."
//
//            // Setting url
//            let secondActivityItem : NSURL = NSURL(string: "http://your-url.com/")!
//
//            // If you want to use an image
//            let image : UIImage = UIImage(named: "your-image-name")!
//            let activityViewController : UIActivityViewController = UIActivityViewController(
//                activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
//
//            // This lines is for the popover you need to show in iPad
//            activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
//
//            // This line remove the arrow of the popover to show in iPad
//            activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
//            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
//
//            // Pre-configuring activity items
//            activityViewController.activityItemsConfiguration = [
//            UIActivity.ActivityType.message
//            ] as? UIActivityItemsConfigurationReading
//
//            // Anything you want to exclude
//            activityViewController.excludedActivityTypes = [
//                UIActivity.ActivityType.postToWeibo,
//                UIActivity.ActivityType.print,
//                UIActivity.ActivityType.assignToContact,
//                UIActivity.ActivityType.saveToCameraRoll,
//                UIActivity.ActivityType.addToReadingList,
//                UIActivity.ActivityType.postToFlickr,
//                UIActivity.ActivityType.postToVimeo,
//                UIActivity.ActivityType.postToTencentWeibo,
//                UIActivity.ActivityType.postToFacebook
//            ]
//
//            activityViewController.isModalInPresentation = true
//            self.present(activityViewController, animated: true, completion: nil)
    }
    
}
