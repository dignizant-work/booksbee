//
//  AccountSettingVC.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class AccountSettingVC: UIViewController {

    //MARK: Variables
    lazy var mainView: AccountSettingView = {
        return self.view as! AccountSettingView
    }()
    
    lazy var mainVM: AccountSettingVM = {
        return AccountSettingVM(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        setupNavigationbarBackButton(titleText: "Account Setting".localized)
        self.mainView.tblAccountHeightConstraint.constant = CGFloat(self.mainVM.arryAccountData.count*68)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
}
