//
//  AccountSettingVM.swift
//  Booksbee
//
//  Created by vishal on 26/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class AccountSettingVM {
    
    //MARK:- Variables
    var theController = AccountSettingVC()
    var arryAccountData: [JSON] = []
    
    init(theController: AccountSettingVC) {
        self.theController = theController
        self.arryAccountData = setUpAccoundData()
    }
    
    func setUpAccoundData() -> [JSON] {
        
        var arr: [JSON] = []
        var dict = JSON()
        dict["img"].stringValue = "ic_accounr_setting"
        dict["title"].stringValue = "Edit profile".localized
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_confirm_order"
        dict["title"].stringValue = "My Bid List".localized
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_my_book"
        dict["title"].stringValue = "My Books".localized
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_contact_us"
        dict["title"].stringValue = "Contact Us".localized
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_about_us"
        dict["title"].stringValue = "About Us".localized
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_notification"
        dict["title"].stringValue = "Notification".localized
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_my_address"
        dict["title"].stringValue = "My address".localized
        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_my_order"
        dict["title"].stringValue = "My Order".localized
        arr.append(dict)
        
//        dict = JSON()
//        dict["img"].stringValue = "ic_confirm_order"
//        dict["title"].stringValue = "Confirm order".localized
//        arr.append(dict)
        
        dict = JSON()
        dict["img"].stringValue = "ic_share"
        dict["title"].stringValue = "Share".localized
        arr.append(dict)
        
        return arr
    }
    
}
