//
//  MyOrderVc_UITableViewDelegate.swift
//  Booksbee
//
//  Created by YASH on 12/7/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension MyOrderVc:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainVM.dictBookData?.bookDataModel?.myOrdeData.count ?? 0 == 0 || self.mainVM.dictBookData?.bookDataModel?.myOrdeData == nil{
            setTableViewEmptyMessage(self.mainVM.dictBookData?.message ?? "", tableView: tableView)
            return 0
        }
    
        tableView.backgroundView = nil
        return self.mainVM.dictBookData?.bookDataModel?.bookData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.mainVM.isControllerCheck == .myOrder {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderTableCell", for: indexPath) as! MyOrderTableCell
            
            if let dict = self.mainVM.dictBookData?.bookDataModel?.myOrdeData[indexPath.row] {
                
                cell.imgOrder.sd_setImage(with: dict.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, context: nil)
                cell.lblOrderIdValue.text = "#\(dict.orderId)"
                cell.lblTotalPrizeValue.text = dict.totalPrice
                cell.lblOrderStatusValue.text = dict.status
                
                let orderdate = stringTodate(OrignalFormatter: "yyyy-MM-dd", YouWantFormatter: "MMM d, yyyy", strDate: dict.orderDate)
                cell.lblOrderDateValue.text = orderdate
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBookTableCell", for: indexPath) as! MyBookTableCell
        
        if let dict = self.mainVM.dictBookData?.bookDataModel?.myOrdeData[indexPath.row] {
            
            cell.imgCover.sd_setImage(with: dict.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, context: nil)
            
            cell.lblBookTitle.text = dict.bookName
            cell.lblBookAuthor.text = dict.author
            cell.vwStarRating.value = dict.rates
            cell.lblAvgRate.text = "\(dict.rateCount)"
            cell.lblPrize.text = "Bid Prize :".localized + " \(dict.bidPrice)"
            cell.lblSubPrize.isHidden = true
            
            cell.btnEdit.isHidden = true
            if dict.isWinner == 1 {
                cell.btnEdit.isHidden = false
                cell.btnEdit.isUserInteractionEnabled = false
                cell.btnEdit.setTitle("Winner", for: .normal)
            }
        }
        
        cell.vwBottomLine.isHidden = true
        cell.vwShowAllBid.isHidden = true
        
        tableView.backgroundColor = .clear

        return cell
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let dict = self.mainVM.dictBookData?.bookDataModel?.myOrdeData[indexPath.row] {
            
            if self.mainVM.isControllerCheck == .myOrder {
                let vc = profileStoryboard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
                vc.mainVM.orderId = dict.orderId
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if self.mainVM.isControllerCheck == .mybidList {
                let vc = profileStoryboard.instantiateViewController(withIdentifier: "BidDetailsVC") as! BidDetailsVC
                vc.mainVM.bookTitle = dict.bookName
                vc.mainVM.bookId = dict.bookId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1) {
            if tableView.visibleCells.contains(cell){
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && self.mainVM.dictBookData?.bookDataModel?.lastPage != self.mainVM.nxtPage {
                    self.mainVM.nxtPage += 1
                    self.showTableFooterIndicater(tableview: tableView)
                    
                    if self.mainVM.isControllerCheck == .myOrder {
                        self.mainVM.getMyOrderListAPI(page: self.mainVM.nxtPage) { data in
                            tableView.tableFooterView?.isHidden = true
                            tableView.reloadData()
                        }
                    }
                    else if self.mainVM.isControllerCheck == .mybidList {
                        self.mainVM.getMyBidListAPI(page: self.mainVM.nxtPage) { data in
                            tableView.tableFooterView?.isHidden = true
                            tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
}
