//
//  MyOrderVc.swift
//  Booksbee
//
//  Created by YASH on 12/7/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class MyOrderVc: UIViewController {
    
    //MARK: Variables
    lazy var mainView: MyOrderView = {
        return self.view as! MyOrderView
    }()
    
    lazy var mainVM: MyOrderVM = {
        return MyOrderVM(theController: self)
    }()
    @IBOutlet weak var btnDeliveryStatus: UIButton!
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUI()
        
        if self.mainVM.isControllerCheck == .myOrder{
            setupNavigationbarBackButton(titleText: "My Orders".localized)
            self.mainView.tblOrder.tableFooterView = UIView()
            self.mainView.vwBottom.isHidden = true
            self.mainView.heightVwBottom.constant = 0

            self.mainVM.getMyOrderListAPI(page: 1) { data in
                self.mainView.tblOrder.reloadData()
            }
        }
        else if self.mainVM.isControllerCheck == .mybidList {
            setupNavigationbarBackButton(titleText: "My Bid List".localized)
            self.mainView.tblOrder.tableFooterView = UIView()
            self.mainView.vwBottom.isHidden = true
            self.mainView.heightVwBottom.constant = 0
            
            self.mainVM.getMyBidListAPI(page: 1) { data in
                self.mainView.tblOrder.reloadData()
            }
        }
        
        setUpPulltorefresh()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mainView.tblOrder.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        mainView.tblOrder.removeObserver(self, forKeyPath: "contentSize")
    }

}

//MARK: SetUp
extension MyOrderVc {
    
    func setUpPulltorefresh() {
        
        self.mainVM.activityIndicator = UIRefreshControl()
        self.mainVM.activityIndicator.tintColor = .appGoldenColor
        self.mainVM.activityIndicator.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblOrder.addSubview(self.mainVM.activityIndicator)
    }
    
    @objc func pullToRefresh() {
        
        self.mainVM.nxtPage = 1
        
        if self.mainVM.isControllerCheck == .myOrder {
            
            self.mainVM.getMyOrderListAPI(page: self.mainVM.nxtPage) { data in
                self.mainView.tblOrder.reloadData()
            }
        }
        else if self.mainVM.isControllerCheck == .mybidList {
            
            self.mainVM.getMyBidListAPI(page: 1) { data in
                self.mainView.tblOrder.reloadData()
            }
        }
    }
    
    //MARK:- Overide Method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.mainView.tblOrderHeightConstraint.constant = self.mainView.tblOrder.contentSize.height
            if self.mainView.tblOrder.contentSize.height == 0.0 || self.mainView.tblOrder.contentSize.height < screenHeight {
                self.mainView.tblOrderHeightConstraint.constant = screenHeight-self.mainView.tblOrder.frame.origin.y
            }
        }
    }
}

//MARK: Buton Action
extension MyOrderVc {
    
    @IBAction func btnDeliveryStatusAction(_ sender: Any) {
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
