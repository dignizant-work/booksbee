//
//  MyOrderVM.swift
//  Booksbee
//
//  Created by YASH on 12/7/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import SwiftyJSON

class MyOrderVM {
    
    //MARK: Variables
    var theController = MyOrderVc()
    var isControllerCheck = enumOrderStatusVCCheck.myOrder

    var dictBookData = BookModel(JSON: JSON().dictionaryValue)
    var activityIndicator = UIRefreshControl()
    var nxtPage = 1

    init(theController: MyOrderVc) {
        self.theController = theController
    }
        
    func getMyOrderListAPI(page: Int, completionHandlor:@escaping(JSON)->Void) {
        
        var url = ""
        if page == 1  {
            url = myOrderURL
            showIndicator()
        }
        else {
            url = myBookListURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Dict:-\(dict)")
                
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 && page == 1 {
                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                }
                else {
                    if page == 1 {
                        self.dictBookData = nil
                        self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = MyOrdeData(JSON: bookData.dictionaryObject!)
                            self.dictBookData?.bookDataModel?.myOrdeData.append(data!)
                        }
                    }
                }
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func getMyBidListAPI(page: Int, completionHandlor:@escaping(JSON)->Void) {
        
        var url = ""
        if page == 1  {
            url = myBidlistURL
            showIndicator()
        }
        else {
            url = myBidlistURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Dict:-\(dict)")
                
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 && page == 1 {
                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                }
                else {
                    if page == 1 {
                        self.dictBookData = nil
                        self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = MyOrdeData(JSON: bookData.dictionaryObject!)
                            self.dictBookData?.bookDataModel?.myOrdeData.append(data!)
                        }
                    }
                }
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
}
