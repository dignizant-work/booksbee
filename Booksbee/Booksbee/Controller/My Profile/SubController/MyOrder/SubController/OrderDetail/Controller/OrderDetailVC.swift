//
//  OrderDetailVC.swift
//  Booksbee
//
//  Created by vishal on 16/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class OrderDetailVC: UIViewController {

    //MARK: Variables
    lazy var mainView: OrderDetailView = {
        return self.view as! OrderDetailView
    }()
    
    lazy var mainVM: OrderDetailVM = {
        return OrderDetailVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUI()
        self.setupNavigationbarBackButton(titleText: "Order Details".localized)
        self.mainVM.getOrderDetailsAPI(orderId: self.mainVM.orderId) { data in
            self.mainView.setUpData(data: self.mainVM.dictOrderData!)
            self.mainView.collectionOrderDetails.reloadData()
        }
    }
    
    @IBAction func btnBackToHomeAction(_ sender: Any) {
        appdelgate.tabBarVC.selectedIndex = 0
    }
    
}
