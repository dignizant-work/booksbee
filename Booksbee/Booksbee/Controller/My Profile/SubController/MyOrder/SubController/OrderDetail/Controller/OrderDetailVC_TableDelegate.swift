//
//  OrderDetailVC_TableDelegate.swift
//  Booksbee
//
//  Created by vishal on 16/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension OrderDetailVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.mainVM.arrayTrack.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackOrderTableCell", for: indexPath) as! TrackOrderTableCell
        
        if indexPath.row == 0{
            cell.imgUpperDashLine.isHidden = true
            cell.imgLowerDashLine.isHidden = false
        }else if indexPath.row == self.mainVM.arrayTrack.count - 1{
            cell.imgUpperDashLine.isHidden = false
            cell.imgLowerDashLine.isHidden = true
        }else{
            cell.imgUpperDashLine.isHidden = false
            cell.imgLowerDashLine.isHidden = false
        }
        
        if self.mainVM.arrayTrack[indexPath.row]["selected"].stringValue == "1"{
            cell.imgUpperDashLine.image = UIImage(named: "ic_order_track_selected")
            cell.imgLowerDashLine.image = UIImage(named: "ic_order_track_selected")
            cell.imgTrack.image = UIImage(named: "ic_select_check")
        }else{
            cell.imgUpperDashLine.image = UIImage(named: "ic_order_track_unselected")
            cell.imgLowerDashLine.image = UIImage(named: "ic_order_track_unselected")
            cell.imgTrack.image = UIImage(named: "ic_unselect_check")
        }
        
        return cell
    }
}
