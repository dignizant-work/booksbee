//
//  OrderDetailVC_CollectionDelegate.swift
//  Booksbee
//
//  Created by vishal on 16/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension OrderDetailVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.mainVM.dictOrderData?.bookDataModel?.orderBookData.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderDetailCollectionCell", for: indexPath) as! OrderDetailCollectionCell
        
        if let dict = self.mainVM.dictOrderData?.bookDataModel?.orderBookData[indexPath.row] {
            cell.setUpData(data: dict)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/1.2, height: collectionView.bounds.height)
    }
}
