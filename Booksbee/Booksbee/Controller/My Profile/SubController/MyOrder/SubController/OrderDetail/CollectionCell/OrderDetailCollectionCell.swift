//
//  OrderDetailCollectionCell.swift
//  Booksbee
//
//  Created by vishal on 16/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SDWebImage

class OrderDetailCollectionCell: UICollectionViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var imgOrder: CustomImageView!
    @IBOutlet weak var lblBookTitle: UILabel!
    @IBOutlet weak var lblBookAuthor: UILabel!
    @IBOutlet weak var vwStarRating: HCSStarRatingView!
    @IBOutlet weak var lblAvgRate: UILabel!
    @IBOutlet weak var lblPrize: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()

        if getLanguage() == arabic{
            lblDate.textAlignment = .left
        }

    }
    
    override func layoutSubviews() {
        imgOrder.layer.cornerRadius = 5
        imgOrder.layer.masksToBounds = true

    }
    
    func setUpData(data: Bookdata) {
        
        self.imgOrder.sd_setImage(with: data.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, context: nil)
        self.lblPrize.text = data.bookPrice
        self.lblDate.text = ""
        self.lblBookTitle.text = data.bookName
        self.lblBookAuthor.text = data.author
        self.vwStarRating.value = data.rates
        self.vwStarRating.allowsHalfStars = true
        self.lblAvgRate.text = String(data.rateCount) + " " + "Reviews".localized
        
    }
    
    func setUpUI() {
        
        [lblBookTitle].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 20, fontname: .medium)
        }
        
        [lblBookAuthor].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [lblPrize].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [lblAvgRate].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        imgOrder.layer.cornerRadius = 5
        imgOrder.layer.masksToBounds = true

    }

}
