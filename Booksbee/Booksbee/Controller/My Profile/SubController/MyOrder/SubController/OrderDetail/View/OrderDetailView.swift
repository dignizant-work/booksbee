//
//  OrderDetailView.swift
//  Booksbee
//
//  Created by vishal on 16/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import HCSStarRatingView

class OrderDetailView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var lblTitleOrderDetails: UILabel!
    @IBOutlet weak var collectionOrderDetails: UICollectionView!
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblSubTotalValue: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    
    @IBOutlet weak var lblSellerTitle: UILabel!
    @IBOutlet weak var imgSeller: UIImageView!
    @IBOutlet weak var lblSellerName: UILabel!
    @IBOutlet weak var vwStarRating: HCSStarRatingView!
    
    @IBOutlet weak var lblTrackOrder: UILabel!
    @IBOutlet weak var lblTrackOrderValue: UILabel!
    @IBOutlet weak var tblTrackOrder: UITableView!
    
    @IBOutlet weak var lblAddressBoldTitle: UILabel!
    @IBOutlet weak var lblAddressName: UILabel!
    @IBOutlet weak var lblAddressNameValue: UILabel!
    @IBOutlet weak var lblGovernorate: UILabel!
    @IBOutlet weak var lblGovernorateValue: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressValue: UILabel!
    
    @IBOutlet weak var btnBackToHome: CustomButton!
    
    
    
    //MARK: SetUp
    func setUpUI() {

        imgSeller.layer.cornerRadius = imgSeller.frame.size.height/2
        imgSeller.layer.masksToBounds = true
        
        [lblTitleOrderDetails,lblSellerTitle,lblSellerName,lblAddressBoldTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 20, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        [lblAddress, lblGovernorate, lblAddressName].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        
        [lblAddressValue, lblGovernorateValue, lblAddressNameValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .regular)
            lbl?.textColor = .appTitleTextColor
        }
        
        [lblSubTotal,lblSubTotalValue,lblDiscount,lblDiscountValue,lblTrackOrderValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .regular)
            lbl?.textColor = .appDetailTextColor
        }
        
        [lblSubTotalValue,lblDiscountValue,lblTotalValue].forEach { (lbl) in
            if getLanguage() == arabic{
                lbl?.textAlignment = .left
            }
            
        }
        
        [lblTotal,lblTotalValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        
        [lblTrackOrder].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        
        [lblTrackOrderValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .appDetailTextColor
        }
        
        [btnBackToHome].forEach { (btn) in
            btn?.setTitleColor(.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .medium)
        }
                
        lblTitleOrderDetails.text = "Order Details".localized
        lblSubTotal.text = "Subtotal".localized
        lblDiscount.text = "Discount".localized
        lblTotal.text = "Total".localized
        lblSellerTitle.text = "Seller".localized
        lblTrackOrder.text = "Track Order".localized
        lblAddressBoldTitle.text = "Address".localized
        lblAddressName.text = "Address Name : ".localized
        lblGovernorate.text = "Govarnorate : ".localized
        lblAddress.text = "Address :".localized
        btnBackToHome.setTitle("Back to Home".localized, for: .normal)
        
        registerXib()
    }
    
    func setUpData(data: BookModel) {
        
        self.lblSubTotal.text = "Order Date".localized
        
        let orderdate = stringTodate(OrignalFormatter: "yyyy-MM-dd", YouWantFormatter: "MMM d, yyyy", strDate: data.bookDataModel?.orderDate ?? "")
        self.lblSubTotalValue.text = orderdate
        
        self.lblTrackOrder.text = "Order Status".localized
        self.lblTrackOrderValue.text = data.bookDataModel?.status.capitalized
        self.lblTotalValue.text = data.bookDataModel?.totalPrice
        
        self.lblAddressNameValue.text = data.bookDataModel?.address?.addressName
        self.lblGovernorateValue.text = data.bookDataModel?.address?.street
        
        if let dict = data.bookDataModel?.address {
            if dict.floor.isEmpty || dict.floor == "" {
                lblAddressValue.text = "\(dict.block), \(dict.avenue), \(dict.street), \(dict.area)"
            }
            else {
                lblAddressValue.text = "\(dict.block) - \(dict.floor), \(dict.avenue), \(dict.street), \(dict.area)"
            }
        }
    }
    
    func registerXib() {
        
        self.collectionOrderDetails.register(UINib(nibName: "OrderDetailCollectionCell", bundle: nil), forCellWithReuseIdentifier: "OrderDetailCollectionCell")
        
        self.tblTrackOrder.register(UINib(nibName: "TrackOrderTableCell", bundle: nil), forCellReuseIdentifier: "TrackOrderTableCell")
    }
    
}
