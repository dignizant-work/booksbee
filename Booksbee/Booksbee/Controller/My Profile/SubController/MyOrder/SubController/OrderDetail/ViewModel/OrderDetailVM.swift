//
//  OrderDetailVM.swift
//  Booksbee
//
//  Created by vishal on 16/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class OrderDetailVM {
    
    //MARK: variables
    var theController = OrderDetailVC()
    var orderId = 0
    var dictOrderData: BookModel? = nil
    
    init(theController: OrderDetailVC) {
        self.theController = theController
        setupArray()
    }
    
    var arrayTrack = [JSON]()
    
    func setupArray(){
        
        var dict = JSON()
        dict["selected"] = "1"
        self.arrayTrack.append(dict)
        
        dict = JSON()
        dict["selected"] = "1"
        self.arrayTrack.append(dict)
        
        dict = JSON()
        dict["selected"] = "0"
        self.arrayTrack.append(dict)
        
        dict = JSON()
        dict["selected"] = "0"
        self.arrayTrack.append(dict)
        
    }
    
    func getOrderDetailsAPI(orderId: Int, completionHandlor:@escaping(JSON)->Void) {
        
        let url = orderDetailsURL
        
        let param = ["order_id": orderId]
        
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
                print("Dict:-\(dict)")
                self.dictOrderData = BookModel(JSON: dict.dictionaryObject!)
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    
}
