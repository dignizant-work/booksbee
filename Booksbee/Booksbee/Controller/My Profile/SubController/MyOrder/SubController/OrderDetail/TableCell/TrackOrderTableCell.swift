//
//  TrackOrderTableCell.swift
//  Booksbee
//
//  Created by vishal on 16/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class TrackOrderTableCell: UITableViewCell {

    //MARK:Outlets
    @IBOutlet weak var imgTrack: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var imgUpperDashLine: UIImageView!
    @IBOutlet weak var imgLowerDashLine: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
        
        [lblValue].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
