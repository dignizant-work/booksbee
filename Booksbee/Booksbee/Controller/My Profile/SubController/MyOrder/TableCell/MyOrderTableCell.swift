//
//  MyOrderTableCell.swift
//  Booksbee
//
//  Created by YASH on 12/7/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class MyOrderTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgOrder: CustomImageView!
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderIdValue: UILabel!
    
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderDateValue: UILabel!
    
    @IBOutlet weak var lblTotalPrize: UILabel!
    @IBOutlet weak var lblTotalPrizeValue: UILabel!
    
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblOrderStatusValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()

    }
    
    override func layoutSubviews() {
        imgOrder.layer.cornerRadius = 5
        imgOrder.layer.masksToBounds = true

    }
    
    func setUpUI() {
        
        [lblOrderStatus, lblOrderId, lblOrderDate, lblTotalPrize].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        lblOrderStatus.text = "Order Status".localized
        lblOrderId.text = "Order ID".localized
        lblOrderDate.text = "Order Date".localized
        lblTotalPrize.text = "Total Prize".localized
        
        [lblOrderStatusValue, lblOrderIdValue, lblOrderDateValue, lblTotalPrizeValue].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        imgOrder.layer.cornerRadius = 5
        imgOrder.layer.masksToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
