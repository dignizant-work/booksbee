//
//  MyOrderView.swift
//  Booksbee
//
//  Created by YASH on 12/7/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class MyOrderView: UIView {

    //MARK: Outlets
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var tblOrder: UITableView!
//    @IBOutlet weak var btnAddaddrress: CustomButton!
    @IBOutlet weak var tblOrderHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var tblFooterView: UIView!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var heightVwBottom: NSLayoutConstraint!
    
    func setUpUI() {
        registerXib()
        
        [vwBG].forEach { (vw) in
            vw?.clipsToBounds = true
            vw?.layer.cornerRadius = 30
            vw?.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        
        [lblDiscount,lblDiscountValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .regular)
            lbl?.textColor = .lightGray
            
        }
        
        [lblTotal,lblTotalValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .regular)
            lbl?.textColor = .black
        }
        
        if getLanguage() == arabic{
            [lblTotalValue,lblDiscountValue].forEach { (lbl) in
                lbl?.textAlignment = .left
            }
        }
        
        lblDiscount.text = "Discount".localized
        lblTotal.text = "Total".localized

    }
    
    func registerXib() {
        
        tblOrder.register(UINib(nibName: "MyOrderTableCell", bundle: nil), forCellReuseIdentifier: "MyOrderTableCell")
        tblOrder.register(UINib(nibName: "MyBookTableCell", bundle: nil), forCellReuseIdentifier: "MyBookTableCell")
    }
    
}
