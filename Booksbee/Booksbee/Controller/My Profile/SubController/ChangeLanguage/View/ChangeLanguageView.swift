//
//  ChangeLanguageView.swift
//  Booksbee
//
//  Created by vishal on 15/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class ChangeLanguageView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var tblChangeLanguage: UITableView!
    @IBOutlet weak var btnChangeLanguage: CustomButton!
    
    
    func setUpUI() {
        
        [btnChangeLanguage].forEach { (btn) in
            btn?.backgroundColor = .appTitleTextColor
            btn?.setTitleColor(.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .regular)
            btn?.setTitle("Change language".localized, for: .normal)
        }
        registerXIB()
    }
    
    func registerXIB() {
        self.tblChangeLanguage.isScrollEnabled = false
        self.tblChangeLanguage.register(UINib(nibName: "ChangeLanguageTableCell", bundle: nil), forCellReuseIdentifier: "ChangeLanguageTableCell")
    }
    
}
