//
//  ChangeLanguageVM.swift
//  Booksbee
//
//  Created by vishal on 15/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class ChangeLanguageVM {
    
    //MARK: Variables
    var theController = ChangeLanguageVC()
    var arrLanguage: [JSON] = []
    
    init(theController: ChangeLanguageVC) {
        self.theController = theController
        setLanguageArray()
    }
    
    func setLanguageArray() {
        
        var dict = JSON()
        dict["title"].stringValue = "English"
        dict["language"].stringValue = english
        dict["isSelected"].string = getLanguage() == english ? "1" : "0"
        self.arrLanguage.append(dict)
        
        dict = JSON()
        dict["title"].stringValue = "Arabic"
        dict["language"].stringValue = arabic
        dict["isSelected"].string = getLanguage() == arabic ? "1" : "0"
        self.arrLanguage.append(dict)
    }
    
}
