//
//  ChangeLanguageTableCell.swift
//  Booksbee
//
//  Created by vishal on 15/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class ChangeLanguageTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSelected: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
