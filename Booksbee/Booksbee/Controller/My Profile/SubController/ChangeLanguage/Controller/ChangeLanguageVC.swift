//
//  ChangeLanguageVC.swift
//  Booksbee
//
//  Created by vishal on 15/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChangeLanguageVC: UIViewController {

    //MARK: Variables
    lazy var mainView: ChangeLanguageView = {
        return self.view as! ChangeLanguageView
    }()
    
    lazy var mainVM: ChangeLanguageVM = {
        return ChangeLanguageVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUI()
        setupNavigationbarBackButton(titleText: "Change language".localized)
    }
    
}

//MARK: Button Action
extension ChangeLanguageVC {
    
    @IBAction func btnChangeLanguageAction(_ sender: Any) {
        
        for i in 0..<self.mainVM.arrLanguage.count {
            let dict = self.mainVM.arrLanguage[i]
            if dict["isSelected"].stringValue == "1" {
                if dict["language"].stringValue != getLanguage() {
                    setLanguage(lng: dict["language"].stringValue)
                    setSwipeDirection()
                    
                    filterDict = JSON()
                    
                    DispatchQueue.main.async {
                        setUpTabbarRoot()
                    }
                }
            }
        }
    }
}

//MARK: Table delegate
extension ChangeLanguageVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainVM.arrLanguage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeLanguageTableCell", for: indexPath) as! ChangeLanguageTableCell
        
        let dict = self.mainVM.arrLanguage[indexPath.row]
        
        cell.lblTitle.text = dict["title"].stringValue
        cell.btnSelected.isSelected = false
        
        if dict["isSelected"].stringValue == "1" {
            cell.btnSelected.isSelected = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<self.mainVM.arrLanguage.count {
            var dict = self.mainVM.arrLanguage[i]
            dict["isSelected"].stringValue = "0"
            self.mainVM.arrLanguage[i] = dict
        }
        self.mainVM.arrLanguage[indexPath.row]["isSelected"].stringValue = "1"
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
