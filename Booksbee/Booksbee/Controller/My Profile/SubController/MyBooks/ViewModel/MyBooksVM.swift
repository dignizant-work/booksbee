//
//  MyBooksVM.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MyBooksVM {
    
    //MARK: Variabels
    var theDelegate = MyBooksVC()
    var isControllerCheck = enumBookOrCartVCCheck.myBook

    var dictBookData = BookModel(JSON: JSON().dictionaryValue)
    var activityIndicator = UIRefreshControl()
    var nxtPage = 1
    
    init(theDelegate: MyBooksVC) {
        self.theDelegate = theDelegate
//        self.arrBookData = setUpBookData()
    }
}

//MARK: - API calling

extension MyBooksVM{
    

    func getMyBookListApi(page: Int, completionHandlor:@escaping(JSON)->Void) {
                                                     
        var url = ""
        if page == 1  {
            url = myBookListURL
            showIndicator()
        }
        else {
            url = myBookListURL+"?page=\(page)"
        }
        
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            self.activityIndicator.endRefreshing()
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                let innerDataArray = dict["data"]["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
//                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    if page == 1 {
                        self.dictBookData = nil
                        self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                        
                    }
                    else {
                        for bookData in innerDataArray {
                            let data = Bookdata(JSON: bookData.dictionaryObject!)
                            self.dictBookData?.bookDataModel?.bookData.append(data!)
                        }
                    }
                }
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func getCartListApi(completionHandlor:@escaping(JSON)->Void) {
        
        let url = cartListingURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                self.dictBookData = nil
                self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
        
    func removeItemInCartListApi(bookId:String, completionHandlor:@escaping(JSON)->Void) {
        
        let param: [String:Any] = ["book_id":bookId]
        
        let url = itemRemovefromCartURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
//                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func deleteMyBookApi(bookId:String, completionHandlor:@escaping(JSON)->Void) {
        
        let param: [String:Any] = ["book_id":bookId]
        
        let url = deleteMybookURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
