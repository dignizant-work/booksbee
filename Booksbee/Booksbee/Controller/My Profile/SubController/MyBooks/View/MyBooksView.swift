//
//  MyBooksView.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class MyBooksView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var tblMyBook: UITableView!
    @IBOutlet weak var tblFooterView: UIView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    
    @IBOutlet weak var btnCheckout: CustomButton!
    @IBOutlet weak var stackViewBottom: UIStackView!
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    
    
    func setUpUI() {
        registerXib()
        
        lblTotal.textColor = .black
        lblTotal.font = themeFont(size: 14, fontname: .medium)
        
        lblTotalValue.textColor = .lightGray
        lblTotalValue.font = themeFont(size: 12, fontname: .medium)
        
        lblTotal.text = "Total".localized + " :"

        btnCheckout.backgroundColor = .appTitleTextColor
        btnCheckout.setTitleColor(.white, for: .normal)
        btnCheckout.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        btnCheckout.setTitle("Checkout".localized, for: .normal)

    }
    
    func registerXib() {
        tblMyBook.register(UINib(nibName: "MyBookTableCell", bundle: nil), forCellReuseIdentifier: "MyBookTableCell")
//        tblMyBook.register(UINib(nibName: "MyBookTableCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "MyBookTableCell")
    }
}
