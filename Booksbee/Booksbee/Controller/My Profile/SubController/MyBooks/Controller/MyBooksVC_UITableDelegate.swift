//
//  MyBooksVC_UITableDelegate.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension MyBooksVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if self.mainVM.isControllerCheck == .myBook {
            
            if self.mainVM.dictBookData?.bookDataModel?.bookData.count ?? 0 == 0 || self.mainVM.dictBookData?.bookDataModel?.bookData == nil{
                setTableViewEmptyMessage(self.mainVM.dictBookData?.message ?? "", tableView: tableView)
                return 0
            }
        
            tableView.backgroundView = nil
            return self.mainVM.dictBookData?.bookDataModel?.bookData.count ?? 0
            
        }
        else if self.mainVM.isControllerCheck == .cart {
            
            if self.mainVM.dictBookData?.bookDataModel?.cartData.count ?? 0 == 0 || self.mainVM.dictBookData?.bookDataModel?.cartData == nil {
                
                setTableViewEmptyMessage(self.mainVM.dictBookData?.message ?? "", tableView: tableView)
                return 0
            }
        
            tableView.backgroundView = nil
            return self.mainVM.dictBookData?.bookDataModel?.cartData.count ?? 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBookTableCell", for: indexPath) as! MyBookTableCell
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEditAction(_:)), for: .touchUpInside)
        
        if self.mainVM.isControllerCheck == .myBook {
            
            if let dict = self.mainVM.dictBookData?.bookDataModel?.bookData[indexPath.row]{
                cell.setupMyBookCell(bookData: dict)
                
                if dict.saleType == 0 {
                    cell.vwShowAllBid.isHidden = false
                }
                else {
                    cell.vwShowAllBid.isHidden = true
                }
            }
            cell.vwBottomLine.isHidden = true
            cell.btnEdit.isHidden = false
            
            cell.btnShowAllBid.tag = indexPath.row
            cell.btnShowAllBid.addTarget(self, action: #selector(btnShowAllBidAction(_:)), for: .touchUpInside)
        }
        else if self.mainVM.isControllerCheck == .cart {
            
            if let dict = self.mainVM.dictBookData?.bookDataModel?.cartData[indexPath.row] {
                cell.setUpCartData(data: dict)
            }
            
            cell.vwBottomLine.isHidden = false
            cell.btnEdit.isHidden = true
        }
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let btnDelete = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
            
            self.showAlert("", "Are you sure you want to remove this item?".localized, "Delete".localized, "Cancel".localized) {
                if self.mainVM.isControllerCheck == .myBook {
                    
                    let book_Id = self.mainVM.dictBookData?.bookDataModel?.bookData[indexPath.row].id ?? 0
                    
                    self.mainVM.deleteMyBookApi(bookId: "\(book_Id)") { data in
                        
                        self.mainVM.dictBookData?.bookDataModel?.bookData.remove(at: indexPath.row)
                        
                        if self.mainVM.dictBookData?.bookDataModel?.bookData.count == 0 {
                            self.mainVM.getMyBookListApi(page: 1) { data in
                                tableView.reloadData()
                            }
                        }
                        tableView.reloadData()
                    }
                }
                else if self.mainVM.isControllerCheck == .cart {
                    
                    let book_Id = self.mainVM.dictBookData?.bookDataModel?.cartData[indexPath.row].id
                    
                    self.mainVM.dictBookData?.bookDataModel?.cartData.remove(at: indexPath.row)
                    
                    self.mainVM.removeItemInCartListApi(bookId: "\(book_Id ?? 0)") { data in
                        
                        if self.mainVM.dictBookData?.bookDataModel?.cartData.count == 0 {
                            self.mainVM.getCartListApi { data in
                                tableView.reloadData()
                                
                                self.mainView.stackViewBottom.isHidden = true
                                self.mainView.bottomHeightConstraint.constant = 0

                            }
                        }
                    }
                    
                }
                tableView.reloadData()
            }
            completion(true)
        }
        btnDelete.image = UIImage(named: "ic_delete")
        btnDelete.backgroundColor = .appBackgroundColor
        return UISwipeActionsConfiguration(actions: [btnDelete])
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1) {
            if tableView.visibleCells.contains(cell){
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && self.mainVM.dictBookData?.bookDataModel?.lastPage != self.mainVM.nxtPage {
                    
                    if self.mainVM.isControllerCheck == .myBook {
                        self.mainVM.nxtPage += 1
                        self.showTableFooterIndicater(tableview: tableView)
                        self.mainVM.getMyBookListApi(page: self.mainVM.nxtPage) { data in
                            tableView.tableFooterView?.isHidden = true
                            tableView.reloadData()
                        }
                    }
                    else if self.mainVM.isControllerCheck == .cart {
                        
                    }
                }
            }
        }
    }

    @objc func btnShowAllBidAction(_ sender: UIButton) {
        
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "BidValueVC") as! BidValueVC
        let book_Id = self.mainVM.dictBookData?.bookDataModel?.bookData[sender.tag].id ?? 0
        vc.mainVM.bookId = book_Id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnEditAction(_ sender: UIButton) {
        
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "AddBooksVC") as! AddBooksVC
        vc.mainVM.checkController = .edit
        vc.mainVM.handlerRefreshBooks = {[weak self] in
                self?.mainVM.getMyBookListApi(page: 1) { data in
                self?.mainView.tblMyBook.reloadData()
            }
        }
        if let dict = self.mainVM.dictBookData?.bookDataModel?.bookData[sender.tag]{
            vc.mainVM.dataBook = dict
            vc.mainVM.intBookId = dict.id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
