//
//  MyBooksVC.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class MyBooksVC: UIViewController {

    //MARK: Variables
    lazy var mainView: MyBooksView = {
        return self.view as! MyBooksView
    }()
    
    lazy var mainVM: MyBooksVM = {
        return MyBooksVM(theDelegate: self)
    }()
    
    //MARK:Controllerlife cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUI()
        
        if self.mainVM.isControllerCheck == .myBook{
            
            self.mainView.stackViewBottom.isHidden = true
            self.mainView.bottomHeightConstraint.constant = 0
            
            setupNavigationbarBackButton(titleText: "My Books".localized)
            btnRightNavigationItem()
            self.mainView.tblMyBook.tableFooterView = UIView()
            
            self.mainVM.getMyBookListApi(page: 1) { data in
                self.mainView.tblMyBook.reloadData()
            }
            self.setUpPulltorefresh()

        }
        else if self.mainVM.isControllerCheck == .cart{
            
            setupNavigationbarBackButton(titleText: "Cart".localized)
            self.mainView.tblMyBook.tableFooterView = self.mainView.tblFooterView
            self.mainVM.getCartListApi { data in
                
                if self.mainVM.dictBookData?.bookDataModel?.cartData.count ?? 0 == 0 || self.mainVM.dictBookData?.bookDataModel?.cartData == nil {
                    self.mainView.stackViewBottom.isHidden = true
                    self.mainView.bottomHeightConstraint.constant = 0
                }
                
                self.mainView.lblTotalValue.text = self.mainVM.dictBookData?.bookDataModel?.totalPrice
                self.mainView.tblMyBook.reloadData()
            }
        }
    }
    
    func btnRightNavigationItem() {
        let button = UIButton.init(type: .custom)
        button.addTarget(self, action: #selector(addBookButtonAction), for: .touchUpInside)
        
        let imageView = UIImageView(frame: CGRect(x: getLanguage() == arabic ? 1 : 62, y: 0, width: 30, height: 30))
        imageView.image = UIImage(named: "ic_plus")
        imageView.contentMode = .center
        
        let label = UILabel(frame: CGRect(x: getLanguage() == arabic ? 22 : 8, y: 0, width: 62, height: 30))
        label.text = "Add book".localized
        label.textColor = .white
        label.font = themeFont(size: 14, fontname: .regular)
        
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 93, height: 30))
        buttonView.backgroundColor = .appGoldenColor
        buttonView.layer.cornerRadius = 15
        button.frame = buttonView.frame
        
        buttonView.addSubview(button)
        buttonView.addSubview(label)
        buttonView.addSubview(imageView)
        
        let barButton = UIBarButtonItem.init(customView: buttonView)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func addBookButtonAction() {
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "AddBooksVC") as! AddBooksVC
        vc.mainVM.checkController = .add
        vc.mainVM.handlerRefreshBooks = {[weak self] in
            
                self?.mainVM.getMyBookListApi(page: 1) { data in
                self?.mainView.tblMyBook.reloadData()
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func btnCheckoutTapped(_ sender: UIButton) {
        
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
        vc.mainVM.dictBookData = self.mainVM.dictBookData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MyBooksVC{
    
    func setUpPulltorefresh() {
        
        self.mainVM.activityIndicator = UIRefreshControl()
        self.mainVM.activityIndicator.tintColor = .appGoldenColor
        self.mainVM.activityIndicator.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblMyBook.addSubview(self.mainVM.activityIndicator)
    }
    
    @objc func pullToRefresh() {
        
        self.mainVM.nxtPage = 1
        
        if self.mainVM.isControllerCheck == .myBook {
            self.mainVM.getMyBookListApi(page: self.mainVM.nxtPage) { data in
                self.mainView.tblMyBook.reloadData()
            }
        }
        else if self.mainVM.isControllerCheck == .cart {
            
        }
    }

    
}

