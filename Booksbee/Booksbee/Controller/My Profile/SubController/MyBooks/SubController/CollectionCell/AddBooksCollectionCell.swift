//
//  AddBooksCollectionCell.swift
//  Booksbee
//
//  Created by vishal on 14/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class AddBooksCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var btnAddBook: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        self.imgBook.layer.cornerRadius = 10
        self.imgBook.clipsToBounds = true
    }

}
