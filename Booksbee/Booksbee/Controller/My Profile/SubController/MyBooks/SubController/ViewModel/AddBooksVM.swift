//
//  AddBooksVM.swift
//  Booksbee
//
//  Created by vishal on 14/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import SwiftyJSON


class AddBooksVM {
    
    //MARK:Variables
    var theController = AddBooksVC()
    var arrImg: [UIImage] = []
    init(theController: AddBooksVC) {
        self.theController = theController
        self.getCategoryListApi()
    }
    
    
    var imageArray = NSMutableArray()
    var updatedImageArray = NSMutableArray()
    
    var checkController = enumAddOrEditBookVCCheck.add
    
    var isDateSelection = 0 //0 - For start Auction, 1- For End Auction
    
    var isStartAuctionDate = Date()
    var isEndAuctionDate = Date()
    
    var categoryDeropDown = DropDown()
    var arrBooksCategory: [JSON] = []
    var intCategoryID = 0
    var intBookId = 0
    var isAutenticationSelect = 0
    var dataBook : Bookdata?
    var handlerRefreshBooks: () -> Void = {}
}

//MARK: - API calling

extension AddBooksVM{
    

    func getCategoryListApi() {
        
        let  url = categoryListURL
        showIndicator()
        
        WebServices().MakeGetAPI(name: url, params: [:]) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:-\(dict)")
                let innerDataArray = dict["data"].arrayValue
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    self.arrBooksCategory = []
                    self.arrBooksCategory = innerDataArray
                    self.categoryDeropDown.dataSource =  self.arrBooksCategory.map({ (json) -> String in
                        return json["category"].stringValue
                    })
                    
                    if self.checkController == .edit{
                        
                        if self.dataBook != nil{
                            self.theController.fillUpData(dataBook: self.dataBook!)
                            
                            if let array = self.dataBook?.images{
                                    
                                for i in 0..<array.count{
                                    
                                    let  dict = NSMutableDictionary()
                                    dict["image_id"] = array[i].id
                                    dict["name"] = array[i].img
                                    dict["is_del"] = 0
                                    self.imageArray.add(dict)
                                }
                            }
                            
                            print("imageArray count:\(self.imageArray.count)")
                            print("imageArray:\(self.imageArray)")
                            
//                            self.updatedImageArray = self.imageArray
                            self.theController.mainView.collectionImageOfBooks.reloadData()
                        }
                    }
                    
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
        
    }
    
    func addBookAPI(param:[String:String], completionHandlor:@escaping(JSON)->Void) {
        
        let url = addBookURL
        
        showIndicator()
        
        WebServices().requestMultiPartFromData(name: url, params: param, imgName: "files", imagesMutableArray: self.imageArray, arrImage: self.arrImg) { (data, error) in
           
            hideIndicator()
            
            if (error == nil) {
                
                let dict = JSON(data!)
                print("dict:\(dict)")
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }

        }
        
    }

    
}

