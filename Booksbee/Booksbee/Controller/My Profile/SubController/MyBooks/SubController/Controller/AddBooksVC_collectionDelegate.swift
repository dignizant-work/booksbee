//
//  AddBooksVC_collectionDelegate.swift
//  Booksbee
//
//  Created by vishal on 14/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import YangMingShan
import SDWebImage
import TOCropViewController

extension AddBooksVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.mainVM.imageArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddBooksCollectionCell", for: indexPath) as! AddBooksCollectionCell
        
        if indexPath.row == self.mainVM.imageArray.count{
            cell.btnAddBook.isHidden = false
            cell.btnAddBook.addTarget(self, action: #selector(btnAddImageAction(_:)), for: .touchUpInside)
            cell.imgBook.image = UIImage(named: "ic_add_book_splash_holder")
            cell.btnDelete.isHidden = true
        }else{
            cell.btnAddBook.isHidden = true
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteImageAction(_:)), for: .touchUpInside)
            
            if let dict = self.mainVM.imageArray[indexPath.row] as? NSMutableDictionary{
                if dict["image_id"] as! Int == 0{
                    cell.imgBook.image = dict["name"] as? UIImage
                }
                else{
                    cell.imgBook.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    cell.imgBook.sd_setImage(with: (dict["name"] as? String ?? "").toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, completed: nil)
                }
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2.5, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

//        let totalCellWidth = 80 * collectionView.numberOfItems(inSection: 0)
//        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        if self.mainVM.imageArray.count == 0 {
            let totalCellWidth = collectionView.frame.size.width/2.5
            let totalSpacingWidth = 0

            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + CGFloat(totalSpacingWidth))) / 2
            let rightInset = leftInset

            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
        
    @objc func btnAddImageAction(_ sender: UIButton) {
//        selectImage()
        ImagePickerManager().pickImage(self) { (img) in
            self.openCropVC(Image: img)
        }
    }
    
    @objc func btnDeleteImageAction(_ sender: UIButton) {
        
        showAlert("", "Are you sure you want to delete this book image?".localized, "Yes".localized, "No".localized) {
            
            if let dict = self.mainVM.imageArray[sender.tag] as? NSMutableDictionary{

                if dict["image_id"] as! Int == 0{
                    self.mainVM.imageArray.removeObject(at: sender.tag)
//                    self.mainVM.updatedImageArray.removeObject(at: sender.tag)
                }
                else{
                    
                    dict["is_del"] = 1
                    dict["name"] = ""
                    
                    self.mainVM.updatedImageArray.add(dict)
                    self.mainVM.imageArray[sender.tag] = dict
                    
                    self.mainVM.imageArray.removeObject(at: sender.tag)
                    /*
                    var index = 0
                    
                    if self.mainVM.updatedImageArray.contains(where: { (dictData) -> Bool in
                        if (dictData as? NSMutableDictionary)?["image_id"] as! Int == dict["image_id"] as! Int{
                            
                            index = self.mainVM.updatedImageArray.index(of: dictData)
                            print("index:\(index)")
                            return true
                        }
                        return false
                    }){
                        if let Updateddict = self.mainVM.updatedImageArray[index] as? NSMutableDictionary{
                            Updateddict["is_del"] = 1
                            self.mainVM.updatedImageArray[index] = Updateddict
                            
                            print("imageArray:\(self.mainVM.imageArray.count)")
                            print("UpdateDict:\(self.mainVM.updatedImageArray)")
                            print("UpdateDict before count:\(self.mainVM.updatedImageArray.count)")
                            
                            self.mainVM.imageArray.removeObject(at: sender.tag)
                            
                            print("UpdateDict count:\(self.mainVM.updatedImageArray.count)")
                        }
                            
                    }else{
                        
                    }
                    
                    print("UpdateDict before count:\(self.mainVM.updatedImageArray.count)")
                     */
                }
            }
            
            self.mainView.collectionImageOfBooks.reloadData()
        }
        
    }

    
}

//MARK: Single image selection and crop
extension AddBooksVC : TOCropViewControllerDelegate {
    
    func openCropVC(Image:UIImage) {
        
        let cropVC = TOCropViewController(croppingStyle: .default, image: Image)
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.width)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.appGoldenColor, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.appGoldenColor, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        self.present(cropVC, animated: false, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        
//        self.mainView.imgUserProfile.image = image
        
        let  dict = NSMutableDictionary()
        dict["image_id"] = 0
        dict["name"] = image
        dict["image"] = image
        dict["is_del"] = 0
        self.mainVM.imageArray.add(dict)
        self.mainView.collectionImageOfBooks.reloadData()
        
//        self.mainVM.updatedImageArray.add(dict)
//        print("ImageArray:\(self.mainVM.imageArray)")
        
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: Select Image Multiple image selection
extension AddBooksVC : YMSPhotoPickerViewControllerDelegate {
    
    func selectImage() {

        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 10
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = .appDetailTextColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = .appTitleTextColor
        pickerViewController.theme.cameraVeilColor = .appTitleTextColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)

        self.present(alertController, animated: true, completion: nil)
    }

    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)

        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            for asset: PHAsset in photoAssets  {
                
                imageManager.requestImageData(for: asset, options: options) { (data, str, img, info) in
                    if let data = data {
                        
                        let  dict = NSMutableDictionary()
                        dict["image_id"] = 0
                        dict["name"] = UIImage(data: data)!
                        dict["image"] = UIImage(data: data)!
                        dict["is_del"] = 0
                        self.mainVM.imageArray.add(dict)
//                        self.mainVM.updatedImageArray.add(dict)
                        
                        print("ImageArray:\(self.mainVM.imageArray)")
                        
                    }
                }
                self.mainView.collectionImageOfBooks.reloadData()
            }
        }
    }
    
}


extension UIImage {
    func fixOrientation(imh: UIImage) -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }
}
