//
//  AddBooksVC.swift
//  Booksbee
//
//  Created by vishal on 14/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddBooksVC: UIViewController {

    //MARK: Variables
    lazy var mainView: AddBooksView = {
        return self.view as! AddBooksView
    }()
    
    lazy var mainVM: AddBooksVM = {
        return AddBooksVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        setUp()
        setUpData()
    }
    
    override func viewDidLayoutSubviews() {
        configureDropdown(dropdown: self.mainVM.categoryDeropDown, sender: self.mainView.txtCategory)
    }
        
}

//MARK:
extension AddBooksVC {
    
    func setUp() {
        
        if self.mainVM.checkController == .add{
            setupNavigationbarBackButton(titleText: "Add book".localized)
        }else{
            setupNavigationbarBackButton(titleText: "Edit book".localized)
        }
        
    }
    
    func setUpData() {
        
        self.mainVM.categoryDeropDown.selectionAction = { [weak self] (index, item) in
            self?.mainView.txtCategory.text = item
            self?.mainVM.intCategoryID = self?.mainVM.arrBooksCategory[index]["id"].intValue ?? 0
        }
        
    }
    
    func fillUpData(dataBook: Bookdata){
        
        self.mainView.txtBookName.text = dataBook.bookName
        self.mainVM.intCategoryID = dataBook.categoryId
        self.mainView.txtBookAuthorName.text = dataBook.author
        self.mainView.txtISBNNumber.text = dataBook.isbn
        self.mainView.txtPrize.text = dataBook.price
        self.mainView.txtvwIntroductions.text = dataBook.description
        self.mainView.lblEnterHere.isHidden = true
        
        self.mainVM.isAutenticationSelect = dataBook.saleType
        
        if dataBook.saleType == 0 {
            self.mainView.vwAuctionTime.isHidden = false
            self.mainView.btnDirectSale.isSelected = false
            self.mainView.btnAuction.isSelected = true
        }
        else {
            self.mainView.vwAuctionTime.isHidden = true
            self.mainView.btnDirectSale.isSelected = true
            self.mainView.btnAuction.isSelected = false
        }
        
        self.mainView.txtStartAuction.text = dataBook.startDateTime
        self.mainView.txtEndAuction.text = dataBook.endDateTime
        
        print("categoyId:\(dataBook.categoryId)")
        self.checkCategoryID(cateogryId: dataBook.categoryId)
        
    }
    
    func checkCategoryID(cateogryId:Int){
        
        var selectedCategpryJSON = JSON()
        
        if self.mainVM.arrBooksCategory.contains(where: { (json) -> Bool in
            
            if json["id"].intValue == cateogryId{
                selectedCategpryJSON = json
                return true
            }
            
            return false
        }){
            self.mainView.txtCategory.text = selectedCategpryJSON["category"].stringValue
        }else{
            
        }
        
    }

}

//MARK: Button Actions
extension AddBooksVC {
    
    @IBAction func btnAuctionsAction(_ sender: Any) {
        self.mainVM.isAutenticationSelect = 0
        self.mainView.vwAuctionTime.isHidden = false
        self.mainView.btnDirectSale.isSelected = false
        self.mainView.btnAuction.isSelected = true
    }
    
    @IBAction func btnDirectSaleActions(_ sender: Any) {
        self.mainVM.isAutenticationSelect = 1
        self.mainView.vwAuctionTime.isHidden = true
        self.mainView.btnDirectSale.isSelected = true
        self.mainView.btnAuction.isSelected = false
    }
    
    @IBAction func btnAddBookActions(_ sender: Any) {
        
        
        if self.mainView.checkValidation(){
            
            if self.mainVM.isAutenticationSelect == 0 {
                if self.mainView.txtStartAuction.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? false {
                    makeToast(strMessage: "Please first select start Aution time".localized)
                    
                }
                else if self.mainView.txtEndAuction.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? false {
                    makeToast(strMessage: "Please first select end Aution time".localized)
                }
                else {
                    self.addBookAPI()
                }
            }
            else {
                self.addBookAPI()
            }
        }
    }
}

//MARK: - LoginAPi

extension AddBooksVC{
    
    func addBookAPI(){
        
        var param : [String:String] = [:]
        
        param = ["book_name":self.mainView.txtBookName.text ?? "",
                 "price":self.mainView.txtPrize.text ?? "",
                 "description" : self.mainView.txtvwIntroductions.text ?? "",
                 "sale_type" : "\(self.mainVM.isAutenticationSelect)", //0 for bid, 1 for direct sale
                 "author" : self.mainView.txtBookAuthorName.text ?? "",
                 "isbn" : self.mainView.txtISBNNumber.text ?? "",
                 "category_id" : "\(self.mainVM.intCategoryID)",
                 "book_id" : self.mainVM.checkController == .add ? "0" : "\(self.mainVM.intBookId)"
        ]
        
        if self.mainVM.isAutenticationSelect == 0 {
            param["start_date_time"] = self.mainView.txtStartAuction.text ?? ""
            param["end_date_time"] = self.mainView.txtEndAuction.text ?? ""
            param["timezone"] = currentTimeZone
        }
        
        let UplodedImg = NSMutableArray()
        
        print("image array Count :\(self.mainVM.imageArray.count)")
//        print("Uopdated Count :\(self.mainVM.updatedImageArray.count)")
        
        let mergedArray = self.mainVM.imageArray.addingObjects(from: self.mainVM.updatedImageArray as [AnyObject])
        let result = NSMutableArray(array: mergedArray)
        
        for i in 0..<result.count
        {
            if let dict = result[i] as? NSMutableDictionary{
                
                let dictUploadImage = NSMutableDictionary()
                dictUploadImage["image_id"] = dict["image_id"]
                if dict["image_id"] as! Int == 0{
                    dictUploadImage["name"] = "files\(i)"
//                    dictUploadImage["image"] = dict["image"]
                    self.mainVM.arrImg.append(dict["image"] as! UIImage)
                }else{
                    dictUploadImage["name"] = ""
//                    dictUploadImage["image"] = ""
                    self.mainVM.arrImg.append(UIImage())
                }
                
                
                dictUploadImage["is_del"] = dict["is_del"]
                
                UplodedImg.add(dictUploadImage)
            }
        }
        
        print("UploadImage:\(UplodedImg)")
        
        let Uplodedfiles = notPrettyString(from: UplodedImg)!
        param["files"] = Uplodedfiles
        
        print("param:\(param)")
        
        self.mainVM.addBookAPI(param: param) { (data) in
            self.mainVM.handlerRefreshBooks()
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func notPrettyString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }

    
}
