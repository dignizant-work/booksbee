//
//  AddBooksVC_TextField.swift
//  Booksbee
//
//  Created by vishal on 08/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import Foundation
import UIKit

//MARK: TextView delegate method
extension AddBooksVC : UITextViewDelegate {
        
    func textViewDidChange(_ textView: UITextView) {
        self.mainView.lblEnterHere.isHidden = self.mainView.txtvwIntroductions.text == "" ? false : true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK: Rextfield delegate
extension AddBooksVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.mainView.txtPrize{
            guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
                return false
            }
            return true
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == mainView.txtCategory{
            self.view.endEditing(true)
            self.mainVM.categoryDeropDown.show()
            return false
        }
        else if textField == self.mainView.txtStartAuction {
            self.view.endEditing(true)
            
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.pickerDelegate = self
            vc.isSetMinimumDate = true
            vc.setMinimumDate = Date()
            self.mainVM.isDateSelection = 0
            vc.controlType = 2
            self.present(vc, animated: true, completion: nil)
            
            return false
        }
        else if textField == self.mainView.txtEndAuction {
            self.view.endEditing(true)
            
            if self.mainView.txtStartAuction.text?.trimmingCharacters(in: .whitespaces).isEmpty ?? false {
                makeToast(strMessage: "Please first select start aution time.".localized)
                return false
            }
            
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            self.mainVM.isDateSelection = 1
            vc.pickerDelegate = self
            vc.isSetMinimumDate = true
            vc.isSetMinimumDate = true
            vc.setMinimumDate = self.mainVM.isStartAuctionDate
            vc.controlType = 2
            self.present(vc, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
}

//MARK: Select Date delegate
extension AddBooksVC : DateTimePickerDelegate {
    
    func setDateandTime(dateValue: Date, type: Int) {
        
        var startAuction = ""
        if self.mainVM.isDateSelection == 0 {
            self.mainVM.isStartAuctionDate = dateValue
            startAuction = DateToString(Formatter: "yyyy-MM-dd HH:mm", date: dateValue)
            self.mainView.txtStartAuction.text = startAuction
            self.mainView.txtEndAuction.text = ""
        }
        else if self.mainVM.isDateSelection == 1 {
            startAuction = DateToString(Formatter: "yyyy-MM-dd HH:mm", date: dateValue)
            self.mainView.txtEndAuction.text = startAuction
        }
    }
}

