//
//  AddBooksView.swift
//  Booksbee
//
//  Created by vishal on 14/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

class AddBooksView: UIView {
    
    //MARK:Outlets
    @IBOutlet weak var btnAuction: UIButton!
    @IBOutlet weak var btnDirectSale: UIButton!
    
    @IBOutlet weak var txtBookName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPrize: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCategory: SkyFloatingLabelTextField!
    @IBOutlet weak var txtBookAuthorName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtISBNNumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblIntroductions: UILabel!
    @IBOutlet weak var lblEnterHere: UILabel!
    @IBOutlet weak var txtvwIntroductions: UITextView!
    
    @IBOutlet weak var btnAddBook: CustomButton!
    
    @IBOutlet weak var collectionImageOfBooks: UICollectionView!
    
    @IBOutlet weak var vwAuctionTime: UIView!
    @IBOutlet weak var txtStartAuction: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEndAuction: SkyFloatingLabelTextField!
    
    
    func setUpUI() {
        
        btnAuction.setTitle("Auction".localized, for: .normal)
        btnDirectSale.setTitle("Direct Sale".localized, for: .normal)
        
        [txtPrize, txtBookName,txtCategory,txtBookAuthorName,txtISBNNumber, txtStartAuction, txtEndAuction].forEach { (txt) in
            txt?.placeholderColor = .appDetailTextColor
            txt?.placeholderFont = themeFont(size: 16, fontname: .medium)
            txt?.font = themeFont(size: 16, fontname: .medium)
            txt?.titleColor = .appTitleTextColor
            txt?.titleFont = themeFont(size: 16, fontname: .medium)
            txt?.titleColor = .appDetailTextColor
            txt?.selectedTitleColor = .appDetailTextColor
                if getLanguage() == arabic {
                txt?.isLTRLanguage = false
            }
        }
        
        txtPrize.placeholder = "Price".localized
        txtBookName.placeholder = "Book name".localized
        txtPrize.title = "Price".localized
        txtBookName.title = "Book name".localized
        
        txtCategory.placeholder = "Category".localized
        txtCategory.title = "Category".localized

        txtBookAuthorName.placeholder = "Author name".localized
        txtBookAuthorName.title = "Author name".localized
        
        txtISBNNumber.placeholder = "ISBN".localized
        txtISBNNumber.title = "ISBN".localized
                
        txtStartAuction.placeholder = "Start Auction".localized
        txtStartAuction.title = "Start Auction".localized
        
        txtEndAuction.placeholder = "End Auction".localized
        txtEndAuction.title = "End Auction".localized
        
        [lblIntroductions].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .appDetailTextColor
            lbl?.text = "Introduction".localized
        }
        
        [lblEnterHere].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .appDetailTextColor
            lbl?.text = "Enter here".localized
        }
        
        [txtvwIntroductions].forEach { (txtvw) in
            txtvw?.textColor = .appTitleTextColor
            txtvw?.font = themeFont(size: 16, fontname: .medium)
            
            if getLanguage() == arabic {
                txtvw?.textAlignment = .right
            }

        }
        
        btnAuction.isSelected = true
        
        self.registerXIB()
    }
    
    func registerXIB() {
        self.collectionImageOfBooks.register(UINib(nibName: "AddBooksCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddBooksCollectionCell")
    }
    
    func checkValidation() -> Bool{
        
        if self.txtBookName.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter book name".localized)
            return false
        }
        else if self.txtCategory.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please select category".localized)
            return false
        }
        else if self.txtBookAuthorName.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter book author name".localized)
            return false
        }
        else if self.txtISBNNumber.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter ISBN number".localized)
            return false
        }
        else if self.txtPrize.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter price".localized)
            return false
        }
        else if self.txtvwIntroductions.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter introduction".localized)
            return false
        }
        
        return true
    }
}

