//
//  MyBookTableCell.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SDWebImage

class MyBookTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgCover: CustomImageView!
    @IBOutlet weak var lblBookTitle: UILabel!
    @IBOutlet weak var lblBookAuthor: UILabel!
    @IBOutlet weak var vwStarRating: HCSStarRatingView!
    @IBOutlet weak var lblAvgRate: UILabel!
    @IBOutlet weak var lblPrize: UILabel!
    @IBOutlet weak var lblSubPrize: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var vwBottomLine: UIView!
    @IBOutlet weak var vwShowAllBid: UIView!
    @IBOutlet weak var btnShowAllBid: CustomButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()
    }
    
    override func layoutSubviews() {
        
        imgCover.layer.cornerRadius = 5
        imgCover.layer.masksToBounds = true
    }
    
    func setUpUI() {
        
        [lblBookTitle].forEach { (lbl) in
            lbl?.textColor = .appTitleTextColor
            lbl?.font = themeFont(size: 20, fontname: .medium)
        }
        
        [lblBookAuthor].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [lblPrize].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [lblAvgRate,lblSubPrize].forEach { (lbl) in
            lbl?.textColor = .appDetailTextColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        [btnEdit, btnShowAllBid].forEach { (btn) in
            btn?.setTitleColor(.white, for: .normal)
            btn?.layer.cornerRadius = btn!.frame.size.height/2
            btn?.backgroundColor = UIColor.appGoldenColor
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        }
        
        btnShowAllBid.setTitle("Show All Bid".localized, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupMyBookCell(bookData: Bookdata){
        
        self.imgCover.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgCover.sd_setImage(with: bookData.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, completed: nil)
        self.lblBookTitle.text =  bookData.bookName
        self.lblAvgRate.text =  String(bookData.rateCount) + " " + "Reviews".localized
        self.lblPrize.text =  bookData.bookPrice
        self.lblSubPrize.text =  ""
        self.vwStarRating.value = bookData.rates
        self.lblBookAuthor.text = bookData.author
        self.vwStarRating.accurateHalfStars = true
        self.vwStarRating.allowsHalfStars = true

    }
    
    func setUpCartData(data: Bookdata) {
        self.imgCover.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgCover.sd_setImage(with: data.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, completed: nil)
        self.lblBookTitle.text =  data.bookName
        self.lblAvgRate.text =  "\(String(data.rateCount))" + " " + "Reviews".localized
        self.lblPrize.text =  data.bookPrice
        self.lblSubPrize.text =  ""
        self.lblBookAuthor.text = data.author
        self.vwStarRating.value = data.rates
        self.vwStarRating.accurateHalfStars = true
        self.vwStarRating.allowsHalfStars = true

    }
    
}
