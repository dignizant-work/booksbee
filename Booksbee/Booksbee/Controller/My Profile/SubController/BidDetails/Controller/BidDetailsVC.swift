//
//  BidDetailsVC.swift
//  Booksbee
//
//  Created by vishal on 11/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import UIKit

class BidDetailsVC: UIViewController {

    //MARK: Variables
    lazy var mainView: BidDetailsView = { [unowned self] in
        return self.view as! BidDetailsView
    }()
    
    lazy var mainVM: BidDetailsVM = { [unowned self] in
        return BidDetailsVM(theController: self)
    }()
    
    //MARK: Conroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.mainView.setUpUI()
        setupNavigationbarBackButton(titleText: self.mainVM.bookTitle)
        self.mainVM.getBidDetailsData { data in
            self.mainView.setUpData(data: self.mainVM.dictBookData!.bookDataModel!)
        }
    }
    
}


//MARK: Button Action
extension BidDetailsVC {
    
    @IBAction func btnpayNowAction(_ sender: Any) {
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
        vc.mainVM.dictBookData = self.mainVM.dictBookData
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
