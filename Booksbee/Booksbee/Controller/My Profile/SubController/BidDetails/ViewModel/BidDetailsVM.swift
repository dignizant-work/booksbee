//
//  BidDetailsVM.swift
//  Booksbee
//
//  Created by vishal on 11/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class BidDetailsVM {
    
    //MARK: Variables
    var theController = BidDetailsVC()
    var dictBookData: BookModel? = nil
    var bookId = 0
    var bookTitle = ""
    
    init(theController: BidDetailsVC) {
        self.theController = theController
    }
    
    func getBidDetailsData(completionHandlor:@escaping(JSON)->Void) {
        
        var param = [String:Any]()
        
        if isBookBidWinner {
            isBookBidWinner = false
            param["book_id"] = dictPushData["notification_type"].stringValue //when push tap
        }
        else {
            param["book_id"] = self.bookId  //When listing throught come
        }
        
        let url = bidDetailsURL
        showIndicator()
        
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Dict:-\(dict)")
                
                if dict["flag"].intValue == 0  {
                    print("")
                }
                else {
                    self.dictBookData = BookModel(JSON: dict.dictionaryObject!)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}

