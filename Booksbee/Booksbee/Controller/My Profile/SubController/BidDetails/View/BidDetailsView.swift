//
//  BidDetailsView.swift
//  Booksbee
//
//  Created by vishal on 11/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import Foundation
import UIKit
import HCSStarRatingView
import SDWebImage

class BidDetailsView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var btnPrice: CustomButton!
    @IBOutlet weak var lblBookName: UILabel!
    @IBOutlet weak var lblBookRatting: UILabel!
    @IBOutlet weak var vwBookRatting: HCSStarRatingView!
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnPayNow: CustomButton!
    
    @IBOutlet weak var lblBidPrize: UILabel!
    @IBOutlet weak var lblBidPrizeValue: UILabel!
    
    func setUpUI() {
        
        imgBook.layer.cornerRadius = 8
        imgBook.layer.masksToBounds = true
        
        [lblBookName,lblUserName].forEach { (lbl) in
            lbl?.textColor = .black
        }
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.bounds.width/2
        imgBook.layer.masksToBounds = true
        
        lblBookName.font = themeFont(size: 20, fontname: .medium)
        
        lblBookRatting.textColor = .lightGray
        lblBookRatting.font = themeFont(size: 13, fontname: .regular)
        
        lblUserName.font = themeFont(size: 22, fontname: .medium)
        
        btnPrice.backgroundColor = UIColor.appGoldenColor
        btnPrice.titleLabel?.font = themeFont(size: 20, fontname: .medium)
        btnPrice.setTitleColor(.white, for: .normal)
        
        [lblBidPrize,lblBidPrizeValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
        }
        
        lblBidPrize.textColor = .appTitleTextColor
        lblBidPrize.text = "Bid Prize".localized
        lblBidPrizeValue.textColor = .red
    }
    
    func setUpData(data: BookDataModel) {
        
        self.lblBookName.text = data.bookName
        self.lblUserName.text = data.author
        self.btnPrice.setTitle(data.book_price, for: .normal)
        
        self.lblBookRatting.text = "\(data.rateCount)" + " " + "Reviews".localized
        
        self.imgBook.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgBook.sd_setImage(with: data.image.toURL(), placeholderImage: UIImage(named: "ic_book_splash_holder"), options: .lowPriority, completed: nil)
        
        self.vwBookRatting.value = data.rates
        self.vwBookRatting.accurateHalfStars = true
        self.vwBookRatting.allowsHalfStars = true
        
        self.imgUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgUserProfile.sd_setImage(with: data.ownerImage.toURL(), placeholderImage: UIImage(named: "ic_my_profile_nig_splash_holder"), options: .lowPriority, completed: nil)
        self.lblUserName.text = data.ownerName
        
        self.lblDescription.text = data.description
        self.lblBidPrizeValue.text = data.bidPrice
        if data.isWinner == 0 {
            self.btnPayNow.isHidden = true
        }
        else {
            self.btnPayNow.isHidden = false
        }
    }
    
}
