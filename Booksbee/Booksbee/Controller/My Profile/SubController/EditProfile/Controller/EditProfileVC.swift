//
//  EditProfileVC.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit
import TOCropViewController
import SwiftyJSON
import SDWebImage
import FirebaseAuth

class EditProfileVC: UIViewController {

    //MARK: Variables
    lazy var mainView: EditProfileView = {
        return self.view as! EditProfileView
    }()
    
    lazy var mainVM: EditProfileVM = {
        return EditProfileVM(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavigationbarBackButton(titleText: "Edit profile".localized)
        
        CommonServices().getUserProfileAPI(isShowLoader: true) { response in
            self.mainView.setUpUI()
            self.setupData()
        }
    }
    
}

//MARK: Button Action
extension EditProfileVC {
    
    @IBAction func btnCameraAction(_ sender: Any) {
        ImagePickerManager().pickImage(self) { (img) in
            self.openCropVC(Image: img)
        }
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        
        if sender.tag == 1 { //Email tapped
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.mainVM.isCheckController = .editEmail
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
        else if sender.tag == 2 { //Phone tapped
            
            if self.mainView.checkValidationForPhoneNumber(){
                
                let param = ["phone_no":self.mainView.txtPhoneNumber.text!,
                             "email":""] as [String:Any]
                
                CommonServices().checkUniqueValueAPI(param: param) { data in
                    self.setupMobileNumberOTP()
                }
            }
        }
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if mainView.checkValidation() {
            editProfileAPI()
        }
    }
    
    
    @IBAction func btnContryCodeTapped(_ sender: UIButton) {
        
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overFullScreen
        obj.mainView.delegate = self
        self.present(obj, animated: false, completion: nil)
    }

    
    func setupData(){
        self.mainView.txtEmailAddress.text = getUserData()?.data?.email
        self.mainView.txtFullName.text = getUserData()?.data?.name
        self.mainView.txtUserName.text = getUserData()?.data?.username
        self.mainView.txtPhoneNumber.text = getUserData()?.data?.mobileNo
        self.mainView.lblContryCode.text = getUserData()?.data?.contryCode
        self.mainVM.countryDialCode = getUserData()?.data?.contryCode ?? ""
        
        self.mainView.imgUserProfile.sd_setImage(with: (getUserData()?.data?.profile ?? "").toURL(), placeholderImage: UIImage(named: "ic_my_profile_nig_splash_holder"), options: .lowPriority, completed: nil)
        
        self.mainView.txtPhoneNumber.addTarget(self, action: #selector(txtFieldValueChanged(txtField:)), for: .editingChanged)
    }
}

//MARK: - TOCropView Delegate

extension EditProfileVC : TOCropViewControllerDelegate
{
    
    func openCropVC(Image:UIImage)
    {
        let cropVC = TOCropViewController(croppingStyle: .circular, image: Image)
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.width)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.appGoldenColor, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.appGoldenColor, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        self.present(cropVC, animated: false, completion: nil)
        
    }
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        
        self.mainView.imgUserProfile.image = image
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        
        dismiss(animated: true, completion: nil)
    }
    
}


//MARK: - OTP
extension EditProfileVC {
    
    func setupMobileNumberOTP() {
        
        let phoneNumber = "\(self.mainVM.countryDialCode)"+"\(self.mainView.txtPhoneNumber.text!)"
        print("phoneNumber:-\(phoneNumber)")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            
            if error == nil {
                print("verificationId:-",verificationId!)
                guard let verify = verificationId else { return }
                Defaults.set(verify, forKey: "verificationId")
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
                vc.mainVM.strPhoneNumber = phoneNumber
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.mainVM.isCheckController = .editPhone
                vc.mainVM.handlorResendOTPVarify = {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
                    vc.mainVM.strPhoneNumber = phoneNumber
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    vc.mainVM.isCheckController = .editPhone
                    vc.mainVM.handlorNumberVarify = {
                        self.mainView.btnEditProfileEnable()
                    }
                    self.present(vc, animated: true, completion: nil)
                }
                vc.mainVM.handlorNumberVarify = {
                    self.mainView.btnEditProfileEnable()
                }
                self.present(vc, animated: true, completion: nil)
                
            }
            else {
                makeToast(strMessage: error?.localizedDescription ?? "")
                print("Error:-\(error?.localizedDescription ?? "")")
            }
        }
    }
    
}


//MARK: - Edit Profile API calling

extension EditProfileVC{
    
    func editProfileAPI(){
        
        let dict = [
                    "email":self.mainView.txtEmailAddress.text ?? "",
                    "full_name": self.mainView.txtFullName.text ?? "",
                    "username": self.mainView.txtUserName.text ?? "",
                    "mobile_no": self.mainView.txtPhoneNumber.text ?? "",
                    "country_code" : self.mainVM.countryDialCode,
                    ] as [String : Any]
        
        self.mainVM.editProfileAPI(param: dict) { (data) in
            
            self.navigationController?.popViewController(animated: false)
        }
    }
    
}

// MARK:- CountryCode Delegate
extension EditProfileVC : CountryCodeDelegate {
    
    func CountryCodeDidFinish(data: JSON) {
        
        print("data:\(data)")
        self.mainVM.countryDialCode = data["dial_code"].stringValue
        self.mainView.lblContryCode.text = data["dial_code"].stringValue
    }
}
