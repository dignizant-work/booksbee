//
//  EditProfileVC_TextfieldDelegate.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension EditProfileVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func txtFieldValueChanged(txtField: UITextField) {
        
        if txtField == self.mainView.txtPhoneNumber {
            if txtField.text == getUserData()?.data?.mobileNo {
                self.mainView.sendButtonDisabled()
                self.mainView.btnEditProfileEnable()
            }
            else {
                self.mainView.sendButtonEnable()
                self.mainView.btnEditProfileDisabled()
            }
        }
        else if txtField == self.mainView.txtEmailAddress {
            
        }
    }
}
