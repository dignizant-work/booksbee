//
//  EditProfileVM.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class EditProfileVM {
    
    //MARK:- Outlets
    var theController = EditProfileVC()
    
    init(theController: EditProfileVC) {
        self.theController = theController
    }
    
    var countryDialCode = ""
    
}

//MARK: - API calling

extension EditProfileVM{
    
    func editProfileAPI(param:[String:Any], completionHandlor:@escaping(JSON)->Void) {
        
        let url = updateProfile
        
        showIndicator()
        
        WebServices().MakePostWithImageAPI(name: url, params: param, images: [self.theController.mainView.imgUserProfile.image ?? UIImage()],imageName: "profile_image") { (data, error, statusCode) in
            
            hideIndicator()
            
            if (error == nil) {
                
                let dict = JSON(data!)
                
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    savedUserData(user: dict)
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
            
        }
        
    }
    
}
