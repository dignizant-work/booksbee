//
//  EditProfileView.swift
//  Booksbee
//
//  Created by vishal on 30/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit


class EditProfileView: UIView {
    
    //MARK: Variables
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var lblContryCode: UITextField!
    
    
    @IBOutlet weak var btnSendEmailPhone: UIButton!
    
    @IBOutlet weak var btnSendPhone: UIButton!
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var btnCamera: CustomButton!
    
    @IBOutlet weak var btnSave: CustomButton!
    
    
    func setUpUI() {
        
        imgBG.image = UIImage(named: "ic_profile_bg")?.imageFlippedForRightToLeftLayoutDirection()
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.height/2
        imgUserProfile.layer.masksToBounds = true
        
        [vwBG].forEach { (vw) in
            vw?.clipsToBounds = true
            vw?.layer.cornerRadius = 30
            vw?.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        
        btnSave.titleLabel?.font = themeFont(size: 18, fontname: .bold)
        btnSave.setTitleColor(.white, for: .normal)
        btnSave.backgroundColor = .appTitleTextColor
        btnSave.setTitle("Edit profile".localized, for: .normal)

        [txtFullName,txtUserName,txtEmailAddress,txtPhoneNumber].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.textColor = .appTitleTextColor
            txt?.placeholderColor(.appDetailTextColor)
            textFieldClearButtonColorSet(txt!, .appTitleTextColor)
            
            if getLanguage() == arabic{
                txt?.textAlignment = .right
            }
        }
        
        self.btnSendEmailPhone.isHidden = true
        sendButtonDisabled()
    }
    
    func sendButtonEnable() {
        self.btnSendPhone.isUserInteractionEnabled = true
        self.btnSendPhone.alpha = 1.0
    }
    
    func sendButtonDisabled() {
        self.btnSendPhone.isUserInteractionEnabled = false
        self.btnSendPhone.alpha = 0.5
    }
    
    func btnEditProfileEnable() {
        self.btnSave.isUserInteractionEnabled = true
        self.btnSave.alpha = 1.0
    }
    
    func btnEditProfileDisabled() {
        self.btnSave.isUserInteractionEnabled = false
        self.btnSave.alpha = 0.5
    }
    
    func checkValidation() -> Bool {
        
        if txtFullName.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter full name".localized)
            return false
        }
        else if txtUserName.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter user name".localized)
            return false
        }
        else if txtEmailAddress.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter email address".localized)
            return false
        }
        else if !isValidEmail(emailAddressString: txtEmailAddress.text ?? "") {
            makeToast(strMessage: "Please enter valid email address".localized)
            return false
        }
        else if txtPhoneNumber.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter phone number".localized)
            return false
        }
        
        return true
    }
    
    func checkValidationForPhoneNumber() -> Bool{
        
         if self.txtPhoneNumber.text!.toTrim().isEmpty {
            makeToast(strMessage: "Please enter phone number".localized)
            return false
        }
        else if !self.txtPhoneNumber.text!.isNumeric {
            makeToast(strMessage: "Please enter valid phone number".localized)
            return false
        }
        
        return true

    }
    
    
}
