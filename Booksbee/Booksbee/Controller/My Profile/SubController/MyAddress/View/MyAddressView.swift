//
//  MyAddressView.swift
//  Booksbee
//
//  Created by vishal on 01/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

class MyAddressView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var tblAddress: UITableView!
    @IBOutlet weak var btnAddaddrress: CustomButton!
    @IBOutlet weak var tblAddressHeightConstraint: NSLayoutConstraint!
    
    
    func setUpUI() {
        
        imgBG.image = UIImage(named: "ic_profile_bg")?.imageFlippedForRightToLeftLayoutDirection()
        [vwBG].forEach { (vw) in
            vw?.clipsToBounds = true
            vw?.layer.cornerRadius = 30
            vw?.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        
        btnAddaddrress.backgroundColor = .appTitleTextColor
        btnAddaddrress.setTitleColor(.white, for: .normal)
        btnAddaddrress.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        
        registerXib()
    }
    
    func registerXib() {
        
        tblAddress.register(UINib(nibName: "MyAddressTableCell", bundle: nil), forCellReuseIdentifier: "MyAddressTableCell")
        tblAddress.reloadData()
    }
}
