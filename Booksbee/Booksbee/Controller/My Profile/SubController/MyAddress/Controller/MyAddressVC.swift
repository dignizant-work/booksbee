//
//  MyAddressVC.swift
//  Booksbee
//
//  Created by vishal on 01/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class MyAddressVC: UIViewController {

    //MARK: Variables
    lazy var mainView: MyAddressView = {
        return self.view as! MyAddressView
    }()
    
    lazy var mainVM: MyAddressVM = {
        return MyAddressVM(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI()
        self.setupNavigationbarBackButton(titleText: "My address".localized)
        setUp()
        
        self.mainVM.getAddressListAPI { data in
            print("Addresses:-\(data)")
            self.mainView.tblAddress.reloadData()
            self.setUp()
        }
    }
    
    
    func setUp() {
        
        if self.mainVM.dictAddressData?.data.count ?? 0 <= 5 {
            self.mainView.tblAddressHeightConstraint.constant = self.view.frame.size.height-150
        }
        else {
            self.mainView.tblAddressHeightConstraint.constant = CGFloat((self.mainVM.dictAddressData?.data.count ?? 0)*125)+50.0
        }
    }
    
}

//MARK: Button action
extension MyAddressVC {
    
    @IBAction func btnAddaddrressAction(_ sender: Any) {
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        
        vc.mainVM.handlorAddUpdateAddress = {
            self.mainVM.getAddressListAPI { data in
                self.mainView.tblAddress.reloadData()
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
