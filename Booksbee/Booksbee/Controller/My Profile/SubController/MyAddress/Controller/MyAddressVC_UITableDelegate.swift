//
//  MyAddressVC_UITableDelegate.swift
//  Booksbee
//
//  Created by vishal on 01/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

extension MyAddressVC: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainVM.dictAddressData?.data.count ?? 0 == 0 || self.mainVM.dictAddressData?.data == nil{
            setTableViewEmptyMessage(self.mainVM.dictAddressData?.message ?? "", tableView: tableView)
            return 0
        }
    
        tableView.backgroundView = nil
        return self.mainVM.dictAddressData?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAddressTableCell", for: indexPath) as! MyAddressTableCell
        
        if let dict = self.mainVM.dictAddressData?.data[indexPath.row] {
            cell.setUpData(data: dict)
        }
        
        cell.btnDefault.tag = indexPath.row
        cell.btnDefault.addTarget(self, action: #selector(setUpDefaultAddress(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = profileStoryboard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        if let dict = self.mainVM.dictAddressData?.data[indexPath.row] {
            vc.mainVM.dictAddressData = dict
        }
        vc.mainVM.handlorAddUpdateAddress = {
            self.mainVM.getAddressListAPI { data in
                self.mainView.tblAddress.reloadData()
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let btnDelete = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
            
            self.showAlert("", "Are you sure you want to remove this item?".localized, "Delete".localized, "Cancel".localized) {
                    
                    let address_Id = self.mainVM.dictAddressData?.data[indexPath.row].id ?? 0
                    
                    self.mainVM.deleteMyAddressApi(addressId: "\(address_Id)") { data in
                        
                        self.mainVM.dictAddressData?.data.remove(at: indexPath.row)
                        
                        if self.mainVM.dictAddressData?.data.count == 0 {
                            self.mainVM.getAddressListAPI { data in
                                tableView.reloadData()
                            }
                        }
                        tableView.reloadData()
                }
                tableView.reloadData()
            }
            completion(true)
        }
        btnDelete.image = UIImage(named: "ic_delete")
        btnDelete.backgroundColor = .white
        return UISwipeActionsConfiguration(actions: [btnDelete])
    }
    
    @objc func setUpDefaultAddress(_ sender: UIButton) {
        
        showAlert("Are you sure?".localized, "You want to change your default address".localized) {
            
            if let dict = self.mainVM.dictAddressData?.data[sender.tag] {
                
                let param = ["address_id":dict.id]
                self.mainVM.isAddressDefaultAPI(param: param) { data in
                    
                    let arr : [AddressData] = self.mainVM.dictAddressData?.data ?? []
                    for i in 0..<arr.count {
                        let data = self.mainVM.dictAddressData?.data[i]
                        data?.isDefault = 0
                        self.mainVM.dictAddressData?.data[i] = data!
                    }
                    self.mainVM.dictAddressData?.data[sender.tag].isDefault = 1
//                    print("Data:-\(dict)")
                    self.mainView.tblAddress.reloadData()
                }
            }
        }
    }
}
