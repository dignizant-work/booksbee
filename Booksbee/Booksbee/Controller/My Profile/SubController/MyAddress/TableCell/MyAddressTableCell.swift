//
//  MyAddressTableCell.swift
//  Booksbee
//
//  Created by vishal on 01/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import UIKit

class MyAddressTableCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var vwBG: CustomView!
    @IBOutlet weak var lblAddressName: UILabel!
    @IBOutlet weak var lblAddressNameValue: UILabel!
    @IBOutlet weak var lblGovernate: UILabel!
    @IBOutlet weak var lblGovarnorateValue: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressValue: UILabel!
    @IBOutlet weak var btnDefault: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()
    }
    
    func setUpUI() {
        
        lblAddressName.text = "Address Name : ".localized
        lblGovernate.text = "Govarnorate : ".localized
        lblAddress.text = "Address :".localized
        
        [lblAddressName,lblGovernate,lblAddress].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .medium)
            lbl?.textColor = .appTitleTextColor
        }
        
        [lblAddressNameValue,lblGovarnorateValue,lblAddressValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 13, fontname: .regular)
            lbl?.textColor = .appDetailTextColor
        }
        
    }
    
    func setUpData(data: AddressData) {
        
        lblAddressNameValue.text = data.addressName
        lblGovarnorateValue.text = data.street
        
        if data.floor.isEmpty || data.floor == "" {
            lblAddressValue.text = "\(data.block), \(data.avenue), \(data.street), \(data.area)"
        }
        else {
            lblAddressValue.text = "\(data.block) - \(data.floor), \(data.avenue), \(data.street), \(data.area)"
        }
        
//        lblAddressValue.text = "\(data.block) - \(data.floor), \(data.avenue), \(data.area)"
        
        if data.isDefault == 0 {
            btnDefault.isSelected = false
        }
        else {
            btnDefault.isSelected = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
