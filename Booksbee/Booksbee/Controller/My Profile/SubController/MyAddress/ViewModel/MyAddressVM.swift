//
//  MyAddressVM.swift
//  Booksbee
//
//  Created by vishal on 01/12/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MyAddressVM {
    
    //MARK:- variables
    var theController = MyAddressVC()
    var arrAddress: [JSON] = []
    var dictAddressData = AddressModel(JSON: JSON().dictionaryValue)
    
    init(theController: MyAddressVC) {
        self.theController = theController
    }
    
    func getAddressListAPI(completionHandlor:@escaping(JSON)->Void) {
        
        let url = myAddressesURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
//                print("Dict:-\(dict)")
                self.dictAddressData = AddressModel(JSON: dict.dictionaryObject!)
                completionHandlor(dict)
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func isAddressDefaultAPI(param:[String:Any],completionHandlor:@escaping(JSON)->Void) {
        
        let url = setDefaultAddressURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                print("Status code:-\(statusCode ?? 0)")
                let dict = JSON(data!)
//                print("Dict:-\(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func deleteMyAddressApi(addressId:String, completionHandlor:@escaping(JSON)->Void) {
        
        let param: [String:Any] = ["address_id":addressId]
        
        let url = deleteMyAddressURL
        showIndicator()
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
//                print("Data:-\(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    makeToast(strMessage: dict["message"].stringValue)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
