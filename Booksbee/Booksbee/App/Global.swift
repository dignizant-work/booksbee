//
//  Global.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import MaterialComponents
import RSLoadingView

//Variables
let appdelgate = UIApplication.shared.delegate as! AppDelegate
let Defaults = UserDefaults.standard
let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
//let iphoneDeviceType = UIDevice().type
let deviceType = "1"    // IOS device type for API
var isEnglish = false
let deviceId = UIDevice.current.identifierForVendor!.uuidString
var isTabbarSearchButtonHidden = false
let appName = Bundle.appName()
let currentTimeZone = TimeZone.current.identifier

var isGuest : Bool = false
let fcmToken = Defaults.value(forKey: "fcmToken") as? String ?? ""

let rsLoadingView = RSLoadingView()



//MARK:- StoryBoard
let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let profileStoryboard = UIStoryboard(name: "MyProfile", bundle: nil)
let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)


//MARK: Push variable
var dictPushData = JSON()
var isBookBidWinner = false
var isAdminNotification = false
var isOrderStatusNotification = false


var middleButton = UIButton.init(type: .custom)

func setUpTabbarRoot(_ selectIndex: Int? = 0) {
    appdelgate.tabBarVC = TabbarVC()
    let nav = UINavigationController(rootViewController: appdelgate.tabBarVC)
    appdelgate.window?.rootViewController = nav
    appdelgate.tabBarVC.selectedIndex = selectIndex ?? 0
    appdelgate.window?.makeKeyAndVisible()
}

func checkLanguage() {
    if getLanguage() == arabic {
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
    }
    else {
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
}

func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
    let rect = CGRect(x:0, y: 0, width: size.width, height: size.height)
    let rectLine = CGRect(x:0, y:size.height-lineSize.height,width: lineSize.width,height: lineSize.height)
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    UIColor.clear.setFill()
    UIRectFill(rect)
    color.setFill()
    UIRectFill(rectLine)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
}

func isNotchDevice() -> Bool {
    if #available( iOS 11.0, * ) {
        
        if (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 > 0) {
            return true
        }
        else {
            return false
        }
    }
    return false
}

func setTableViewEmptyMessage(_ message: String, tableView: UITableView) {
    let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height))
    
    noDataLabel.textAlignment = .center
    noDataLabel.text = message
    noDataLabel.numberOfLines = 0
    noDataLabel.textColor = UIColor.black
    noDataLabel.font = themeFont(size: 14, fontname: .regular)
    noDataLabel.center = tableView.center
    
    tableView.backgroundView = noDataLabel
}

//MARK:- Print Fonts
func printFonts() {
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName )
        print("Font Names = [\(names)]")
    }
}

//MARK: - Set Toaster
func makeToast(strMessage : String){
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager().show(messageSnack)
}

//Label attributed text multiple color set
func stringMultipleColorSet(first:String, firstTxtFont: UIFont, second:String, secondTxtFont: UIFont) -> NSAttributedString {
    
    let firstStr = [NSAttributedString.Key.font : firstTxtFont, NSAttributedString.Key.foregroundColor : UIColor.appTitleTextColor]
    
    let secondStr = [NSAttributedString.Key.font : secondTxtFont, NSAttributedString.Key.foregroundColor : UIColor.appDetailTextColor]
    
    let attributedString1 = NSMutableAttributedString(string:first, attributes:firstStr)
    let attributedString2 = NSMutableAttributedString(string:second, attributes:secondStr)
    
    attributedString1.append(attributedString2)
    return attributedString1
}

//MARK: - Activity Indicator

func showIndicator(){
    
    rsLoadingView.variantKey = "inAndOut"
    rsLoadingView.mainColor = .appGoldenColor
    rsLoadingView.show(on: appdelgate.window?.rootViewController?.view ?? UIView())
}

func hideIndicator(){
    RSLoadingView.hide(from: appdelgate.window?.rootViewController?.view ?? UIView())
}

func textFieldClearButtonColorSet(_ textField: UITextField,_ color: UIColor) {
    guard let clearButton = textField.value(forKey: "_clearButton") as? UIButton else {
        return
    }
    let templateImage = clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
    clearButton.setImage(templateImage, for: .normal)
    clearButton.setImage(templateImage, for: .highlighted)
    clearButton.tintColor = color
}

func randomString(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0...length-1).map{ _ in letters.randomElement()! })
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImage().jpegData(compressionQuality: quality.rawValue)
        //        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
    
    func png(_ quality: JPEGQuality) -> Data? {
        return self.pngData()
    }
}

extension String {
    
    public func toURL() -> URL? {
        return URL(string: self)
    }
    
    func toTrim() -> String {
        let trimmedString = self.trimmingCharacters(in: .whitespaces)
        return trimmedString
    }
}

extension UICollectionView {

    func showCollectionEmptyEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = themeFont(size: 14, fontname: .regular)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
    }

    func restore() {
        self.backgroundView = nil
    }
}

extension UIImageView {

    func tintImageColor(color: UIColor) {
        guard let image = image else { return }
        self.image = image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.tintColor = color
    }

}


extension Bundle {
    static func appName() -> String {
        guard let dictionary = Bundle.main.infoDictionary else {
            return ""
        }
        if let version : String = dictionary["CFBundleName"] as? String {
            return version
        } else {
            return ""
        }
    }
}
