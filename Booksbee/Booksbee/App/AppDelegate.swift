//
//  AppDelegate.swift
//  Booksbee
//
//  Created by iMac on 23/11/20.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import Messages
import SwiftyJSON

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    var tabBarVC = TabbarVC()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        setUpPushNotifications(application: application)
        checkLanguage()
//        setUpTabbarRoot()
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        application.applicationIconBadgeNumber = 0
    }
    
}


//MARK: Setup notifications
extension AppDelegate {
    
    //Setup firebase notification
    func setUpPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        }
        else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        let fcmTokenString = fcmToken ?? ""
        Defaults.setValue(fcmTokenString, forKey: "fcmToken")
        print("Firebase registration token: \(fcmTokenString)")
    }
    
    //Notification Show
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let dictData = JSON(notification.request.content.userInfo)
        print("JSONDICT: ", dictData)
        
        completionHandler([.alert, .sound, .badge])
    }
    
    //Notification Tapped
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = JSON(response.notification.request.content.userInfo)
        dictPushData = userInfo
        print("info:=\(userInfo)")
        
        if userInfo["notification_type"].stringValue == "1" {
            isBookBidWinner = true
            setUpTabbarRoot(4)
        }
        else if userInfo["notification_type"].stringValue == "2" {
            isAdminNotification = true
            setUpTabbarRoot(4)
        }
        else if userInfo["notification_type"].stringValue == "3" {
            isOrderStatusNotification = true
            setUpTabbarRoot(4)
        }
    }
    
    //Got device token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Device Token:- \(token)")
    }
    
}
