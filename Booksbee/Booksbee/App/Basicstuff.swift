//
//  Basicstuff.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

//MARKL - Fonts

//MARK: - Validation email

func isValidEmail(emailAddressString:String) -> Bool {
    var returnValue = true
    let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
    
    do {
        let regex = try NSRegularExpression(pattern: emailRegEx)
        let nsString = emailAddressString as NSString
        let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
        
        if results.count == 0 {
            returnValue = false
        }
        
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        returnValue = false
    }
    
    return  returnValue
}

public func isValidPassword(passwordString:String) -> Bool {
    let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
    return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: passwordString)
}

func dialNumber(number : String) {
    
    if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    else {
        makeToast(strMessage: "Calling functinality not support this device")
    }
}
