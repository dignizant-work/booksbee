//
//  Constants.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


let preferredLanguage = NSLocale.preferredLanguages[0] // get system device language


///--- User data ---
func getUserData() -> LoginModel? {
    if let user = Defaults.object(forKey: user_data_key) as? Data, let json = JSON(user).dictionaryObject {
        return LoginModel(JSON: json)
    }
    return nil
}

func setValueUserDetail(forKey: String ,forValue: String) {

    guard let userDetail = UserDefaults.standard.value(forKey: user_data_key) as? Data else { return }
    var data = JSON(userDetail)
//    data["description"][forKey].stringValue = forValue
    data[forKey].stringValue = forValue

    guard let rowdata = try? data.rawData() else {return}
    Defaults.setValue(rowdata, forKey: user_data_key)
    Defaults.synchronize()
}

func savedUserData(user: JSON) {
    guard let rawData = try? user.rawData() else { return }
    Defaults.set(rawData, forKey: user_data_key)
    Defaults.synchronize()
}

func removeUserData() {
    
    DispatchQueue.main.async {
        Defaults.removeObject(forKey: "fcmToken")
        Defaults.removeObject(forKey: "isSearchString")
        Defaults.removeObject(forKey: user_data_key)
        Defaults.synchronize()
    }
}


//func updateUserDicData(user: JSON) {
//    guard let userDetail = UserDefaults.standard.value(forKey: user_data_key) as? Data else { return }
//    var data = JSON(userDetail)
//    data["user"] = user
//
//    guard let rowdata = try? data.rawData() else {return}
//    userDefault.setValue(rowdata, forKey: user_data_key)
//    userDefault.synchronize()
//}


//MARK: - Reload Extensions
extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

extension UICollectionView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

//MARK: File Manager path
extension FileManager {
    
    class func directoryUrl() -> URL? {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths.first
    }
    
    class func allRecordedData() -> [URL]? {
        if let documentsUrl = FileManager.directoryUrl() {
            do {
                let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
                return directoryContents.filter{ $0.pathExtension == "m4a" }
            } catch {
                return nil
            }
        }
        return nil
    }
    
}
