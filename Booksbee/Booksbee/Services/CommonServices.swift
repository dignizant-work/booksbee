//
//  CommonServices.swift
//  Booksbee
//
//  Created by vishal on 08/01/21.
//  Copyright © 2021 vishal. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class CommonServices {
    
    //MARK: Check Email/Phone unique
    func checkUniqueValueAPI(param: [String:Any],completionHandlor:@escaping(JSON)->Void) {
        
        let url = checkUniqueValueURL
//        showIndicator()
//        print("PARAM:- \(param)")
        WebServices().makePostApiWithBody(name: url, params: param) { (data, error, statusCode) in
            hideIndicator()
            if (error == nil) {
                let dict = JSON(data!)
                print("Dict:-\(dict)")
                if dict["flag"].boolValue == false {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
    
    func getUserProfileAPI(isShowLoader: Bool? = false, completionHandlor:@escaping(JSON)->Void) {
        
        let url = getUserProfileURL
        if isShowLoader == true {
            showIndicator()
        }
        WebServices().makePostApiWithBody(name: url, params: [String:Any]()) { (data, error, statusCode) in
            
            if (error == nil) {
                let dict = JSON(data!)
                print("Data:- \(dict)")
                if dict["flag"].intValue == 0 {
                    makeToast(strMessage: dict["message"].stringValue)
                }
                else {
                    savedUserData(user: dict)
                    completionHandlor(dict)
                }
            }
            else {
                makeToast(strMessage: error ?? "")
            }
        }
    }
}
