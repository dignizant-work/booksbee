//
//  Enum+Extension.swift
//  Booksbee
//
//  Created by vishal on 25/11/20.
//  Copyright © 2020 vishal. All rights reserved.
//

import Foundation
import UIKit

enum enumOtpVCCheck {
    case email
    case number
    case editEmail
    case editPhone
}

enum enumTermsConditionVCCheck {
    case termsCondition
    case privacyPolicy
    case aboutAs
}

enum enumOrderStatusVCCheck{
    case myOrder
    case confirmOrder
    case mybidList
}

enum enumBookOrCartVCCheck {
    case myBook
    case cart
}

enum enumSuccessOrFailVCCheck {
    case success
    case fail
}

enum enumAddOrEditBookVCCheck{
    case add
    case edit
}

enum enumSearchListDataCheck {
    
    case search
    case filter
    case ourTopBooks
    case viewAllBestSellers
    case sellerAllBook
}
