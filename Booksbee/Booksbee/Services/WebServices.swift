//
//  WebServices.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SystemConfiguration


struct WebServices {
        
    //MARK: - Post with body
    func makePostApiWithBody(name: String, params:[String:Any], completionHandler: @escaping(NSDictionary?, String?, Int?) -> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        var headers:[String:String] = [:]
        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["Accept"] = "application/json"
        if getUserData()?.data != nil{
            headers["Authorization"] = getUserData()?.accessToken
        }
        
        let url = baseURL + name
        print("url:-\(url)")
        print("Header:-\(headers)")
        print("PARAM:- \(params)")
        request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: headers).responseJSON(completionHandler: { (response) in
            
            hideIndicator()
            
            let statusCode = response.response?.statusCode
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                
                if let jsonData = response.data {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                    }
                    catch let err{
                        print("\n\n===========Error===========")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Print Server data:- " + str)
                        }
                        debugPrint(error)
                        print("===========================\n\n")
                        
                        debugPrint(err)
                        
                        error = err.localizedDescription
                    }
                }
                else {
                    debugPrint(error as Any)
                }
                
                if !error.isEmpty {
                    completionHandler(nil, error, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            }
        })
    }
    
    //MARK: - Post
    func MakePostAPI(name:String, params:[String:Any], completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
                
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        var headers:[String:String] = [:]
        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        
        if getUserData()?.data != nil{
            headers["Authorization"] = getUserData()?.accessToken
        }

        let url = baseURL + name
        print("url:-\(url)")
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            
            hideIndicator()
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                
                if let jsonData = response.data {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                    }
                    catch let err{
                        print("\n\n===========Error===========")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Print Server data:- " + str)
                        }
                        debugPrint(error)
                        print("===========================\n\n")
                        
                        debugPrint(err)
                        
                        error = err.localizedDescription
                    }
                }
                else {
                    debugPrint(error as Any)
                }
                
                if !error.isEmpty {
                    completionHandler(nil, error, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            }
            
            }.resume()
    }
    
    
    //MARK: - Get
    func MakeGetAPI(name:String, params:[String:Any], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        var headers:[String:String] = [:]
        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        
        if getUserData()?.data != nil{
            headers["Authorization"] = getUserData()?.accessToken
        }

        let url = baseURL + name
        print("url:-\(url)")
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            
            hideIndicator()
            
            let statusCode = response.response?.statusCode
//            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                
                if(response.error == nil) {
                    
                    if let _ = response.result.value as? Any {
                        completionHandler(response.result.value as? Any, nil, statusCode)
                    }
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                
                if let jsonData = response.data {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                    }
                    catch let err{
                        print("\n\n===========Error===========")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Print Server data:- " + str)
                        }
                        debugPrint(error)
                        print("===========================\n\n")
                        
                        debugPrint(err)
                        
                        error = err.localizedDescription
                    }
                }
                else {
                    debugPrint(error as Any)
                }
                
                if !error.isEmpty {
                    completionHandler(nil, error, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
                
            }
            }.resume()
    }
    
    func MakeGetAPIWithoutAuth(name:String, params:[String:Any], progress: Bool = true, completionHandler: @escaping (NSDictionary?, String)-> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        
//        var headers:[String:String] = [:]
//        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
//        headers["Content-Type"] = "application/x-www-form-urlencoded"
//
//        if getUserData()?.data != nil{
//            headers["Authorization"] = getUserData()?.accessToken
//        }

        
        let headers:[String : String] = [:]
        let url = baseURL + name
        print("url:-\(url)")
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            
            hideIndicator()
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, "")
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized)
                }
            case .failure( _):
                var error = ""
                
                if let jsonData = response.data {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                    }
                    catch let err{
                        print("\n\n===========Error===========")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Print Server data:- " + str)
                        }
                        debugPrint(error)
                        print("===========================\n\n")
                        
                        debugPrint(err)
                        
                        error = err.localizedDescription
                    }
                }
                else {
                    debugPrint(error as Any)
                }
                
                if !error.isEmpty {
                    completionHandler(nil, error)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized)
                }
            }
            }.resume()
    }
    
    //MARK: - Put
    func MakePutAPI(name:String, params:[String:Any], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
                
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        var headers:[String:String] = [:]
        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        
        if getUserData()?.data != nil{
            headers["Authorization"] = getUserData()?.accessToken
        }
        
        let url = baseURL + name
        print("url:-\(url)")
        
        Alamofire.request(url, method: .put, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
            
            hideIndicator()
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? Any, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                
                if let jsonData = response.data {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                    }
                    catch let err{
                        print("\n\n===========Error===========")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Print Server data:- " + str)
                        }
                        debugPrint(error)
                        print("===========================\n\n")
                        
                        debugPrint(err)
                        
                        error = err.localizedDescription
                    }
                }
                else {
                    debugPrint(error as Any)
                }
                
                if !error.isEmpty {
                    completionHandler(nil, error, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            }

            }.resume()
    }
    
    func MakeDeleteAPI(name:String, params:[String:Any], isAuth:Bool = true, completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
                
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        
        let headers:[String : String] = [:]
        print("headers:-",headers)
        let url = baseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        
        Alamofire.request(url, method: .delete, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            hideIndicator()
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                
                if let jsonData = response.data {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                    }
                    catch let err{
                        print("\n\n===========Error===========")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Print Server data:- " + str)
                        }
                        debugPrint(error)
                        print("===========================\n\n")
                        
                        debugPrint(err)
                        
                        error = err.localizedDescription
                    }
                }
                else {
                    debugPrint(error as Any)
                }
                
                if !error.isEmpty {
                    completionHandler(nil, error, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            }
            }.resume()
    }
    
    //MARK: PATCH
    func MakePatchAPI(name:String, params:[String:Any], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        var headers:[String:String] = [:]
        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        
        if getUserData()?.data != nil{
            headers["Authorization"] = getUserData()?.accessToken
        }
        
        let url = baseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        print("header:-\(headers)")
        
        Alamofire.request(url, method: .patch, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            
            hideIndicator()
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            print("Response: ", response)
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    
                    if let _ = response.result.value as? Any {
                        completionHandler(response.result.value as? Any, nil, statusCode)
                    }
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                
                if let jsonData = response.data {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                    }
                    catch let err{
                        print("\n\n===========Error===========")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Print Server data:- " + str)
                        }
                        debugPrint(error)
                        print("===========================\n\n")
                        
                        debugPrint(err)
                        
                        error = err.localizedDescription
                    }
                }
                else {
                    debugPrint(error as Any)
                }
                
                if !error.isEmpty {
                    completionHandler(nil, error, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            }
        }.resume()
    }
    
    //MARK: - Post with Image upload
    func MakePostWithImageAPI(name:String, params:[String:Any], images:[UIImage], imageName:String = "photo", isAuth:Bool = true, completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void)
    {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        var headers:[String:String] = [:]
        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["Accept"] = "application/json"

        if getUserData()?.data != nil{
            headers["Authorization"] = getUserData()?.accessToken
        }

//        print(headers)
        let url = baseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for image in images {
                
                if image != UIImage(named: "ic_my_profile_nig_splash_holder"){
                    let imgData = image.jpegData(compressionQuality: 0.7)
                    multipartFormData.append(imgData!, withName: imageName,fileName: "image", mimeType: "image.jpg")
                }
                
            }
            
            for (key, value) in params {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method:.post,
           headers:headers, encodingCompletion: { result in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    
                    let statusCode = response.response?.statusCode
                    print("statusCode:-\(statusCode ?? 0)")
                    
                    if(response.error == nil) {
                        completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                    }
                    else {
                        completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                    }
                })
            case .failure( _):
                
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, 500)
            }
        })
    }
    
    func MakePutWithImageAPI(name:String, params:[String:Any], images:[UIImage], imageName:String = "photo", headers:[String : String], completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        print("headers:", headers)
        let url = baseURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for image in images {
                multipartFormData.append(image.jpeg(.medium)!, withName: imageName,fileName: "\(randomString(length: 5)).jpg", mimeType: "image/jpg")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method:.put,
           headers:headers, encodingCompletion: { result in
            
            
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    
                    let statusCode = response.response?.statusCode
                    print("statusCode:-\(statusCode ?? 0)")
                    
                    if(response.error == nil) {
                        completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                    }
                    else {
                        completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                    }
                })
            case .failure( _):
                
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, 500)
            }
        })
    }
    
    //MARK: - Delete with body
    func makeDeleteApiWithBody(name: String, params:[String:Any], completionHandler: @escaping(NSDictionary?, String?, Int?) -> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        var headers:[String:String] = [:]
        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        
        if getUserData()?.data != nil{
            headers["Authorization"] = getUserData()?.accessToken
        }

        print("header:-\(headers)")
        
        let url = baseURL + name
        print("url:-\(url)")
        print("Param:-\(JSON(params))")
        
        request(url, method: .delete, parameters: params, encoding: URLEncoding.httpBody , headers: headers).responseJSON(completionHandler: { (response) in
            print("response:", response)
            
            hideIndicator()
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                
                if let jsonData = response.data {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                    }
                    catch let err{
                        print("\n\n===========Error===========")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Print Server data:- " + str)
                        }
                        debugPrint(error)
                        print("===========================\n\n")
                        
                        debugPrint(err)
                        
                        error = err.localizedDescription
                    }
                }
                else {
                    debugPrint(error as Any)
                }
                
                if !error.isEmpty {
                    completionHandler(nil, error, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            }
        })
    }
    
    //Vishal setup
    func requestMultiPartFromData(name:String, params:[String:String], imgName:String, imagesMutableArray: NSMutableArray,arrImage:[UIImage], completionHandler: @escaping (NSDictionary?, String?)-> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No internet connection available.".localized)
            hideIndicator()
            return
        }
        
        let uploadURL = baseURL+name
        
        var headers:[String:String] = [:]
        headers["X-localization"] =  getLanguage() == arabic ? arabic : english
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["Accept"] = "application/json"
        
        if getUserData()?.data != nil{
            headers["Authorization"] = getUserData()?.accessToken
        }
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in

                for i in 0..<imagesMutableArray.count{
                    
                    if let dict = imagesMutableArray[i] as? NSMutableDictionary{
                        if dict["image_id"] as! Int == 0{
                            
                            multipartFormData.append((arrImage[i]).jpegData(compressionQuality: 0.8)!, withName: "\(imgName)\(i)",fileName:"\(imgName)\(i)", mimeType: "image/jpg")
                        }
                    }
                }
                
                for (key, value) in params {
                    multipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
                }
            }, to: uploadURL, method: .post,headers: headers,encodingCompletion: { result in
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: { (response) in
                        
                        let statusCode = response.response?.statusCode
                        print("statusCode:-\(statusCode ?? 0)")
                        
                        if(response.error == nil) {
                            completionHandler(response.result.value as? NSDictionary, nil)
                        }
                        else {
                            completionHandler(nil, "Server_not_responding_please_try_again_key".localized)
                        }
                    })
                case .failure( _):
                    
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized)
                }
            })
        
    }
}
