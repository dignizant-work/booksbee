//
//  ServiceListPostfix.swift
//  Gymscanner
//
//  Created by Vishal on 30/06/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit

///--- Validate
let user_data_key = "userData"

//api iOS version

//Main URL:
let baseURL = "http://52.66.128.99/booksbee/api/"

let login = "login"
let register = "register"
let forgotPassword = "request-reset-password"
let changePassword = "changepassword"
let updateProfile = "updateProfile"

let writeReviewURL = "writeReview"
let bookDetailsURL = "bookDetails"

let homepageURL = "homepage"
let organizationListURL = "OrganizationList"
let myBookListURL = "mybookList"
let bookListURL = "bookList"
let addToCartURL = "addToCart"
let bookBidingURL = "bookBiding"

let reviewListsURL = "reviewList"
let addAddressURL = "addAddress"
let myAddressesURL = "myAddresses"
let deleteMyAddressURL = "deleteMyAddress"

let addOrderURL = "addOrder"
let myOrderURL = "myOrder"
let orderDetailsURL = "orderDetails"
let checkUniqueValueURL = "checkUniqueValue"
let getUserProfileURL = "getUserProfile"
let myBidlistURL = "myBidlist"
let bidListingURL = "bidListing"
let bidDetailsURL = "BidDetails"

let fatoorahPaymentpageURL = "fatoorah_paymentpage"

let setDefaultAddressURL = "setDefaultAddress"

let categoryListURL = "categoryList"
let addBookURL = "addBook"

let cartListingURL = "cartListing"
let itemRemovefromCartURL = "itemRemovefromCart"
let deleteMybookURL = "deleteMybook"

let myDonateBookURL = "myDonateBook"
let donateBookURL = "donateBook"

let viewAllBestsellersURL = "viewAllBestsellers"
let bestSellerBooksURL = "bestSellerBooks"

let notificationListURL = "notificationList"

let termsURL = "terms"
let privacyURL = "privacy"

let logout = "logout"


